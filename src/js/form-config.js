$('.form__input').blur(function () {
    if ($(this).val()) {
        $(this).closest('.form__wrapper').addClass('form--filled');
    } else {
        $(this).closest('.form__wrapper').removeClass('form--filled');
    }
});