<?php
    include 'src/includes/header.php'
?>
<main>
    <section class="sct-banner">
        <div class="degrade-int"></div>
        <img class="img-banner" src="assets/images/banner/encuentra-tu-aro.jpg" alt="">
        <div class="content-text-banner prdcts">
            <h2 class="title-banner text-uppercase font-bold">Encuentra tu aro</h2>
            <div class="wrapper-select-banner">
                <div class="wrapper-select-cart select-banner-one">
                    <div class="button dropdown">
                        <select id="select-stores" class="font-bold">
                            <option>DIÁMETRO :</option>
                            <option value="125" class="stores">125</option>
                            <option value="130" class="stores">130</option>
                        </select>
                    </div>
                </div>
                <div class="wrapper-select-cart select-banner-medio">
                    <div class="button dropdown">
                        <select id="select-stores" class="font-bold">
                            <option>DISTANCIA :</option>
                            <option value="125" class="stores">125</option>
                            <option value="130" class="stores">130</option>
                        </select>
                    </div>
                </div>
                <button class="btn-bsc-select"><i class="icon-Lupa"></i></button>
            </div>
        </div>
        <ol class="breadcrumb font-regular text-uppercase">
            <li class="bread-item"><a class="bread-link" href="#">INICIO</a></li>
            <li class="bread-item active"><a class="bread-link" href="#">aros</a></li>
        </ol>
        <img class="img-product-banner wow slideInUp" src="assets/images/banner/Aros.png" alt="">
    </section>
    <section class="sct-product">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 padd-tselect">
                    <div class="">
                        <h2 class="font-bold title-busq col-xs-12 col-sm-6">Resultado de búsqueda: <span class="color-primary">VF542 Eliminator</span></h2>
                        <div class="select-int col-xs-12 col-sm-6">
                            <h3 class="title-left-select font-regular d-in-block">ORDENAR POR: </h3>
                            <div class="wrapper-select-cart">
                                <div class="button dropdown">
                                    <select id="select-stores" class="font-regular">
                                        <option value="mayor-menor" class="stores">Precio de mayor a menor</option>
                                        <option value="menor-mayor" class="stores">Precio de menor a mayor</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- PRODUCTOS -->
                <div class="col-xs-12">
                    <div class="row">
                        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                            <div class="xhov wow zoomIn">
                                <div class="wrapper-card-product">
                                    <a href="detalle-de-producto.php" class="link-wrapper-prod h100 w100 d-block">
                                        <div class="wrapper-img-prod h-auto">
                                            <div class="content-img-prod">
                                                <img class="w-100" src="assets/images/productos/aro.jpg" alt="">
                                            </div>
                                        </div>
                                        <div class="info-product text-uppercase">
                                            <h2 class="font-bold name-cod-prod">aro 15</h2>
                                            <h3 class="font-regular name-brand">DARWIN</h3>
                                            <div class="wrapper-price flex-al-cent sp-arrow">
                                                <h2 class="font-bold price-prod"><span class="font-regular price-dsct-prod">S/720.00</span> S/ 720.00</h2>
                                            </div>
                                        </div>
                                    </a>
                                    <div class="dif">
                                        <div class="add-purchase ad2b d-flex">
                                            <div class="qty mt-5">
                                                <span class="plus bg-dark">+</span>
                                                <input type="number" class="count font-regular" name="qty" value="1">
                                                <span class="minus bg-dark">-</span>
                                            </div>
                                            <div class="btn-purchase">
                                                <a href="carrito-de-compras.php" class="link-purchase font-bold text-uppercase text-center">comprar <i class="icon-carrito"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="circle-dscto d-flex">
                                        <h4 class="font-regular">-20%</h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                            <div class="xhov wow zoomIn">
                                <div class="wrapper-card-product">
                                    <a href="detalle-de-producto.php" class="link-wrapper-prod h100 w100 d-block">
                                        <div class="wrapper-img-prod h-auto">
                                            <div class="content-img-prod">
                                                <img class="w-100" src="assets/images/productos/aro.jpg" alt="">
                                            </div>
                                        </div>
                                        <div class="info-product text-uppercase">
                                            <h2 class="font-bold name-cod-prod">aro 15</h2>
                                            <h3 class="font-regular name-brand">DARWIN</h3>
                                            <div class="wrapper-price flex-al-cent sp-arrow">
                                                <h2 class="font-bold price-prod"><span class="font-regular price-dsct-prod">S/720.00</span> S/ 720.00</h2>
                                            </div>
                                        </div>
                                    </a>
                                    <div class="dif">
                                        <div class="add-purchase ad2b d-flex">
                                            <div class="qty mt-5">
                                                <span class="plus bg-dark">+</span>
                                                <input type="number" class="count font-regular" name="qty" value="1">
                                                <span class="minus bg-dark">-</span>
                                            </div>
                                            <div class="btn-purchase">
                                                <a href="carrito-de-compras.php" class="link-purchase font-bold text-uppercase text-center">comprar <i class="icon-carrito"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="circle-dscto d-flex">
                                        <h4 class="font-regular">-20%</h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                            <div class="xhov wow zoomIn">
                                <div class="wrapper-card-product">
                                    <a href="detalle-de-producto.php" class="link-wrapper-prod h100 w100 d-block">
                                        <div class="wrapper-img-prod h-auto">
                                            <div class="content-img-prod">
                                                <img class="w-100" src="assets/images/productos/aro.jpg" alt="">
                                            </div>
                                        </div>
                                        <div class="info-product text-uppercase">
                                            <h2 class="font-bold name-cod-prod">aro 15</h2>
                                            <h3 class="font-regular name-brand">DARWIN</h3>
                                            <div class="wrapper-price flex-al-cent sp-arrow">
                                                <h2 class="font-bold price-prod"><span class="font-regular price-dsct-prod">S/720.00</span> S/ 720.00</h2>
                                            </div>
                                        </div>
                                    </a>
                                    <div class="dif">
                                        <div class="add-purchase ad2b d-flex">
                                            <div class="qty mt-5">
                                                <span class="plus bg-dark">+</span>
                                                <input type="number" class="count font-regular" name="qty" value="1">
                                                <span class="minus bg-dark">-</span>
                                            </div>
                                            <div class="btn-purchase">
                                                <a href="carrito-de-compras.php" class="link-purchase font-bold text-uppercase text-center">comprar <i class="icon-carrito"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="circle-dscto d-flex">
                                        <h4 class="font-regular">-20%</h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                            <div class="xhov wow zoomIn">
                                <div class="wrapper-card-product">
                                    <a href="detalle-de-producto.php" class="link-wrapper-prod h100 w100 d-block">
                                        <div class="wrapper-img-prod h-auto">
                                            <div class="content-img-prod">
                                                <img class="w-100" src="assets/images/productos/aro.jpg" alt="">
                                            </div>
                                        </div>
                                        <div class="info-product text-uppercase">
                                            <h2 class="font-bold name-cod-prod">aro 15</h2>
                                            <h3 class="font-regular name-brand">DARWIN</h3>
                                            <div class="wrapper-price flex-al-cent sp-arrow">
                                                <h2 class="font-bold price-prod"><span class="font-regular price-dsct-prod">S/720.00</span> S/ 720.00</h2>
                                            </div>
                                        </div>
                                    </a>
                                    <div class="dif">
                                        <div class="add-purchase ad2b d-flex">
                                            <div class="qty mt-5">
                                                <span class="plus bg-dark">+</span>
                                                <input type="number" class="count font-regular" name="qty" value="1">
                                                <span class="minus bg-dark">-</span>
                                            </div>
                                            <div class="btn-purchase">
                                                <a href="carrito-de-compras.php" class="link-purchase font-bold text-uppercase text-center">comprar <i class="icon-carrito"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="circle-dscto d-flex">
                                        <h4 class="font-regular">-20%</h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                            <div class="xhov wow zoomIn">
                                <div class="wrapper-card-product">
                                    <a href="detalle-de-producto.php" class="link-wrapper-prod h100 w100 d-block">
                                        <div class="wrapper-img-prod h-auto">
                                            <div class="content-img-prod">
                                                <img class="w-100" src="assets/images/productos/aro.jpg" alt="">
                                            </div>
                                        </div>
                                        <div class="info-product text-uppercase">
                                            <h2 class="font-bold name-cod-prod">aro 15</h2>
                                            <h3 class="font-regular name-brand">DARWIN</h3>
                                            <div class="wrapper-price flex-al-cent sp-arrow">
                                                <h2 class="font-bold price-prod"><span class="font-regular price-dsct-prod">S/720.00</span> S/ 720.00</h2>
                                            </div>
                                        </div>
                                    </a>
                                    <div class="dif">
                                        <div class="add-purchase ad2b d-flex">
                                            <div class="qty mt-5">
                                                <span class="plus bg-dark">+</span>
                                                <input type="number" class="count font-regular" name="qty" value="1">
                                                <span class="minus bg-dark">-</span>
                                            </div>
                                            <div class="btn-purchase">
                                                <a href="carrito-de-compras.php" class="link-purchase font-bold text-uppercase text-center">comprar <i class="icon-carrito"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="circle-dscto d-flex">
                                        <h4 class="font-regular">-20%</h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                            <div class="xhov wow zoomIn">
                                <div class="wrapper-card-product">
                                    <a href="detalle-de-producto.php" class="link-wrapper-prod h100 w100 d-block">
                                        <div class="wrapper-img-prod h-auto">
                                            <div class="content-img-prod">
                                                <img class="w-100" src="assets/images/productos/aro.jpg" alt="">
                                            </div>
                                        </div>
                                        <div class="info-product text-uppercase">
                                            <h2 class="font-bold name-cod-prod">aro 15</h2>
                                            <h3 class="font-regular name-brand">DARWIN</h3>
                                            <div class="wrapper-price flex-al-cent sp-arrow">
                                                <h2 class="font-bold price-prod"><span class="font-regular price-dsct-prod">S/720.00</span> S/ 720.00</h2>
                                            </div>
                                        </div>
                                    </a>
                                    <div class="dif">
                                        <div class="add-purchase ad2b d-flex">
                                            <div class="qty mt-5">
                                                <span class="plus bg-dark">+</span>
                                                <input type="number" class="count font-regular" name="qty" value="1">
                                                <span class="minus bg-dark">-</span>
                                            </div>
                                            <div class="btn-purchase">
                                                <a href="carrito-de-compras.php" class="link-purchase font-bold text-uppercase text-center">comprar <i class="icon-carrito"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="circle-dscto d-flex">
                                        <h4 class="font-regular">-20%</h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

</main>
    <?php
        include 'src/includes/footer.php'
    ?>
</body>
</html>