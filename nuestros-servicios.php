<?php
    include 'src/includes/header.php'
?>
    <main class="main-services">
        <section class="sct-banner">
            <div class="degrade-int"></div>
            <img class="img-banner" src="assets/images/banner/nuestros-servicios.jpg" alt="">
            <div class="content-text-banner">
                <h2 class="title-banner text-uppercase font-bold">Nuestros servicios</h2>
                <p class="p-internas">Lorem ipsum dolor sit, amet consectetur adipisicing elit. Nihil quos laudantium,
                    vero incidunt optio deserunt nulla, odit nesciunt maxime veniam quibusdam magnam quo iure corrupti
                    consectetur obcaecati omnis distinctio numquam?</p>
            </div>
            <a href="#our-services" class="icon-arrow" data-ancla="our-services"></a>
        </section>
        <section class="our-services" id="our-services">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-xs-12 col-service">
                        <div class="container">
                            <div class="row">
                                <div class="col-xs-12 col-md-6 wrapper-service px-0 wow slideInLeft">
                                    <div class="info-service">
                                        <h2 class="title-service text-uppercase font-bold">Alineamiento
                                            computarizado<br>(CONVERGENCIA
                                            Y DIVERGENCIA)</h2>
                                        <p class="p-internas">Lorem ipsum, dolor sit amet consectetur adipisicing elit.
                                            Accusantium
                                            eligendi possimus aut! Nesciunt illo perspiciatis nihil dolores? Expedita ab
                                            aliquam commodi assumenda odio maiores in fugit porro ducimus, deserunt
                                            perspiciatis!</p>
                                        <a href="#" class="font-regular link-mas-info">MÁS INFORMACIÓN <span
                                                class="icon-whatsapp"></span></a>
                                    </div>
                                    <div class="img-service">
                                        <img class="img100-cover" src="assets/images/internas/alineamiento.jpg" alt="">
                                    </div>
                                </div>
                                <div class="col-xs-12 col-md-6 wrapper-service px-0 wow slideInRight">
                                    <div class="img-service">
                                        <img class="img100-cover"
                                            src="assets/images/internas/serv-inclinacion-camber.jpg" alt="">
                                    </div>
                                    <div class="info-service">
                                        <h2 class="title-service text-uppercase font-bold">inclinación de camber y
                                            caster</h2>
                                        <p class="p-internas">Lorem ipsum, dolor sit amet consectetur adipisicing elit.
                                            Accusantium
                                            eligendi possimus aut! Nesciunt illo perspiciatis nihil dolores? Expedita ab
                                            aliquam commodi assumenda odio maiores in fugit porro ducimus, deserunt
                                            perspiciatis!</p>
                                        <a href="#" class="font-regular link-mas-info">MÁS INFORMACIÓN <span
                                                class="icon-whatsapp"></span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-service">
                        <div class="container">
                            <div class="row">
                                <div class="col-xs-12 col-md-6 wrapper-service px-0 wow slideInLeft">
                                    <div class="info-service">
                                        <h2 class="title-service text-uppercase font-bold">inclinación mc pherson</h2>
                                        <p class="p-internas">Lorem ipsum, dolor sit amet consectetur adipisicing elit.
                                            Accusantium
                                            eligendi possimus aut! Nesciunt illo perspiciatis nihil dolores? Expedita ab
                                            aliquam commodi assumenda odio maiores in fugit porro ducimus, deserunt
                                            perspiciatis!</p>
                                        <a href="#" class="font-regular link-mas-info">MÁS INFORMACIÓN <span
                                                class="icon-whatsapp"></span></a>
                                    </div>
                                    <div class="img-service">
                                        <img class="img100-cover" src="assets/images/internas/serv-inclinacion-mc.jpg"
                                            alt="">
                                    </div>
                                </div>
                                <div class="col-xs-12 col-md-6 wrapper-service px-0 wow slideInRight">
                                    <div class="img-service">
                                        <img class="img100-cover" src="assets/images/internas/serv-rotacion.jpg"
                                            alt="">
                                    </div>
                                    <div class="info-service">
                                        <h2 class="title-service text-uppercase font-bold">rotación</h2>
                                        <p class="p-internas">Lorem ipsum, dolor sit amet consectetur adipisicing elit.
                                            Accusantium
                                            eligendi possimus aut! Nesciunt illo perspiciatis nihil dolores? Expedita ab
                                            aliquam commodi assumenda odio maiores in fugit porro ducimus, deserunt
                                            perspiciatis!</p>
                                        <a href="#" class="font-regular link-mas-info">MÁS INFORMACIÓN <span
                                                class="icon-whatsapp"></span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-service">
                        <div class="container">
                            <div class="row">
                                <div class="col-xs-12 col-md-6 wrapper-service px-0 wow slideInLeft">
                                    <div class="info-service">
                                        <h2 class="title-service text-uppercase font-bold">parchado</h2>
                                        <p class="p-internas">Lorem ipsum, dolor sit amet consectetur adipisicing elit.
                                            Accusantium
                                            eligendi possimus aut! Nesciunt illo perspiciatis nihil dolores? Expedita ab
                                            aliquam commodi assumenda odio maiores in fugit porro ducimus, deserunt
                                            perspiciatis!</p>
                                        <a href="#" class="font-regular link-mas-info">MÁS INFORMACIÓN <span
                                                class="icon-whatsapp"></span></a>
                                    </div>
                                    <div class="img-service">
                                        <img class="img100-cover" src="assets/images/internas/serv-parchado.jpg"
                                            alt="">
                                    </div>
                                </div>
                                <div class="col-xs-12 col-md-6 wrapper-service px-0 wow slideInRight">
                                    <div class="img-service">
                                        <img class="img100-cover" src="assets/images/internas//serv-balanceo.jpg"
                                            alt="">
                                    </div>
                                    <div class="info-service">
                                        <h2 class="title-service text-uppercase font-bold">balanceo</h2>
                                        <p class="p-internas">Lorem ipsum, dolor sit amet consectetur adipisicing elit.
                                            Accusantium
                                            eligendi possimus aut! Nesciunt illo perspiciatis nihil dolores? Expedita ab
                                            aliquam commodi assumenda odio maiores in fugit porro ducimus, deserunt
                                            perspiciatis!</p>
                                        <a href="#" class="font-regular link-mas-info">MÁS INFORMACIÓN <span
                                                class="icon-whatsapp"></span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-service">
                        <div class="container">
                            <div class="row">
                                <div class="col-xs-12 col-md-6 wrapper-service px-0 wow slideInLeft">
                                    <div class="info-service">
                                        <h2 class="title-service text-uppercase font-bold">enllante</h2>
                                        <p class="p-internas">Lorem ipsum, dolor sit amet consectetur adipisicing elit.
                                            Accusantium
                                            eligendi possimus aut! Nesciunt illo perspiciatis nihil dolores? Expedita ab
                                            aliquam commodi assumenda odio maiores in fugit porro ducimus, deserunt
                                            perspiciatis!</p>
                                        <a href="#" class="font-regular link-mas-info">MÁS INFORMACIÓN <span
                                                class="icon-whatsapp"></span></a>
                                    </div>
                                    <div class="img-service">
                                        <img class="img100-cover" src="assets/images/internas/serv-enllante.jpg"
                                            alt="">
                                    </div>
                                </div>
                                <div class="col-xs-12 col-md-6 wrapper-service px-0 wow slideInRight">
                                    <div class="img-service">
                                        <img class="img100-cover" src="assets/images/internas/serv-dianosticos.jpg"
                                            alt="">
                                    </div>
                                    <div class="info-service">
                                        <h2 class="title-service text-uppercase font-bold">diagnósticos</h2>
                                        <p class="p-internas">Lorem ipsum, dolor sit amet consectetur adipisicing elit.
                                            Accusantium
                                            eligendi possimus aut! Nesciunt illo perspiciatis nihil dolores? Expedita ab
                                            aliquam commodi assumenda odio maiores in fugit porro ducimus, deserunt
                                            perspiciatis!</p>
                                        <a href="#" class="font-regular link-mas-info">MÁS INFORMACIÓN <span
                                                class="icon-whatsapp"></span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
<?php
    include 'src/includes/footer.php'
?>
</body>

</html>