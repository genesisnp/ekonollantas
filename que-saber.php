<?php
    include 'src/includes/header.php'
?>
    <main class="main-">
        <section class="sct-banner">
            <div class="degrade-int"></div>
            <img class="img-banner" src="assets/images/banner/nuestros-servicios.jpg" alt="">
            <div class="content-text-banner">
                <i class="icon-banner icon-que-saber"></i>
                <h2 class="title-banner text-uppercase font-bold">que saber</h2>
            </div>
            <a href="#what-to-know" class="icon-arrow" data-ancla="what-to-know"></a>
        </section>
        <section class="what-to-know" id="what-to-know">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <ul class="accordion">
                            <li><a class="accordion-heading font-bold">Antes de adquirir tus llantas ten en cuenta <i
                                        class="icon-flecha-accord"></i></a>
                                <div class="accordion-content">
                                    <p class="p-internas">Las llantas por su costo se convierten en una compra mayor, te
                                        acompañarán por un buen tiempo en tus viajes y en el uso diario. Por ello, que
                                        se
                                        hace
                                        necesario afinar la elección optando por los que mejor se ajusten a tus usos y
                                        requerimientos,así como comprender como cuidar tu inversión. No olvides que de
                                        las
                                        llantas depende tu seguridad y de otras personas.</p>
                                    <p class="p-internas">Adquiere las mejores llantas que tu economía pueda comprar,
                                        busca
                                        asesoría que te brinde la información adecuada para decidir en base a beneficios
                                        y
                                        precios.</p>
                                    <p class="p-internas">Asegúrate de comprar unas que satisfagan tus expectativas, ya
                                        que
                                        estas te van a acompañar por un periodo largo de tiempo y se convertirán en tu
                                        agrado o
                                        decepción cada vez que manejes.</p>
                                </div>
                            </li>
                            <li><a class="accordion-heading font-bold">La garantía EKONO Llantas <i
                                        class="icon-flecha-accord"></i></a>
                                <div class="accordion-content">
                                    <p class="p-internas">Todas las llantas comercializadas por EKONO llantas cuentan
                                        con
                                        una
                                        garantía limitada
                                        contra defectos de manufactura, materiales o mano de obra de empleados.</p>
                                    <p class="p-internas">Esta garantía excluye y no se aplica a aquellas llantas que
                                        estén
                                        gastadas más allá de
                                        los indicadores de desgaste de la banda de rodamiento o en todo caso que tenga
                                        menos
                                        de
                                        1.6 mm de profundidad de cocada. La garantía es vigente hasta 5 años desde la
                                        fecha
                                        de
                                        compra.</p>
                                </div>
                            </li>
                            <li><a class="accordion-heading font-bold">Recomendaciones técnicas para cambiar sus
                                    llantas <i class="icon-flecha-accord"></i></a>
                                <div class="accordion-content">
                                    <p class="p-internas">Al reemplazar los neumáticos respete todas sus características
                                        (medida, código de
                                        velocidad, índice de carga) estipuladas por el fabricante del vehículo en sus
                                        manuales.
                                    </p>
                                    <p class="p-internas">Si desea reemplazar la medida, debe respetar las conversiones
                                        recomendadas por el
                                        fabricante del vehículo / neumático y cuidar que no se produzcan interferencias
                                        con
                                        la
                                        carrocería u otras partes mecánicas del vehículo.</p>
                                    <p class="font-bold">Además, tenga en cuenta lo siguiente:</p>
                                    <ul>
                                        <li class="p-internas">Nunca reemplace por neumáticos con código de velocidad
                                            menor
                                            al original
                                            provisto.
                                            Por ejemplo: Si tiene el código "H" no puede cambiarlo por un "T" o menores.
                                        </li>
                                        <li class="p-internas">Nunca reemplace por neumáticos con un índice de carga
                                            inferior al original
                                            provisto.
                                            Por ejemplo: si es 85 no colocar 82.</li>
                                        <li class="p-internas">Nunca mezcle en un mismo vehículo neumáticos
                                            convencionales
                                            con radiales.</li>
                                    </ul>
                                </div>
                            </li>
                            <li><a class="accordion-heading font-bold">Eligiendo la llanta para tu 4x4 <i
                                        class="icon-flecha-accord"></i></a>
                                <div class="accordion-content">
                                    <h2 class="subtitle-heading font-bold">Las llantas Duele H/T</h2>
                                    <p class="p-internas">Las camionetas 4x4 vienen equipadas de fábrica con este tipo
                                        de
                                        neumático. Son diseñados para obtener un desempeño sobresaliente en asfalto y
                                        ocasionalmente fuera de carretera. El diseño de huella y el compuesto de caucho
                                        buscan
                                        un mejor agarre en asfalto a altas velocidades, confort y bajos niveles de
                                        ruido.
                                    </p>
                                    <div class="img-llanta-4x4">
                                        <img src="assets/images/internas/que-saber/img-llantas-dueleh-t.jpg" alt="">
                                    </div>

                                    <h2 class="subtitle-heading font-bold">Las llantas A/T</h2>
                                    <p class="p-internas">(50% pista y 50 % fuera de carretera)<br>
                                        Son las más populares en nuestro medio ya que brindan un buen comportamiento en
                                        todo
                                        tipo de terreno, tienen una estructura robusta, gracias a las nuevas
                                        tecnologías,
                                        ofrecen confort y bajo nivel de ruido. Es el neumático más conocido por ser
                                        refinado
                                        en
                                        la ciudad y agresivo en la aventura off road (Todo terreno).</p>
                                    <div class="img-llanta-4x4">
                                        <img src="assets/images/internas/que-saber/img-llantas-duelea-t.jpg" alt="">
                                    </div>

                                    <h2 class="subtitle-heading font-bold">Las llantas M/T</h2>
                                    <p class="p-internas">De aspecto agresivo, con bloques grandes que aumentan la
                                        tracción
                                        y
                                        auto limpieza en barro y lodo, son neumáticos robustos reservados para
                                        geografías
                                        muy
                                        agrestes. Diseñada para soportan impactos y cortes.</p>
                                    <div class="img-llanta-4x4">
                                        <img src="assets/images/internas/que-saber/img-llantas-duelem-t.jpg" alt="">
                                    </div>
                                </div>
                            </li>
                            <li><a class="accordion-heading font-bold">Equivalencia de llantas <i
                                        class="icon-flecha-accord"></i></a>
                                <div class="accordion-content">
                                    <p class="p-internas">Con la gran variedad de medidas y modelos de llantas
                                        existentes es
                                        posible variar el diámetro de los aros sin modificar la altura total del
                                        conjunto
                                        llanta
                                        aro y de esta manera no alterar la geometría del diseño original del vehículo.
                                    </p>
                                    <p class="p-internas">Es posible incrementar el diámetro del aro sin cambiar
                                        sustancialmente
                                        el diámetro total del conjunto utilizando llantas de perfiles más bajos. En casi
                                        la
                                        totalidad de los vehículos es permitida la variación de más o menos 3% del
                                        diámetro
                                        total del conjunto sin producir alteración significativa en las medidas del
                                        vehículo.
                                    </p>
                                    <p class="p-internas">Mayores anchos, mayores diámetros de aros y perfiles más
                                        bajos,
                                        compuestos más adherentes implican un mayor control en el manejo, una más rápida
                                        respuesta de dirección y menores distancias de frenado.</p>
                                    <div class="img-large">
                                        <img src="assets/images/internas/que-saber/equivalencia-de-llantas.jpg" alt="">
                                    </div>
                                </div>
                            </li>
                            <li><a class="accordion-heading font-bold">Incrementa la performance de tu auto <i
                                        class="icon-flecha-accord"></i></a>
                                <div class="accordion-content">
                                    <p class="p-internas">Los autos llegan con una medida de llanta original de fábrica,
                                        la
                                        cual
                                        tiene un buen comportamiento. Sin embargo, esta medida puede ser variada si se
                                        busca
                                        optimizar algunos aspectos del vehículo, de acuerdo con gustos, necesidades y
                                        estilo
                                        de
                                        manejo, este incremento en estética y performance, se puede lograr mediante la
                                        siguiente
                                        fórmula:</p>
                                    <ul>
                                        <li class="p-internas">Usar llantas con perfiles más bajos.</li>
                                        <li class="p-internas">Aumentar el ancho de sección de las llantas.</li>
                                        <li class="p-internas">Cambiar los aros por unos de mayor diámetro y ancho.</li>
                                        <li class="p-internas">Mantener el tamaño total del conjunto aro y llanta.</li>
                                        <li class="p-internas">Mantener o mejorar la capacidad de carga y velocidad
                                            máxima
                                            de las llantas.</li>
                                    </ul>
                                    <div class="img-large">
                                        <img src="assets/images/internas/que-saber/incrementa-la-performance.jpg"
                                            alt="">
                                    </div>
                                </div>
                            </li>
                            <li><a class="accordion-heading font-bold">Servicios de prevención y mantenimiento -
                                    Suspensión y amortiguación <i class="icon-flecha-accord"></i></a>
                                <div class="accordion-content">
                                    <p class="p-internas">Los vehículos recorren diferentes rutas y terrenos, estos
                                        pueden
                                        ser
                                        irregulares, por el estado o antigüedad de los mismos, el resultado da como
                                        consecuencia
                                        que el trabajo de la suspensión sea muy exigente. Es muy importante
                                        periódicamente
                                        revisar su suspensión así evitará hacer gastos mayores.</p>
                                    <p class="p-internas">En EKONO llantas revisar su suspensión no tiene costo, un
                                        técnico
                                        especialista realizará una revisión al detalle.</p>
                                    <div class="img-peq">
                                        <img src="assets/images/internas/que-saber/suspe--amortig.jpg" alt="">
                                    </div>
                                </div>
                            </li>
                            <li><a class="accordion-heading font-bold">Tus llantas pueden estar pinchadas y no lo has
                                    notado <i class="icon-flecha-accord"></i></a>
                                <div class="accordion-content">
                                    <p class="p-internas">Las llantas al ser de caucho pueden sufrir pinchaduras o
                                        penetraciones
                                        por clavos u otros objetos que puedan estar en las pistas, en algunos casos no
                                        necesariamente la llanta se baja en su totalidad, por lo tanto podrías estar
                                        usando
                                        tu
                                        vehículo con una llanta en mal estado y por el orificio se va filtrando agua, y
                                        otros
                                        agentes contaminantes que van a ir deteriorando sus llantas.</p>
                                    <p class="p-internas">Visítanos, un técnico especialista revisará tus llantas y
                                        confirmar
                                        que estén pinchadas.</p>
                                    <div class="img-peq2 display-flex">
                                        <img src="assets/images/internas/que-saber/llanta-pinchada1.jpg" alt="">
                                        <img src="assets/images/internas/que-saber/llanta-pinchada2.jpg" alt="">
                                    </div>
                                </div>
                            </li>
                            <li><a class="accordion-heading font-bold">Servicio de prevención y mantenimiento - Balanceo
                                    y rotación <i class="icon-flecha-accord"></i></a>
                                <div class="accordion-content">
                                    <p class="p-internas">Unas ruedas desbalanceadas producirán vibraciones en el
                                        volante y
                                        un deterioro prematuro de los rodajes y terminales, en algunos casos las
                                        vibraciones
                                        no se notan.Se recomienda balancear y rotar sus llantas cada 5,000 km.La
                                        rotación
                                        permitirá un desgaste parejo en tus 4 ruedas.</p>
                                    <div class="img-large">
                                        <img src="assets/images/internas/que-saber/img-balanceoyrotacion.jpg" alt="">
                                    </div>
                                </div>
                            </li>
                            <li><a class="accordion-heading font-bold">Servicios de preención y mantenimiento -
                                    Alineamiento de dirección <i class="icon-flecha-accord"></i></a>
                                <div class="accordion-content">
                                    <p class="p-internas">Un golpe fuerte contra el sardinel o bache puede causar el
                                        desalineamiento de tu vehículo, esto deteriorará rápidamente tus llantas.</p>
                                    <p class="p-internas">Revisa periódicamente el alineamiento tal como lo especifica
                                        el
                                        manual de tu vehículo o cada 5,000 km o siempre que note que la dirección jale
                                        para
                                        uno de los lados.</p>
                                    <div class="img-large">
                                        <img src="assets/images/internas/que-saber/img-alineamiento.jpg" alt="">
                                    </div>
                                </div>
                            </li>
                            <li><a class="accordion-heading font-bold">¿Sabías que las llantas desgastadas no
                                    frenan? <i class="icon-flecha-accord"></i></a>
                                <div class="accordion-content">
                                    <p class="p-internas">El desgaste de tus llantas ocasiona la perdida de adherencia o
                                        agarre. Cuanto más desgastadas estén (huellas cada vez menos profundas), más
                                        largas
                                        serán las distancias de frenado, especialmente en carretera mojada, y mayor el
                                        riesgo de hidroplaneo (cuando las llantas pierden contacto con el pavimento por
                                        una
                                        película de agua y por consiguiente disminuye o se elimina el poder de
                                        adherencia de
                                        las ruedas).</p>
                                    <p class="p-internas">Este cuadro es referencial, la distancia de frenado no sólo
                                        van en
                                        función del desgaste sino también de la medida, tecnología de la llanta, o
                                        estado de
                                        los frenos, características del vehículo, tipo de pavimento, etc.</p>
                                    <div class="img-large">
                                        <img src="assets/images/internas/que-saber/llantas-desgastadas.jpg" alt="">
                                    </div>
                                </div>
                            </li>
                            <li><a class="accordion-heading font-bold">¿Sabías que puedes controlar el desgaste de tus
                                    llantas? <i class="icon-flecha-accord"></i></a>
                                <div class="accordion-content">
                                    <p class="p-internas">Al examinar la presión de aire de tus llantas, también es
                                        necesario verificar el desgaste de la banda de rodamiento. Para verificar los
                                        límites de uso podemos apoyarnos en los indicadores TWI que se ubican en la
                                        banda de
                                        rodamiento.</p>
                                    <p class="p-internas">El desgaste no debe alcanzar jamás los indicadores ubicados en
                                        el
                                        fondo de la huella y esta debe permanecer uniforme en toda la superficie.</p>
                                    <p class="p-internas">"El indicador de desgaste de 1.6milímetros indica la altura de
                                        huella mínima legal".Visítanos y un técnico revisará el desgaste de tus llantas.
                                    </p>
                                    <div class="img-peq">
                                        <img src="assets/images/internas/que-saber/controlar-desgaste.jpg" alt="">
                                    </div>
                                </div>
                            </li>
                            <li><a class="accordion-heading font-bold">Controla la presión de aire, y alarga la vida de
                                    tus llantas <i class="icon-flecha-accord"></i></a>
                                <div class="accordion-content">
                                    <p class="p-internas">Las llantas deben ser utilizadas con presiones correctas,
                                        especificadas por el fabricante del vehículo, para lograr la máxima seguridad y
                                        durabilidad. En la puerta y manual de tu vehículo está escrito las cantidades
                                        exactas.</p>
                                    <h2 class="subtitle-heading font-bold"> Para controlar la presión de tus llantas:
                                    </h2>
                                    <ul>
                                        <li class="p-internas">Sólo con las llantas en frío.</li>
                                        <li class="p-internas">Una vez a la semana y siempre antes de viajar.</li>
                                        <li class="p-internas">Use un buen medidor de presión.</li>
                                        <li class="p-internas">No olvide colocar las tapas de válvulas.</li>
                                        <li class="p-internas">Si prefieres pasa por EKONO llantas (este servicio no
                                            tiene
                                            costo).</li>
                                    </ul>
                                </div>
                            </li>
                            <li><a class="accordion-heading font-bold">Ten cuidado con los llaneteros deshonestos <i
                                        class="icon-flecha-accord"></i></a>
                                <div class="accordion-content">
                                    <ul>
                                        <li class="p-internas">Una modalidad de engaño es que en la tina de agua colocan
                                            elementos filudos
                                            (clavos o puntas de metal) para hacerle varias perforaciones a tu llanta.
                                        </li>
                                        <li class="p-internas">También el operario esconde herramientas puntiagudas
                                            entre
                                            sus prendas, para
                                            hacer más huecos a tu llanta. Así es como logran hacer varias perforaciones
                                            a
                                            las llantas, malográndola y aprovechando para cobrar precios excesivos en la
                                            reparación.</li>
                                        <li class="p-internas">Otra modalidad de fraude es venderle un parche más grande
                                            que
                                            el necesario, esto
                                            no es recomendable ya que es más costoso y afecta el balanceo de tu llanta.
                                        </li>
                                    </ul>
                                    <p class="p-internas">No te dejes sorprender, es poco probable que existan más de
                                        dos
                                        perforaciones en una sola llanta. Visítanos y siéntete satisfecho manteniendo tu
                                        llanta en buen estado.</p>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>
    </main>
    <?php
        include 'src/includes/footer.php'
    ?>
</body>

</html>