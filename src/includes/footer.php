<div class="wrapper-footer">
    <!-- SUSCRIPCIÓN -->
    <div class="bg-suscription">
        <div class="wrapper-suscription d-flex sp-betw container">
            <div class="row">
                <div class="content-text-susc float-left col-xs-12 col-sm-6">
                    <div class="d-flex">
                        <i class="icon-sobreBig float-left"></i>
                        <p class="p-internas d-in-block title-dskt-sus">Recibe actualizaciones por correo electrónico sobre nuestra
                            tienda y ofertas especiales.</p>
                        <p class="font-bold text-center m-auto title-mob-sus">Recibe nuestras ofertas especiales</p>
                    </div>

                </div>
                <div class="content-input-susc d-flex col-xs-12 col-sm-6">
                    <input type="text" class="d-in-block font-regular">
                    <button class="d-in-block btn-suscription font-bold">SUSCRÍBETE</button>
                </div>
            </div>
        </div>
    </div>
    <footer>
        <div class="container">
            <div class="row">
                <!-- NAV FOOTER -->
                <div class="col-xs-12 wrapper-nav-footer">
                    <div class="row">
                        <ul class="list-f">
                            <li class="item-f"><a class="link-f text-uppercase font-bold" href="nuestros-servicios.php">servicios</a>
                            </li>
                            <li class="item-f"><a class="link-f text-uppercase font-bold" href="nuestras-tiendas.php">tiendas</a></li>
                            <li class="item-f"><a class="link-f text-uppercase font-bold" href="nosotros.php">nosotros</a></li>
                        </ul>
                        <ul class="list-f">
                            <li class="item-f"><a class="link-f text-uppercase font-bold" href="como-comprar.php">¿cómo
                                    comprar?</a></li>
                            <li class="item-f"><a class="link-f text-uppercase font-bold" href="pedido-especial.php">pedido
                                    especial</a></li>
                            <li class="item-f"><a class="link-f text-uppercase font-bold" href="liquidacion.php">liquidación</a>
                            </li>
                        </ul>
                        <ul class="list-f">
                            <li class="item-f"><a class="link-f text-uppercase font-bold" href="formas-de-pago.php">formas de
                                    pago</a></li>
                            <li class="item-f"><a class="link-f text-uppercase font-bold" href="que-saber.php">que saber</a>
                            </li>
                            <li class="item-f"><a class="link-f text-uppercase font-bold" href="garantias.php">garantías y
                                    devoluciones</a>
                            </li>
                        </ul>
                        <ul class="list-f">
                            <li class="item-f"><a class="link-f text-uppercase font-bold" href="terminos-y-condiciones.php">términos y
                                    condiciones</a>
                            </li>
                            <li class="item-f"><a class="link-f text-uppercase font-bold" href="trabaja-con-nosotros.php">trabaja con
                                    nosotros</a>
                            </li>
                            <li class="item-f"><a class="link-f text-uppercase font-bold" href="contactanos.php">contacto</a></li>
                        </ul>
                        <ul class="list-f">
                            <li class="item-f"><a class="link-f text-uppercase font-bold" href="#">comprobantes
                                    electrónicos</a>
                            </li>
                            <li class="item-f"><a class="link-f text-uppercase font-bold" href="libro-de-reclamos.php">libro de
                                    reclamaciones</a>
                            </li>
                        </ul>
                        <ul class="item-contact">
                            <li class="item-f"><a class="link-f phones-f text-uppercase font-bold" href="#"><i
                                        class="icon-phone2 icon-f"></i>01 - 2194650</a></li>
                            <li class="item-f"><a class="link-f phones-f text-uppercase font-bold" href="#"><i
                                        class="icon-wts icon-f"></i>(+51) 941242206</a></li>
                            <li class="item-f"><a class="p-internas link-f text-emailF" href="#"><i
                                        class="icon-email icon-f"></i>compraenlinea@ekonollantas.com</a></li>
                        </ul>
                    </div>
                </div>
                <!-- METODOS DE PAGO -->
                <div class="col-xs-12">
                    <div class="content-met-pag">
                        <ul class="d-in-block">
                            <li class="list-img-met d-in-block"><img class="img100-cover"
                                    src="assets/images/logos/visa.jpg" alt=""></li>
                            <li class="list-img-met d-in-block"><img class="img100-cover"
                                    src="assets/images/logos/mastercard.jpg" alt=""></li>
                            <li class="list-img-met d-in-block"><img class="img100-cover"
                                    src="assets/images/logos/amerExpress.jpg" alt=""></li>
                            <li class="list-img-met d-in-block"><img class="img100-cover"
                                    src="assets/images/logos/dinersClub.jpg" alt=""></li>
                        </ul>
                        <ul class="d-in-block">
                            <li class="list-img-met d-in-block"><img class="img100-cover"
                                    src="assets/images/logos/afiliate-visa.jpg" alt=""></li>
                            <li class="list-img-met d-in-block"><img class="img100-cover"
                                    src="assets/images/logos/verif-visa.jpg" alt=""></li>
                            <li class="list-img-met d-in-block"><img class="img100-cover"
                                    src="assets/images/logos/mstcrd-secureC.jpg" alt=""></li>
                            <li class="list-img-met d-in-block"><img class="img100-cover"
                                    src="assets/images/logos/am-exp-skey.jpg" alt=""></li>
                        </ul>
                    </div>
                </div>
                <!--CREDITOS -->
                <div class="col-xs-12 px-0">
                    <div class="crd d-flex sp-betw">
                        <div class="crd1">
                            <p><a href="#">EKONOLLANTAS</a></p>
                            <p class="text-credits">TODOS LOS DERECHOS RESERVADOS 2019</p>
                        </div>
                        <div class="crd2">
                            <p><span class="text-credits">POWERED BY </span><a href="http://exe.pe/"
                                    class="text-credits">EXE.DIGITAL</a></p>
                            <p class="oculMob"><a href="https://validator.w3.org/check?uri=referer"
                                    class="text-f text-credits">HTML </a><a
                                    href="https://jigsaw.w3.org/css-validator/check/referer"
                                    class="text-f text-credits">CSS</a></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
</div>
<script src="assets/js/app.js"></script>
<script src="assets/js/libraries/wow.min.js"></script>
<script>
    var wow = new WOW({
        boxClass:     'wow',      
        animateClass: 'animated', 
        offset:       0,          
        mobile:       false,      
        live:         true,       
        callback:     function(box) {
        },
            scrollContainer: null,   
            resetAnimation: true,   
        }
    );
    wow.init();
</script>
