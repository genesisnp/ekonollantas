<?php
    include 'src/includes/header.php'
?>
    <main class="main-special-order">
        <section class="sct-banner">
            <div class="degrade-int"></div>
            <img class="img-banner" src="assets/images/banner/nuestros-servicios.jpg" alt="">
            <div class="content-text-banner">
                <i class="icon-banner icon-ped-especial"></i>
                <h2 class="title-banner text-uppercase font-bold">pedido especial</h2>
                <p class="subtitle-banner">¿No encuentras tus llantas? ¿Buscas medidas especiales?</p>
                <p class="subtitle-banner">Nosotros lo conseguimos por tí, envíanos tu consulta llenando el siguiente
                    formulario:</p>
            </div>
            <a href="#form-special-order" class="icon-arrow" data-ancla="form-special-order"></a>
        </section>
        <section class="form-special-order" id="form-special-order">
            <div class="container">
                <form action="#" class="form row" method="post" id="form-contact">
                    <div class="col-xs-12">
                        <div class="row">
                            <div class="col-xs-12 col-md-8">
                                <div class="row">
                                    <div class="form__wrapper col-xs-12 col-sm-6">
                                        <input type="text" class="form__input bg-input" id="name-pd-es" name="name-pd-es">
                                        <label class="form__label">
                                            <span class="form__label-content">Nombres:</span>
                                        </label>
                                    </div>
                                    <div class="form__wrapper col-xs-12 col-sm-6">
                                        <input type="text" class="form__input bg-input" id="last-name-pd-es" name="last-name-pd-es">
                                        <label class="form__label">
                                            <span class="form__label-content">Apellidos:</span>
                                        </label>
                                    </div>
                                    <div class="form__wrapper col-xs-12 col-sm-6">
                                        <input type="text" class="form__input bg-input" id="phone-pd-es" name="phone-pd-es">
                                        <label class="form__label">
                                            <span class="form__label-content">Teléfono:</span>
                                        </label>
                                    </div>
                                    <div class="form__wrapper col-xs-12 col-sm-6">
                                        <input type="email" class="form__input bg-input" id="email-pd-es" name="email-pd-es">
                                        <label class="form__label">
                                            <span class="form__label-content">E-mail</span>
                                        </label>
                                    </div>
                                    <div class="form__wrapper col-xs-12 col-sm-4">
                                        <input type="text" class="form__input bg-input" id="ancho-pd-es" name="ancho-pd-es">
                                        <label class="form__label">
                                            <span class="form__label-content">ancho</span>
                                        </label>
                                    </div>
                                    <div class="form__wrapper col-xs-12 col-sm-4">
                                        <input type="text" class="form__input bg-input" id="perfil-pd-es" name="perfil-pd-es">
                                        <label class="form__label">
                                            <span class="form__label-content">perfil</span>
                                        </label>
                                    </div>
                                    <div class="form__wrapper col-xs-12 col-sm-4">
                                        <input type="text" class="form__input bg-input" id="aro-pd-es" name="aro-pd-es">
                                        <label class="form__label">
                                            <span class="form__label-content">aro</span>
                                        </label>
                                    </div>
                                    <div class="form__wrapper col-xs-12 col-sm-4">
                                        <input type="text" class="form__input bg-input" id="marca-pd-es" name="marca-ped-es">
                                        <label class="form__label">
                                            <span class="form__label-content">marca de vehículo</span>
                                        </label>
                                    </div>
                                    <div class="form__wrapper col-xs-12 col-sm-4">
                                        <input type="text" class="form__input bg-input" id="modelo-pd-es" name="modelo-pd-es">
                                        <label class="form__label">
                                            <span class="form__label-content">modelo de vehículo</span>
                                        </label>
                                    </div>
                                    <div class="form__wrapper col-xs-12 col-sm-4">
                                        <input type="text" class="form__input bg-input" id="year-pd-es" name="year-pd-es">
                                        <label class="form__label">
                                            <span class="form__label-content">año de vehículo</span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-4 h100">
                                <div class="row h100 txt-ar">
                                    <div class="form__wrapper col-xs-12 h100">
                                        <textarea class="form__input form_textarea bg-input" id="textarea"
                                            name="textarea"></textarea>
                                        <label class="form__label">
                                            <span class="form__label-content">Mensaje</span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12">
                            <div class="checkbox">
                                <label class="font-regular label-pol">
                                    <input type="checkbox" /><i class="helper"></i><span>He leído y acepto los<span
                                            class="span-pol color-primary btn-modals">Términos y
                                            Condiciones</span> y la <span class="span-pol color-primary btn-modals">Política
                                            de Privacidad</span></span>
                                </label>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-4">
                            <div class="btn-container">
                                <button type="submit" class="btn-send font-bold" id="btn-send-form">ENVIAR</button>
                            </div>
                        </div>
                        </div>
                        
                    </div>
                </form>
            </div>
        </section>
    </main>
    <?php
        include 'src/includes/footer.php'
    ?>
    <script src="assets/js/form.js"></script>
</body>

</html>