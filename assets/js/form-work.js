$(document).ready(function () {
    $("#file").on("change", function (e) {
        var files = $(this)[0].files;
        if (files.length >= 2) {
            $(".file_label").text(files.length + " Adjuntar archivo");
        } else {
            var fileName = e.target.value.split("\\").pop();
            $(".file_label").text(fileName);
        }
    });
});
$('#btn-send-form').on('click', function(){
    $("#form-work-whith-us").validate({
        rules: {
            "name-work": "required",
            "last-name-work": "required",
            "area-work": "required",
            "email-work": {
                required: true,
                email: true
            },
            "phone-work": {
                required: true,
                minlength: 9
            },
            "textarea": {
                required: true
            }
        },
    });
})
