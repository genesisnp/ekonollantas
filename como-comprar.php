<?php
    include 'src/includes/header.php'
?>
    <main class="main-how-to-buy">
        <section class="sct-banner">
            <div class="degrade-int"></div>
            <img class="img-banner" src="assets/images/banner/nuestros-servicios.jpg" alt="">
            <div class="content-text-banner">
                <i class="icon-banner icon-como-comprar"></i>
                <h2 class="title-banner text-uppercase font-bold">¿Cómo comprar?</h2>
            </div>
            <a href="#how-to-buy" class="icon-arrow" data-ancla="how-to-buy"></a>
        </section>
        <section class="how-to-buy" id="how-to-buy">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-4">
                        <div class="wrapper-passed text-center">
                            <div class="content-svg-number">
                                <svg id="svg1" data-name="Capa 1" xmlns="http://www.w3.org/2000/svg"
                                    viewBox="0 0 39.8 100.3">
                                    <path class="cls-1" d="M216.8,198.1V175.9h38.8v99.2H230.7v-77Z"
                                        transform="translate(-216.3 -175.4)"
                                        style="stroke-dasharray: 276, 278; stroke-dashoffset: 0;"></path>
                                </svg>
                                <h2 class="passed text-uppercase font-bold wow zoomIn" data-wow-delay="1s">paso</h2>
                            </div>
                            <div class="info-passed">
                                <h2 class="title-passed text-uppercase font-bold">seleccione un producto</h2>
                                <p class="p-internas">Ingrese a la opción PRODUCTOS, elige los productos que desees y
                                    agrégalos a tu carrito de compras.</p>
                            </div>
                            <a href="#" class="btn-passed icon-play-circle"></a>

                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-4">
                        <div class="wrapper-passed text-center">
                            <div class="content-svg-number">
                                <svg id="svg2" data-name="Capa 1" xmlns="http://www.w3.org/2000/svg"
                                    viewBox="0 0 71.6 101.8">
                                    <path class="cls-1"
                                        d="M308,252.3q13.3-11.2,21.3-18.3a87.7,87.7,0,0,0,13.1-14.6c3.6-5,5.3-9.8,5.3-14.4s-.8-6.8-2.6-8.8-4.3-3.1-7.7-3.1c-7.1,0-10.8,5.1-11.1,15.2H303.2q.3-11.9,5.1-19.8a29.1,29.1,0,0,1,12.8-11.8,40.1,40.1,0,0,1,17.5-3.8c7.5,0,13.7,1.3,18.7,4a26,26,0,0,1,11.1,10.9A31.9,31.9,0,0,1,372,203c0,9.5-3.4,18.4-10.1,26.7s-15.4,16.4-25.9,24.6h37.8v19.4H303.5V255.9Z"
                                        transform="translate(-302.7 -172.4)"></path>
                                </svg>
                                <h2 class="passed text-uppercase font-bold wow zoomIn" data-wow-delay="1s">paso</h2>
                            </div>
                            <div class="info-passed">
                                <h2 class="title-passed text-uppercase font-bold">complete el formulario</h2>
                                <p class="p-internas">Completa todos los campos que aparecen en el formulario y presiona
                                    la opción pagar.</p>
                            </div>
                            <a href="#" class="btn-passed icon-play-circle"></a>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-4">
                        <div class="wrapper-passed text-center">
                            <div class="content-svg-number">
                                <svg id="svg3" data-name="Capa 1" xmlns="http://www.w3.org/2000/svg"
                                    viewBox="0 0 72.1 106.4">
                                    <path class="cls-1"
                                        d="M427.3,178.7c6.1-5.5,14.5-8.2,25-8.2a41.3,41.3,0,0,1,18,3.7,25.6,25.6,0,0,1,11.3,9.9,26.3,26.3,0,0,1,3.9,14.2c0,6.1-1.5,11.1-4.6,15a23.3,23.3,0,0,1-10.8,7.9v.6a25.6,25.6,0,0,1,12.6,8.8c3.1,4.1,4.6,9.4,4.6,15.8a28.6,28.6,0,0,1-4,15.3,26.4,26.4,0,0,1-11.6,10.5,40.6,40.6,0,0,1-18.1,3.8c-11.2,0-20.2-2.8-26.8-8.3s-10.2-13.8-10.6-24.9h23.2a14.3,14.3,0,0,0,3.5,9.7c2.3,2.4,5.5,3.6,9.8,3.6s6.5-1.1,8.5-3.2a11.4,11.4,0,0,0,3-8.3c0-4.5-1.4-7.7-4.3-9.7s-7.4-3-13.6-3h-4.5V212.4h4.5c4.8,0,8.6-.8,11.5-2.5s4.5-4.5,4.5-8.7-1-6-2.8-7.8a10.4,10.4,0,0,0-7.7-2.8c-3.6,0-6.2,1-8,3.2a14.6,14.6,0,0,0-3,8H417.5C417.8,191.8,421.1,184.1,427.3,178.7Z"
                                        transform="translate(-415.7 -170)"></path>
                                </svg>
                                <h2 class="passed text-uppercase font-bold wow zoomIn" data-wow-delay="1s">paso</h2>
                            </div>
                            <div class="info-passed">
                                <h2 class="title-passed text-uppercase font-bold">complete el pago</h2>
                                <p class="p-internas">Completa el sistema de pago con tu tarjeta de preferencia.</p>
                            </div>
                            <a href="#" class="btn-passed icon-play-circle"></a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
    <?php
        include 'src/includes/footer.php'
    ?>
    <script src="assets/js/form.js"></script>
    <script src="assets/js/libraries/vivus.min.js"></script>
    <script>
        var mysvg = new Vivus('svg1', {
            duration: 100,
            type: 'delayed',
            start: 'inViewport',
        }, function () {});
        var mysvg = new Vivus('svg2', {
            duration: 100,
            type: 'delayed',
            start: 'inViewport',
        }, function () {});
        var mysvg = new Vivus('svg3', {
            duration: 100,
            type: 'delayed',
            start: 'inViewport',
        }, function () {});
    </script>
</body>

</html>