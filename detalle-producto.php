<?php
    $show_menu_color = true;
    include 'src/includes/header.php'
?>
    <main class="details-products container-fluid px-0">
        <!-- <div class="sct-select-detail col-xs-12">
            <div class="wrapper-select-cart">
                <h2 class="font-bold title-slct-descr">ENCUENTRA TU LLANTA:</h2>
                <div class="button dropdown">
                    <select id="select-stores" class="font-regular text-uppercase">
                        <option value="medidaLlanta" class="stores"><span class="icon-llanta"></span>por Medida de llanta</option>
                        <option value="modeloAuto" class="stores"><i class="icon-auto"></i>por Modelo de Auto</option>
                    </select>
                </div>

                <div class="output col-xs-12 col-sm-4 px-0">
                    <div id="medidaLlanta" class="stores-info">
                        <div class="select-banner-one d-in-block">
                            <div class="select-filter">
                                <div class="wrapper-filter d-in-block">
                                    <div class="dropdown dropdown-selects">
                                        <div class="select">
                                            <span class="title-select font-regular"></span>
                                            <i class="icon-select icon-triangle-down"></i>
                                            <label class="form__label font-regular">
                                                ANCHO :
                                            </label>
                                        </div>
                                        <input type="hidden" name="gender" value="">
                                        <ul class="dropdown-menu font-regular">
                                            <li id="menor-mayor">125cms</li>
                                            <li id="mayor-menor">200cms</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="select-banner-medio d-in-block">
                            <div class="select-filter">
                                <div class="wrapper-filter d-in-block">
                                    <div class="dropdown dropdown-selects">
                                        <div class="select">
                                            <span class="title-select font-regular"></span>
                                            <i class="icon-select icon-triangle-down"></i>
                                            <label class="form__label font-regular">
                                                PERFIL :
                                            </label>
                                        </div>
                                        <input type="hidden" name="gender" value="Precio de menor a mayor">
                                        <ul class="dropdown-menu font-regular">
                                            <li id="menor-mayor">123</li>
                                            <li id="mayor-menor">3456</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="select-banner-medio d-in-block">
                            <div class="select-filter">
                                <div class="wrapper-filter d-in-block">
                                    <div class="dropdown dropdown-selects">
                                        <div class="select">
                                            <span class="title-select font-regular"></span>
                                            <i class="icon-select icon-triangle-down"></i>
                                            <label class="form__label font-regular">
                                                ARO :
                                            </label>
                                        </div>
                                        <input type="hidden" name="gender" value="Precio de menor a mayor">
                                        <ul class="dropdown-menu font-regular">
                                            <li id="menor-mayor">789</li>
                                            <li id="mayor-menor">9115</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <button class="btn-bsc-select"><i class="icon-Lupa"></i></button>
                    </div>
                    <div id="modeloAuto" class="stores-info">
                        <div class="select-banner-one d-in-block">
                            <div class="select-filter">
                                <div class="wrapper-filter d-in-block">
                                    <div class="dropdown dropdown-selects">
                                        <div class="select">
                                            <span class="title-select font-regular"></span>
                                            <i class="icon-select icon-triangle-down"></i>
                                            <label class="form__label font-regular">
                                                MARCA :
                                            </label>
                                        </div>
                                        <input type="hidden" name="gender" value="">
                                        <ul class="dropdown-menu font-regular">
                                            <li id="menor-mayor">125cms</li>
                                            <li id="mayor-menor">200cms</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="select-banner-medio d-in-block">
                            <div class="select-filter">
                                <div class="wrapper-filter d-in-block">
                                    <div class="dropdown dropdown-selects">
                                        <div class="select">
                                            <span class="title-select font-regular"></span>
                                            <i class="icon-select icon-triangle-down"></i>
                                            <label class="form__label font-regular">
                                                AÑO :
                                            </label>
                                        </div>
                                        <input type="hidden" name="gender" value="Precio de menor a mayor">
                                        <ul class="dropdown-menu font-regular">
                                            <li id="menor-mayor">123</li>
                                            <li id="mayor-menor">3456</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="select-banner-medio d-in-block">
                            <div class="select-filter">
                                <div class="wrapper-filter d-in-block">
                                    <div class="dropdown dropdown-selects">
                                        <div class="select">
                                            <span class="title-select font-regular"></span>
                                            <i class="icon-select icon-triangle-down"></i>
                                            <label class="form__label font-regular">
                                                MODELO :
                                            </label>
                                        </div>
                                        <input type="hidden" name="gender" value="Precio de menor a mayor">
                                        <ul class="dropdown-menu font-regular">
                                            <li id="menor-mayor">789</li>
                                            <li id="mayor-menor">9115</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <button class="btn-bsc-select"><i class="icon-Lupa"></i></button>
                    </div>
                </div>
            </div>
        </div> -->
        <section class="sct-detail-product">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 bread-detail">
                        <ol class="breadcrumb font-regular text-uppercase">
                            <li class="bread-item"><a class="bread-link" href="#">INICIO</a><span class="icon-bread-flec"></span></li>
                            <li class="bread-item"><a class="bread-link" href="#">Llantas</a><span class="icon-bread-flec"></span></li>
                            <li class="bread-item active"><a class="bread-link" href="#">bridgestone</a></li>
                        </ol>
                    </div>
                    <!-- img-detalle-producto -->
                    <div class="col-xs-12 col-md-6">
                        <div class="img-big-detail-product wow zoomIn">
                            <img src="assets/images/productos/llantas.png" alt="" class="img-dproduct">
                            <div class="circle-dsct">
                                <span class="font-regular">-20%</span>
                            </div>
                        </div>
                    </div>
                    <!-- detalle producto -->
                    <div class="col-xs-12 col-md-6 wow slideInRight">
                        <div class="row">
                            <div class="col-xs-10">
                                <img src="assets/images/logos/bridgestone.jpg" alt="" class="img-detail-product-brand">
                                <h2 class="font-bold text-uppercase my-0">195/65r15 91h</h2>
                                <h3 class="marca-descrp font-bold text-uppercase my-0">dueler g/t 684</h3>
                                <span class="p-internas">Código: 0001666</span>
                                <p class="p-internas text-dscr-cond">Diseñado para conductores entusiastas que desean una combinación
                                    look y rendimiento, destacando la maniobrabilidad y tracción en suelos secos y
                                    mojados.</p>
                            </div>
                            <div class="col-xs-12">
                                <div class="wrapper-select-cart">
                                    <div class="button dropdown col-xs-12 col-sm-6 col-md-8 col-lg-6">
                                        <select id="select-stores" class="font-regular">
                                            <option>SELECCIONE UNA TIENDA</option>
                                            <option value="benavides" class="stores">benavides</option>
                                            <option value="magdalena" class="stores">magdalena</option>
                                        </select>
                                    </div>

                                    <div class="output col-xs-12 px-0">
                                        <div id="benavides" class="stores-info">
                                            <div class="ubication-stores">
                                                <h2 class="name-stores font-bold color-primary">EKONO BENAVIDES</h2>
                                                <span class="distrito-stores font-bold">SURCO</span>
                                                <p class="p-internas mb-0">Av. Prolongación Benavides N° 3999(A media
                                                    cdra
                                                    cruce con Av. Ayacucho).</p>
                                                <p class="p-internas">Horario de atención de 8:00am a 6:00pm de lunes a
                                                    sábado.</p>
                                            </div>
                                            <div class="modal-map modal-">
                                                <button class="button font-bold ver-map" data-target="modal1"><i
                                                        class="icon-ubc color-primary"></i>VER MAPA</button>
                                                <div class="modal" id="modal1">
                                                    <div class="modal_inner">
                                                        <div class="modal_header font-bold">EKONO BENAVIDES</div>
                                                        <div class="modal_body">
                                                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3900.7659263065407!2d-76.99596388464805!3d-12.128161746723425!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x9105c7f604a64219%3A0x195b83fd19af1c28!2sAv.%20Alfredo%20Benavides%203999%2C%20Cercado%20de%20Lima%2015038!5e0!3m2!1ses-419!2spe!4v1574886848558!5m2!1ses-419!2spe"
                                                            frameborder="0" style="border:0;" allowfullscreen=""></iframe>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="div-alert">
                                                <p class="p-internas">Stock no disponible para este local, le
                                                    agradeceremos
                                                    seleccione otra tienda, de lo contrario se le recargará el monto de
                                                    traslado y los productos serán llevadas al local en un máximo de 48
                                                    horas.</p>
                                            </div>
                                        </div>
                                        <div id="magdalena" class="stores-info">
                                            <div class="ubication-stores">
                                                <h2 class="name-stores font-bold color-primary">EKONO primavera</h2>
                                                <span class="distrito-stores font-bold">surco</span>
                                                <p class="p-internas mb-0">Av. Primavera Nº 1195 (Alt. Pte. Primavera)
                                                </p>
                                                <p class="p-internas">Horario de atención de 8:00am a 6:00pm de lunes a
                                                    sábado.</p>
                                            </div>
                                            <div class="modal-map modal-">
                                                <button class="button font-bold ver-map" data-target="modal2"><i
                                                        class="icon-ubc color-primary"></i>VER MAPA</button>
                                                <div class="modal" id="modal2">
                                                    <div class="modal_inner">
                                                        <div class="modal_header font-bold">EKONO BENAVIDES</div>
                                                        <div class="modal_body">
                                                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3900.7659263065407!2d-76.99596388464805!3d-12.128161746723425!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x9105c7f604a64219%3A0x195b83fd19af1c28!2sAv.%20Alfredo%20Benavides%203999%2C%20Cercado%20de%20Lima%2015038!5e0!3m2!1ses-419!2spe!4v1574886848558!5m2!1ses-419!2spe"
                                                            frameborder="0" style="border:0;" allowfullscreen=""></iframe>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <div class="btn-comprar-detail col-xs-12">
                                    <div class="scr2">
                                        <div class="scrl">
                                            <div class="wrapper-price flex-al-cent sp-arrow float-left">
                                                <span class="font-regular price-dsct-prod">S/720.00</span>
                                                <h2 class="font-bold price-prod">S/ 720.00</h2>
                                            </div>
                                            <div class="add-purchase d-flex float-right">
                                                <div class="qty mt-5">
                                                    <span class="minus bg-dark">-</span>
                                                    <input type="number" class="count font-regular" name="qty" value="1">
                                                    <span class="plus bg-dark">+</span>
                                                </div>
                                                <div class="btn-purchase">
                                                    <a href="#"
                                                        class="link-purchase font-bold text-uppercase text-center">comprar <i class="icon-carrito"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tabs-info-product col-xs-12 px-0">
                                    <div class="tabs text-uppercase">
                                        <span class="font-regular tab-link current" data-tab="descripcion">Descripción</span>
                                        <span class="font-regular tab-link" data-tab="accesorios">accesorios</span>
                                        <span class="font-regular tab-link" data-tab="videos">videos</span>
                                    </div>

                                    <div class="content">
                                        <div id="descripcion" class="tab-content current">
                                            <ul>
                                                <li class="list-descr-detall"> 
                                                    <div class="color-primary font-bold float-left">Código</div>
                                                    <span class="font-regular">000111</span>
                                                </li>
                                                <li class="list-descr-detall"> 
                                                    <div class="color-primary font-bold float-left">Marca</div>
                                                    <span class="font-regular">xxxxxx</span>
                                                </li>
                                                <li class="list-descr-detall"> 
                                                    <div class="color-primary font-bold float-left">Modelo</div>
                                                    <span class="font-regular">xxxxxx</span>
                                                </li>
                                                <li class="list-descr-detall"> 
                                                    <div class="color-primary font-bold float-left">Medida</div>
                                                    <span class="font-regular">000111</span>
                                                </li>
                                            </ul>
                                        </div>
                                        <div id="accesorios" class="tab-content">
                                            <p class="p-internas">2Lorem ipsum dolor sit amet consectetur adipisicing elit. Asperiores aut 
                                                consequuntur ut quidem soluta mollitia! Deserunt est voluptatum ut nemo amet quidem ab iusto dolores, 
                                                omnis atque modi iste enim.</p>
                                        </div>
                                        <div id="videos" class="tab-content">
                                            <p class="p-internas">3Lorem ipsum dolor sit amet consectetur adipisicing elit. Asperiores aut 
                                                consequuntur ut quidem soluta mollitia! Deserunt est voluptatum ut nemo amet quidem ab iusto dolores, 
                                                omnis atque modi iste enim.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="product-related">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <h2 class="title-product-related font-bold text-center">PRODUCTOS RELACIONADOS</h2>
                    </div>
                    <div class="col-xs-12">
                        <div class="row">
                            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                                <div class="xhov wow zoomIn">
                                    <div class="wrapper-card-product">
                                        <a href="detalle-producto.php" class="link-wrapper-prod h100 w100 d-block">
                                            <div class="wrapper-img-prod h-auto">
                                                <div class="content-img-prod">
                                                    <img class="w-100" src="assets/images/productos/llantas2.jpg" alt="">
                                                </div>
                                            </div>
                                            <div class="info-product text-uppercase">
                                                <h2 class="font-bold name-cod-prod">235/45R18</h2>
                                                <h3 class="font-regular name-brand">DARWIN</h3>
                                                <div class="wrapper-price flex-al-cent sp-arrow">
                                                    <h2 class="font-bold price-prod"><span class="font-regular price-dsct-prod">S/720.00</span> S/ 720.00</h2>
                                                </div>
                                            </div>
                                        </a>
                                        <div class="dif">
                                            <div class="add-purchase ad2b d-flex">
                                                <div class="qty mt-5">
                                                    <span class="plus bg-dark">+</span>
                                                    <input type="number" class="count font-regular" name="qty" value="1">
                                                    <span class="minus bg-dark">-</span>
                                                </div>
                                                <div class="btn-purchase">
                                                    <a href="carrito-de-compras.php" class="link-purchase font-bold text-uppercase text-center">comprar <i class="icon-carrito"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="circle-dscto d-flex">
                                            <h4 class="font-regular">-20%</h4>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                                <div class="xhov wow zoomIn">
                                    <div class="wrapper-card-product">
                                        <a href="detalle-producto.php" class="link-wrapper-prod h100 w100 d-block">
                                            <div class="wrapper-img-prod h-auto">
                                                <div class="content-img-prod">
                                                    <img class="w-100" src="assets/images/productos/llantas2.jpg" alt="">
                                                </div>
                                            </div>
                                            <div class="info-product text-uppercase">
                                                <h2 class="font-bold name-cod-prod">235/45R18</h2>
                                                <h3 class="font-regular name-brand">DARWIN</h3>
                                                <div class="wrapper-price flex-al-cent sp-arrow">
                                                    <h2 class="font-bold price-prod"><span class="font-regular price-dsct-prod">S/720.00</span> S/ 720.00</h2>
                                                </div>
                                            </div>
                                        </a>
                                        <div class="dif">
                                            <div class="add-purchase ad2b d-flex">
                                                <div class="qty mt-5">
                                                    <span class="plus bg-dark">+</span>
                                                    <input type="number" class="count font-regular" name="qty" value="1">
                                                    <span class="minus bg-dark">-</span>
                                                </div>
                                                <div class="btn-purchase">
                                                    <a href="carrito-de-compras.php" class="link-purchase font-bold text-uppercase text-center">comprar <i class="icon-carrito"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="circle-dscto d-flex">
                                            <h4 class="font-regular">-20%</h4>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                                <div class="xhov wow zoomIn">
                                    <div class="wrapper-card-product">
                                        <a href="detalle-producto.php" class="link-wrapper-prod h100 w100 d-block">
                                            <div class="wrapper-img-prod h-auto">
                                                <div class="content-img-prod">
                                                    <img class="w-100" src="assets/images/productos/llantas2.jpg" alt="">
                                                </div>
                                            </div>
                                            <div class="info-product text-uppercase">
                                                <h2 class="font-bold name-cod-prod">235/45R18</h2>
                                                <h3 class="font-regular name-brand">DARWIN</h3>
                                                <div class="wrapper-price flex-al-cent sp-arrow">
                                                    <h2 class="font-bold price-prod"><span class="font-regular price-dsct-prod">S/720.00</span> S/ 720.00</h2>
                                                </div>
                                            </div>
                                        </a>
                                        <div class="dif">
                                            <div class="add-purchase ad2b d-flex">
                                                <div class="qty mt-5">
                                                    <span class="plus bg-dark">+</span>
                                                    <input type="number" class="count font-regular" name="qty" value="1">
                                                    <span class="minus bg-dark">-</span>
                                                </div>
                                                <div class="btn-purchase">
                                                    <a href="carrito-de-compras.php" class="link-purchase font-bold text-uppercase text-center">comprar <i class="icon-carrito"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="circle-dscto d-flex">
                                            <h4 class="font-regular">-20%</h4>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                                <div class="xhov wow zoomIn">
                                    <div class="wrapper-card-product">
                                        <a href="detalle-producto.php" class="link-wrapper-prod h100 w100 d-block">
                                            <div class="wrapper-img-prod h-auto">
                                                <div class="content-img-prod">
                                                    <img class="w-100" src="assets/images/productos/llantas2.jpg" alt="">
                                                </div>
                                            </div>
                                            <div class="info-product text-uppercase">
                                                <h2 class="font-bold name-cod-prod">235/45R18</h2>
                                                <h3 class="font-regular name-brand">DARWIN</h3>
                                                <div class="wrapper-price flex-al-cent sp-arrow">
                                                    <h2 class="font-bold price-prod"><span class="font-regular price-dsct-prod">S/720.00</span> S/ 720.00</h2>
                                                </div>
                                            </div>
                                        </a>
                                        <div class="dif">
                                            <div class="add-purchase ad2b d-flex">
                                                <div class="qty mt-5">
                                                    <span class="plus bg-dark">+</span>
                                                    <input type="number" class="count font-regular" name="qty" value="1">
                                                    <span class="minus bg-dark">-</span>
                                                </div>
                                                <div class="btn-purchase">
                                                    <a href="carrito-de-compras.php" class="link-purchase font-bold text-uppercase text-center">comprar <i class="icon-carrito"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="circle-dscto d-flex">
                                            <h4 class="font-regular">-20%</h4>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
    <?php
        include 'src/includes/footer.php'
    ?>
    <script src="assets/js/counting.js"></script>
    <script>
        $('#select-stores').change(function () {
            $('.stores-info').hide();
            $('#' + $(this).val()).show();
        });
        $(".button[data-target]").click(function() {
            $("#" + this.dataset.target).toggleClass("-open")
            })

            $(".modal").click(function(e) {
            if (e.target === this) {
                $(this).toggleClass("-open")
            }
        });
    </script>
    <script>
        $(document).ready(function(){
            $('.tabs span').click(function(){
                var tab_id = $(this).attr('data-tab');

                $('.tabs span').removeClass('current');
                $('.tab-content').removeClass('current');

                $(this).addClass('current');
                $("#"+tab_id).addClass('current');
            })
        })
    </script>
    <script>
        $(document).ready(function () {
            $(window).scroll(function () {
                var scrollTop = $(window).scrollTop();
                if (scrollTop > 450) {
                    $('.btn-comprar-detail').addClass('btn-comprar-scroll');

                } else {
                    $('.btn-comprar-detail').removeClass('btn-comprar-scroll');
                }
            });
        });
        
    </script>
</body>

</html>