<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>EKONOLLANTAS</title>
    <link rel="shortcut icon" type="image/x-icon" href="assets/images/logos/favicon.png" />
    <link rel="stylesheet" href="assets/css/app.css">
    <link rel="stylesheet" href="assets/css/app_copia.css">
    
</head>

<body>
    <?php 
        $uriSegments = explode("/", parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH));    
    ?>
    <div class="wrapper-header <?= isset($show_menu_color) ? "header-scroll" :  "scroll" ?>">
        <header class="container-fluid">
            <div class="container">
                <div class="row">
                    <div class="content-logo float-left">
                    <a href="index.php">
                        <img src="assets/images/logos/logoB.png" alt="" class="img-logo logo-dskt">
                        <img src="assets/images/logos/logoC.png" alt="" class="img-logo logo-mob">
                    </a>
                        
                    </div>
                    <div class="menus float-right">
                        <div class="navbar-top hidden-xs hidden-sm">
                            <a href="registro.html" class="link-nav-top font-regular d-in-block">Registrate</a>
                            <a href="inicio-sesion.html" class="link-nav-top font-regular d-in-block">Iniciar Sesión</a>
                            <div class="social-network-header d-in-block">
                                <i class="icon-rs-header icon-facebook"></i>
                                <i class="icon-rs-header icon-instagram"></i>
                                <i class="icon-rs-header icon-youtube"></i>
                            </div>
                            <a href="tel:01941242206" class="hidden-xs hidden-sm link-nav-phones font-regular d-in-block"><i
                                    class="icon-phone"></i>01 2194650</a>
                            <a href="tel:01941242206" class="hidden-xs hidden-sm link-nav-phones font-regular d-in-block"><i
                                    class="icon-wts"></i>(+51) 941242206</a>
                        </div>
                        <div class="navbar-principal">
                            <ul class="nav-list">
                                <li class="hidden-xs hidden-sm nav-item float-left <?= in_array('llantas.php', $uriSegments ) ? 'active' : ''; ?>"><a href="llantas.php"
                                        class="nav-link font-regular text-uppercase float-left">llantas</a></li>
                                <li class="hidden-xs hidden-sm nav-item float-left <?= in_array('aros.php', $uriSegments ) ? 'active' : ''; ?>"><a href="aros.php"
                                        class="nav-link font-regular text-uppercase float-left">aros</a></li>
                                <li class="hidden-xs hidden-sm nav-item float-left <?= in_array('baterias.php', $uriSegments ) ? 'active' : ''; ?>"><a href="baterias.php"
                                        class="nav-link font-regular text-uppercase float-left">baterías</a></li>
                                <li class="hidden-xs hidden-sm nav-item float-left <?= in_array('repuestos-y-otros.php', $uriSegments ) ? 'active' : ''; ?>"><a href="repuestos-y-otros.php"
                                        class="nav-link font-regular text-uppercase float-left">repuestos / otros</a>
                                </li>
                                <li class="hidden-xs hidden-sm nav-item float-left <?= in_array('servicios.php', $uriSegments ) ? 'active' : ''; ?>"><a href="nuestros-servicios.php"
                                        class="nav-link font-regular text-uppercase float-left">servicios</a></li>
                                <li class="hidden-xs hidden-sm nav-item float-left <?= in_array('tiendas.php', $uriSegments ) ? 'active' : ''; ?>"><a href="nuestras-tiendas.php"
                                        class="nav-link font-regular text-uppercase float-left">tiendas</a></li>
                                <div class="icons-bsc-car">
                                    <a href="#" class="icon-Lupa buscar"></a>
                                    <a href="carrito-de-compras.php" class="icon-carrito cr-mob"></a>
                                </div>
                            </ul>
                            <div class="buscador">
                                <div class="contenedor">
                                    <input type="text" />
                                    <span class="cerrar font-bold">X</span>
                                    <button class="font-bold">BUSCAR</button>
                                </div>
                            </div>
                        </div>
                        
                        <i id="menu-btn" class="icon-hamb-open"></i>
                    </div>
                </div>
            </div>
            <div id="navbar-right">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12">
                            <a href="registro.html" class="link-nav-top font-regular d-in-block">Registrate</a>
                            <a href="inicio-sesion.html" class="link-nav-top font-regular d-in-block">Iniciar Sesión</a>
                        </div>
                        <div class="col-xs-12">
                            <ul class="row list-big-nav-right">
                                <li class="item-big col-xs-5 col-sm-6 font-bold text-uppercase <?= in_array('llantas.php', $uriSegments ) ? 'active' : ''; ?>"><a class="link-big" href="llantas.php"
                                        class="color-primary">llantas</a></li>
                                <li class="item-big col-xs-7 col-sm-6 font-bold text-uppercase <?= in_array('aros.php', $uriSegments ) ? 'active' : ''; ?>"><a class="link-big" href="baterias.php"
                                        class="color-primary">baterías</a></li>
                                <li class="item-big col-xs-5 col-sm-6 font-bold text-uppercase <?= in_array('aros.php', $uriSegments ) ? 'active' : ''; ?>"><a class="link-big" href="aros.php"
                                        class="color-primary">aros</a></li>
                                <li class="item-big col-xs-7 col-sm-6 font-bold text-uppercase <?= in_array('repuestos-y-otros.php', $uriSegments ) ? 'active' : ''; ?>"><a class="link-big" href="repuestos-y-otros.php"
                                        class="color-primary">Repuestos / otros</a></li>
                            </ul>
                        </div>
                        <div class="col-xs-12">
                            <ul class="row list2-nav-right">
                                <li class="item-nav-right col-xs-12 col-sm-6 font-regular <?= in_array('servicios.php', $uriSegments ) ? 'active' : ''; ?>"><a class="link-nav-right"
                                        href="nuestros-servicios.php">Servicios</a></li>
                                <li class="item-nav-right col-xs-12 col-sm-6 font-regular <?= in_array('que-saber.php', $uriSegments ) ? 'active' : ''; ?>"><a class="link-nav-right"
                                        href="que-saber.php">Qué saber</a></li>
                                <li class="item-nav-right col-xs-12 col-sm-6 font-regular <?= in_array('tiendas.php', $uriSegments ) ? 'active' : ''; ?>"><a class="link-nav-right"
                                        href="nuestras-tiendas.php">Tiendas</a></li>
                                <li class="item-nav-right col-xs-12 col-sm-6 font-regular <?= in_array('formas-de-pago.php', $uriSegments ) ? 'active' : ''; ?>"><a class="link-nav-right"
                                        href="formas-de-pago.php">Formas de pago</a></li>
                                <li class="item-nav-right col-xs-12 col-sm-6 font-regular <?= in_array('nosotros.php', $uriSegments ) ? 'active' : ''; ?>"><a class="link-nav-right"
                                        href="nosotros.php">Nosotros</a></li>
                                <li class="item-nav-right col-xs-12 col-sm-6 font-regular <?= in_array('garantias.php', $uriSegments ) ? 'active' : ''; ?>"><a class="link-nav-right"
                                        href="garantias.php">Garantías y devoluciones</a></li>
                                <li class="item-nav-right col-xs-12 col-sm-6 font-regular <?= in_array('pedido-especial.php', $uriSegments ) ? 'active' : ''; ?>"><a class="link-nav-right"
                                        href="pedido-especial.php">Pedido especial</a></li>
                                <li class="item-nav-right col-xs-12 col-sm-6 font-regular <?= in_array('terminos-y-condiciones.php', $uriSegments ) ? 'active' : ''; ?>"><a class="link-nav-right"
                                        href="terminos-y-condiciones.php">Términos y condiciones</a></li>
                                <li class="item-nav-right col-xs-12 col-sm-6 font-regular <?= in_array('liquidacion.php', $uriSegments ) ? 'active' : ''; ?>"><a class="link-nav-right"
                                        href="liquidacion.php">Liquidación</a></li>
                                <li class="item-nav-right col-xs-12 col-sm-6 font-regular <?= in_array('trabaja-con-nosotros.php', $uriSegments ) ? 'active' : ''; ?>"><a class="link-nav-right"
                                        href="trabaja-con-nosotros.php">Trabaja con nosotros</a></li>
                                <li class="item-nav-right col-xs-12 col-sm-6 font-regular <?= in_array('como-comprar.php', $uriSegments ) ? 'active' : ''; ?>"><a class="link-nav-right"
                                        href="como-comprar.php">¿Cómo comprar?</a></li>
                                <li class="item-nav-right col-xs-12 col-sm-6 font-regular <?= in_array('contactanos.php', $uriSegments ) ? 'active' : ''; ?>"><a class="link-nav-right"
                                        href="contactanos.php">Contáctanos</a></li>
                            </ul>
                        </div>
                        <div class="col-xs-12">
                            <div class="row">
                                <div class="wrapper-phones-navr">
                                    <a class="a-navr d-in-block font-regular" href="#"><i class="icon-phone"></i>01
                                        2194650</a>
                                    <a class="a-navr d-in-block font-regular" href="#"><i class="icon-wts"></i>(+51)
                                        941242206</a>
                                </div>
                                <div class="wrapper-redsocials-r">
                                    <a href="#" class="a-navr icon-facebook"></a>
                                    <a href="#" class="a-navr icon-instagram"></a>
                                    <a href="#" class="a-navr icon-youtube"></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>
    </div>
