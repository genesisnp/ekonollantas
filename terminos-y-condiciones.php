<?php
    include 'src/includes/header.php'
?>
    <main class="main-">
        <section class="sct-banner">
            <div class="degrade-int"></div>
            <img class="img-banner" src="assets/images/banner/nuestros-servicios.jpg" alt="">
            <div class="content-text-banner">
                <i class="icon-banner icon-term-y-cond"></i>
                <h2 class="title-banner text-uppercase font-bold">términos y condiciones</h2>
            </div>
            <a href="#terms-and-conditions" class="icon-arrow" data-ancla="termn-and-conditions"></a>
        </section>
        <section class="terms-and-conditions" id="terms-and-conditions">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <ul class="accordion">
                            <li><a class="accordion-heading font-bold">Presentación <i
                                        class="icon-flecha-accord"></i></a>
                                <div class="accordion-content">
                                    <p class="p-internas">¡EKONO llantas te da la bienvenida!</p>
                                    <p class="p-internas">Hemos implementado la venta en línea para facilitar la compra
                                        nuestros productos, te recomendamos ver los Términos y Condiciones creados con
                                        el fin de brindar información necesaria para tu total satisfacción.</p>
                                </div>
                            </li>
                            <li><a class="accordion-heading font-bold">Disposiciones Generales <i
                                        class="icon-flecha-accord"></i></a>
                                <div class="accordion-content">
                                    <p class="p-internas">Los siguientes términos y condiciones de venta, en adelante
                                        “Términos y Condiciones”, se aplicaran a la venta a través de
                                        www.EKONOllantas.com o llamado Sitio Web, los mismos que son aceptados, y pueden
                                        actualizarse y modificarse, por este motivo recomendamos revisarlos cada vez que
                                        realices una compra, así mismo expresamos que nos regimos bajo la legislación
                                        del Perú.</p>
                                    <p class="p-internas">La compra a través de este sitio web solo puede ser realizada
                                        por personas mayores de edad (18 años).</p>
                                </div>
                            </li>
                            <li><a class="accordion-heading font-bold">Derecho y Protección del Consumidor <i
                                        class="icon-flecha-accord"></i></a>
                                <div class="accordion-content">
                                    <p class="p-internas">Al comprar a través de este Sitio Web, recibes todos los
                                        derechos reconocidos con la legislación sobre protección al consumidor vigente
                                        en el Perú, tales como información, rectificación, uso de libro de reclamaciones
                                        y cancelaciones de tus datos personales, conforme a la Ley Nº29733 sobre
                                        protección de datos de carácter personal, así mismo podrás navegar por este
                                        Sitio Web sin obligación de realizar alguna compra.</p>
                                </div>
                            </li>
                            <li><a class="accordion-heading font-bold">Identificación de EKONO llanta <i
                                        class="icon-flecha-accord"></i></a>
                                <div class="accordion-content">
                                    <p class="p-internas">Nuestros datos legales:</p>
                                    <ul class="list2">
                                        <li class="p-internas">Razón Social: IZOO S.A.</li>
                                        <li class="p-internas">Nombre Comercial: EKONO llantas</li>
                                        <li class="p-internas">Ruc: 20100310369</li>
                                        <li class="p-internas">Domicilio Fiscal: Av. La Marina 1200 Pueblo Libre.</li>
                                    </ul>
                                </div>
                            </li>
                            <li><a class="accordion-heading font-bold">Contacto <i class="icon-flecha-accord"></i></a>
                                <div class="accordion-content">
                                    <p class="p-internas">Para cualquier consulta envíanos un correo a
                                        <strong class="color-primary">compraenlinea@ekonollantas.com</strong>, o recibe
                                        asistencia telefónica llamando al
                                        2194666.</p>
                                </div>
                            </li>
                            <li><a class="accordion-heading font-bold">Registro <i class="icon-flecha-accord"></i></a>
                                <div class="accordion-content">
                                    <p class="p-internas">Para comprar en <a href="http://ekonollantas.com"><strong
                                                class="color-primary">www.EKONOllantas.com</strong></a> es necesario
                                        registrarse llenando un formulario simple, siendo una obligación llenar todos
                                        los campos.</p>
                                </div>
                            </li>
                            <li><a class="accordion-heading font-bold">Autorización de uso de los datos personales <i
                                        class="icon-flecha-accord"></i></a>
                                <div class="accordion-content">
                                    <p class="p-internas">Al registrarte, aceptas y autorizas el envío de promociones y
                                        otras novedades a tu correo electrónico, pudiendo elegir renunciar a recibir
                                        este tipo de información ("Tus datos estarán seguros y no serán compartidos con
                                        nadie").</p>
                                </div>
                            </li>
                            <li><a class="accordion-heading font-bold">Productos <i class="icon-flecha-accord"></i></a>
                                <div class="accordion-content">
                                    <p class="p-internas">EKONO llantas como empresa responsable, se compromete a
                                        adoptar las precauciones necesarias para asegurar que todos los detalles,
                                        descripción e imágenes de productos que aparecen en este Sitio Web sean
                                        correctos en el momento de ingresarlos en el sistema, nos obstante, en la medida
                                        en que lo permita la legislación aplicable, no garantizamos que dichos detalles,
                                        descripciones e imágenes de productos sean exactos, completos, fiables,
                                        actualizados o libres de errores.</p>
                                </div>
                            </li>
                            <li><a class="accordion-heading font-bold">Stock y Disponibilidad <i
                                        class="icon-flecha-accord"></i></a>
                                <div class="accordion-content">
                                    <p class="p-internas">Los productos que se comercializan en este Sitio Web están
                                        sujetos a disponibilidad de stock, siendo nuestra principal preocupación el
                                        mantener la información actualizada, sin embargo, no estamos libres encontrarnos
                                        con algún inconveniente en el momento de traslado de mercadería de nuestros
                                        almacenes u otra situación ajena a nuestros procedimientos, si esto sucediera,
                                        nos comunicaremos contigo para encontrar una pronta solución a cualquier
                                        incomodidad causada.</p>
                                </div>
                            </li>
                            <li><a class="accordion-heading font-bold">Precio de los Productos <i
                                        class="icon-flecha-accord"></i></a>
                                <div class="accordion-content">
                                    <p class="p-internas">Los precios indicados en este Sitio Web están expresados en
                                        Nuevos Soles e incluyen el IGV.</p>
                                    <p class="p-internas">Revisamos regularmente los precios, no obstante, en la medida
                                        que lo permita la legislación aplicable, no podemos garantizar la ausencia de
                                        errores, siendo así, brindaremos la oportunidad de comprar el producto al precio
                                        correcto o cancelar el pedido.</p>
                                    <p class="p-internas">Los precios publicitados en esta página web son unitarios,
                                        incluyen el IGV. Los precios de las llantas no incluyen aros. No acumulable ni
                                        validos con otras promociones.</p>
                                </div>
                            </li>
                            <li><a class="accordion-heading font-bold">Medios de Pago <i
                                        class="icon-flecha-accord"></i></a>
                                <div class="accordion-content">
                                    <p class="p-internas">Los productos ofrecidos en este Sitio Web, salvo se presente
                                        un caso excepcional, se podrán pagar de la siguiente forma:</p>
                                    <p class="p-internas">Si el pago se realizó con depósito o trasferencia a nuestras
                                        cuentas, se emitirá un cheque no negociable, en un plazo de 07 días hábiles.</p>
                                    <h3 class="subtitle-heading2 font-bold">Pago con tarjeta de crédito o débito Visa
                                    </h3>
                                    <p class="p-internas">La utilización de este medio de pago se condiciona a los
                                        procesos de confirmación y validación de la entidad financiera emisora.</p>
                                    <p class="p-internas">Se podrá pagar con tarjetas de crédito o débito, emitidas por
                                        bancos en el Perú y bancos extranjeros, si el pago es con tarjeta de un banco
                                        fuera del Perú se deberá adjuntar información adicional como:</p>
                                    <ul class="list2">
                                        <li class="p-internas">Copia de la tarjeta por ambos lados.</li>
                                        <li class="p-internas">Copia del DNI o Ruc del propietario de la tarjeta y/o
                                            carnet de extranjería.</li>
                                    </ul>
                                    <p class="p-internas">Es de exclusiva responsabilidad del cliente el elegir
                                        cualquiera de estos medios de pago, el cual se debe de regir bajo los Términos y
                                        Condiciones de la entidad emisora del medio de pago.</p>
                                    <p class="p-internas">Nos reservamos el derecho de cancelar cualquier pedido que no
                                        satisfaga estos criterios.</p>
                                </div>
                            </li>
                            <li><a class="accordion-heading font-bold">Emisión del comprobante de pago <i
                                        class="icon-flecha-accord"></i></a>
                                <div class="accordion-content">
                                    <p class="p-internas">Por las compras realizadas, los clientes recibirán un
                                        comprobante de pago (boleta o factura de venta), habiendo escogido uno de estos.
                                        No podrán ser cambiados posteriormente.</p>
                                </div>
                            </li>
                            <li><a class="accordion-heading font-bold">Entrega de Productos <i
                                        class="icon-flecha-accord"></i></a>
                                <div class="accordion-content">
                                    <p class="p-internas">Todos los productos adquiridos en este Sitio Web se entregarán
                                        en las tiendas EKONO llantas, previa coordinación con los clientes.</p>
                                    <p class="p-internas">Una vez pasada la fecha límite de recojo establecida en la
                                        constancia de compra web, de no haber realizado el pago de los productos, le
                                        reserva quedará automáticamente anulada y los producto quedarán disponibles para
                                        la venta al público, pudiendo variar el precio y la disponibilidad de stock.</p>
                                </div>
                            </li>
                            <li><a class="accordion-heading font-bold">Retracto, Cambios o Devoluciones <i
                                        class="icon-flecha-accord"></i></a>
                                <div class="accordion-content">
                                    <p class="p-internas">¡Tu Satisfacción es lo primero!</p>
                                    <p class="p-internas">Si no estás conforme con el producto, lo cambiamos o te
                                        reintegramos el pago, deberás comunicarte en un máximo de 7 días hábiles a
                                        partir de haber recibido los productos (no aplica para compras por importación
                                        directa ni para compras de llantas de vehículos de carga o transporte como bus,
                                        camión u OTR).</p>
                                    <p class="p-internas">Los productos deben de estar en perfecto estado, sin uso, como
                                        cuando los entregamos y no deben haberse instalados.</p>
                                    <p class="p-internas">Es muy importante recibir tu comunicación para gestionar este
                                        cambio o devolución.</p>
                                    <p class="p-internas">Envíanos un correo a <strong
                                            class="color-primary">compraenlinea@ekonollantas.com</strong>, o recibe
                                        asistencia telefónica llamando al 2194650.</p>
                                </div>
                            </li>
                            <li><a class="accordion-heading font-bold">Integro del Pago <i
                                        class="icon-flecha-accord"></i></a>
                                <div class="accordion-content">
                                    <p class="p-internas">Si el pago se realizó con tarjeta de crédito, el plazo
                                        establecido es de 15 días hábiles.</p>
                                    <p class="p-internas">Si el pago se realizó con depósito o trasferencia a nuestras
                                        cuentas, se emitirá un cheque no negociable, en un plazo de 07 días hábiles.</p>
                                    <h3 class="subtitle-heading2 font-bold">Tener en cuenta :</h3>
                                    <ul>
                                        <li class="p-internas">Si compraste con boleta de venta el cheque se emitirá con
                                            el nombre de quién realizó la compra (los datos de la boleta de venta), no
                                            se podrá emitir a nombre de otra persona o a nombre de una empresa.</li>
                                        <li class="p-internas">Si compraste con factura el cheque se emitirá con el
                                            nombre de la empresa, no se podrá emitir a nombre de otra empresa o a nombre
                                            personal.</li>
                                    </ul>
                                    <p class="p-internas">¡Estaremos gustosos de atenderte!</p>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>
    </main>
    <?php
        include 'src/includes/footer.php'
    ?>
</body>

</html>