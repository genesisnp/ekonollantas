<?php
    include 'src/includes/header.php'
?>
    <main class="main-">
        <section class="sct-banner">
            <div class="degrade-int"></div>
            <img class="img-banner" src="assets/images/banner/nuestros-servicios.jpg" alt="">
            <div class="content-text-banner">
                <i class="icon-banner icon-nosotros"></i>
                <h2 class="title-banner text-uppercase font-bold">repuestos</h2>
                <p class="p-internas">Lorem ipsum dolor sit amet consectetur, adipisicing elit. Impedit vitae minus,
                    in odit laboriosam repellat pariatur? Odit, blanditiis amet. Ab eum iste distinctio dolores</p>
            </div>
            <ol class="breadcrumb font-regular text-uppercase">
                <li class="bread-item"><a class="bread-link" href="#">INICIO</a></li>
                <li class="bread-item active"><a class="bread-link" href="#">repuestos / otros</a></li>
            </ol>
            <a href="#spare-parts" class="icon-arrow" data-ancla="spare-parts"></a>
        </section>
        <section class="spare-parts" id="spare-parts" name="spare-parts">
            <div class="container">
                <div class="row">
                    <!--nav repuestos-->
                    <div class="col-xs-12 col-md-3">
                        <div class="nav-spare-parts wow slideInLeft">
                            <div class="container-nav">
                                <div class="hidden-xs hidden-sm title-nav-s-parts font-bold text-uppercase">repuestos y otros</div>
                                <ul class="hidde-xs hidden-sm list-nav-sparts">
                                    <li class="item-sparts">
                                        <a href="#" class="link-sparts font-regular"><i class="icon-circle"></i> Válvula (pitón)</a>
                                    </li>
                                    <li class="item-sparts">
                                        <a href="#" class="link-sparts font-regular"><i class="icon-circle"></i> Tuercas</a>
                                    </li>
                                    <li class="item-sparts">
                                        <a href="#" class="link-sparts font-regular"><i class="icon-circle"></i> Seguros</a>
                                    </li>
                                    <li class="item-sparts">
                                        <a href="#" class="link-sparts font-regular"><i class="icon-circle"></i> Medidor de cocada</a>
                                    </li>
                                    <li class="item-sparts">
                                        <a href="#" class="link-sparts font-regular"><i class="icon-circle"></i> Medidor de presión de aire</a>
                                    </li>
                                </ul>
                                <div class="select-int col-xs-12 col-sm-6 visible-xs visible-sm px-0 ">
                                    <div class="wrapper-select-cart">
                                        <div class="button dropdown">
                                            <select id="select-stores" class="font-bold">
                                                <option class="">REPUESTOS Y OTROS</option>
                                                <option value="valvula" class="stores">Válvula (pitón)</option>
                                                <option value="mtuercas" class="stores">Tuercas</option>
                                                <option value="seguros" class="stores">Seguros</option>
                                                <option value="medidor-cocada" class="stores">Medidor de cocada</option>
                                                <option value="medidor-presion" class="stores">Medidor de presión de aire</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--PRODUCTOS REPUESTOS / OTROS-->
                    <div class="col-xs-12 col-md-9">
                        <div class="row">
                            <div class="col-xs-12 padd-tselect">
                                <div class="">
                                    <div class="select-int col-xs-12 col-sm-6">
                                        <h3 class="title-left-select font-regular d-in-block">ORDENAR POR: </h3>
                                        <div class="wrapper-select-cart">
                                            <div class="button dropdown">
                                                <select id="select-stores" class="font-regular">
                                                    <option value="mayor-menor" class="stores">Precio de mayor a menor</option>
                                                    <option value="menor-mayor" class="stores">Precio de menor a mayor</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6 col-lg-4 px-0">
                                <div class="xhov wow zoomIn">
                                    <div class="wrapper-card-product">
                                        <a href="detalle-producto.php" class="link-wrapper-prod h100 w100 d-block">
                                            <div class="wrapper-img-prod h-auto">
                                                <div class="content-img-prod">
                                                    <img class="w-100" src="assets/images/productos/aro.jpg" alt="">
                                                </div>
                                            </div>
                                            <div class="info-product text-uppercase">
                                                <h2 class="font-bold name-cod-prod">235/45R18</h2>
                                                <h3 class="font-regular name-brand">DARWIN</h3>
                                                <div class="wrapper-price flex-al-cent sp-arrow">
                                                    <h2 class="font-bold price-prod"><span class="font-regular price-dsct-prod">S/720.00</span> S/ 720.00</h2>
                                                </div>
                                            </div>
                                        </a>
                                        <div class="dif">
                                            <div class="add-purchase ad2b d-flex">
                                                <div class="qty mt-5">
                                                    <span class="plus bg-dark">+</span>
                                                    <input type="number" class="count font-regular" name="qty" value="1">
                                                    <span class="minus bg-dark">-</span>
                                                </div>
                                                <div class="btn-purchase">
                                                    <a href="carrito-de-compras.php" class="link-purchase font-bold text-uppercase text-center">comprar <i class="icon-carrito"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="circle-dscto d-flex">
                                            <h4 class="font-regular">-20%</h4>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6 col-lg-4 px-0">
                                <div class="xhov wow zoomIn">
                                    <div class="wrapper-card-product">
                                        <a href="detalle-producto.php" class="link-wrapper-prod h100 w100 d-block">
                                            <div class="wrapper-img-prod h-auto">
                                                <div class="content-img-prod">
                                                    <img class="w-100" src="assets/images/productos/aro.jpg" alt="">
                                                </div>
                                            </div>
                                            <div class="info-product text-uppercase">
                                                <h2 class="font-bold name-cod-prod">235/45R18</h2>
                                                <h3 class="font-regular name-brand">DARWIN</h3>
                                                <div class="wrapper-price flex-al-cent sp-arrow">
                                                    <h2 class="font-bold price-prod"><span class="font-regular price-dsct-prod">S/720.00</span> S/ 720.00</h2>
                                                </div>
                                            </div>
                                        </a>
                                        <div class="dif">
                                            <div class="add-purchase ad2b d-flex">
                                                <div class="qty mt-5">
                                                    <span class="plus bg-dark">+</span>
                                                    <input type="number" class="count font-regular" name="qty" value="1">
                                                    <span class="minus bg-dark">-</span>
                                                </div>
                                                <div class="btn-purchase">
                                                    <a href="carrito-de-compras.php" class="link-purchase font-bold text-uppercase text-center">comprar <i class="icon-carrito"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="circle-dscto d-flex">
                                            <h4 class="font-regular">-20%</h4>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6 col-lg-4 px-0">
                                <div class="xhov wow zoomIn">
                                    <div class="wrapper-card-product">
                                        <a href="detalle-producto.php" class="link-wrapper-prod h100 w100 d-block">
                                            <div class="wrapper-img-prod h-auto">
                                                <div class="content-img-prod">
                                                    <img class="w-100" src="assets/images/productos/aro.jpg" alt="">
                                                </div>
                                            </div>
                                            <div class="info-product text-uppercase">
                                                <h2 class="font-bold name-cod-prod">235/45R18</h2>
                                                <h3 class="font-regular name-brand">DARWIN</h3>
                                                <div class="wrapper-price flex-al-cent sp-arrow">
                                                    <h2 class="font-bold price-prod"><span class="font-regular price-dsct-prod">S/720.00</span> S/ 720.00</h2>
                                                </div>
                                            </div>
                                        </a>
                                        <div class="dif">
                                            <div class="add-purchase ad2b d-flex">
                                                <div class="qty mt-5">
                                                    <span class="plus bg-dark">+</span>
                                                    <input type="number" class="count font-regular" name="qty" value="1">
                                                    <span class="minus bg-dark">-</span>
                                                </div>
                                                <div class="btn-purchase">
                                                    <a href="carrito-de-compras.php" class="link-purchase font-bold text-uppercase text-center">comprar <i class="icon-carrito"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="circle-dscto d-flex">
                                            <h4 class="font-regular">-20%</h4>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
    <?php
        include 'src/includes/footer.php'
    ?>
</body>

</html>