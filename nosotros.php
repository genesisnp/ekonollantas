<?php
    include 'src/includes/header.php'
?>
    <main class="main-us">
        <section class="sct-banner">
            <div class="degrade-int"></div>
            <img class="img-banner" src="assets/images/banner/nuestros-servicios.jpg" alt="">
            <div class="content-text-banner">
                <i class="icon-banner icon-nosotros"></i>
                <h2 class="title-banner text-uppercase font-bold">nosotros</h2>
            </div>
            <a href="#info-company" class="icon-arrow" data-ancla="info-company"></a>
        </section>
        <section class="info-company" id="info-company">
            <div class="container-fluid">
                <div class="row">
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="description-page wow zoomIn">
                                    <p class="text-center text-page font-regular">Somos una empresa con más de 33
                                        años de experiencia en asesoramiento y venta de neumáticos, aros, baterias, así
                                        como servicios relacionados (Balanceos, parches, alineamiento).</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 content-vm">
                        <div class="container">
                            <div class="row">
                                <div class="col-xs-12 col-sm-4 col-lg-3 px-0 wow slideInLeft">
                                    <h2 class="font-bold title-mv color-primary text-uppercase">Misión</h2>
                                    <p class="p-internas text-mv">Ofrecemos productos y servicios con la mejor relación precio,
                                        calidad y seguridad dirigidos al sector automotriz, siendo los neumáticos el
                                        negocio principal. En ese sentido, utilizamos los más altos estándares de
                                        servicio al cliente e infraestructura para satisfacer las necesidades de
                                        nuestros clientes.</p>
                                </div>
                                <div class="col-xs-12 col-sm-4 col-lg-6 wow zoomIn">
                                    <div class="content-img-mv">
                                        <img class="img100-cover" src="assets/images/internas/nosotros.jpg" alt="">
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-4 col-lg-3 px-0 wow slideInRight">
                                    <h2 class="font-bold title-mv color-primary text-uppercase">visión</h2>
                                    <p class="p-internas text-mv">Afianzar nuestra posición de liderazgo en el rubro automotriz
                                        diferenciándonos como una empresa moderna y eficiente. Queremos ser reconocidos
                                        por prestar la mejor calidad de atención con personal capacitado y utilizando el
                                        más alto nivel de infraestructura y equipamiento, para una mayor satisfacción de
                                        nuestros clientes.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12">
                        <div class="container">
                            <div class="row">
                                <h2 class="col-xs-12 font-bold title-mv color-primary text-uppercase text-center">
                                    nuestros valores</h2>
                                <div class="col-xs-12 col-sm-6 col-lg-3 mb-mob wow zoomIn">
                                    <div class="img-values">
                                        <img src="assets/images/icons/positivo.svg" alt="">
                                    </div>
                                    <div class="info-values text-center">
                                        <h3 class="title-value font-bold text-uppercase">ser positivo</h3>
                                        <p class="p-internas">Desde nuestra forma de comunicarnos.</p>
                                        <p class="p-internas">Tratar con entusiasmo.</p>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-lg-3 mb-mob wow zoomIn" data-wow-delay="1s">
                                    <div class="img-values">
                                        <img src="assets/images/icons/profesionalismo.svg" alt="">
                                    </div>
                                    <div class="info-values text-center">
                                        <h3 class="title-value font-bold text-uppercase">profesionalismo</h3>
                                        <p class="p-internas">Saber hacer tu trabajo.</p>
                                        <p class="p-internas">Toma de decisiones en base a información.</p>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-lg-3 mb-mob wow zoomIn" data-wow-delay="2s">
                                    <div class="img-values">
                                        <img src="assets/images/icons/honestidad.svg" alt="">
                                    </div>
                                    <div class="info-values text-center">
                                        <h3 class="title-value font-bold text-uppercase">Honestidad</h3>
                                        <p class="p-internas">Decir siempre la verdad.</p>
                                        <p class="p-internas">No robar.</p>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-lg-3 mb-mob wow zoomIn" data-wow-delay="3s">
                                    <div class="img-values">
                                        <img src="assets/images/icons/respeto.svg" alt="">
                                    </div>
                                    <div class="info-values text-center">
                                        <h3 class="title-value font-bold text-uppercase">respeto</h3>
                                        <p class="p-internas">Pensar en el beneficio mutuo.</p>
                                        <p class="p-internas">Tratar a los demás como quieres que te traten.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
    <?php
        include 'src/includes/footer.php'
    ?>

</body>

</html>