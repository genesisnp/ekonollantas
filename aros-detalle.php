<?php
    include 'src/includes/header.php'
?>
    <main class="details-products container-fluid px-0">
        <!-- <div class="sct-select-detail col-xs-12">
            <div class="wrapper-select-cart">
                <h2 class="font-bold title-slct-descr">ENCUENTRA TU ARO:</h2>

                <div class="output col-xs-12 col-sm-4 px-0 flex-al-cent">
                    <div class="select-banner-one d-in-block">
                        <div class="select-filter">
                            <div class="wrapper-filter d-in-block">
                                <div class="dropdown dropdown-selects">
                                    <div class="select">
                                        <span class="title-select font-regular"></span>
                                        <i class="icon-select icon-triangle-down"></i>
                                        <label class="form__label font-regular">
                                            ANCHO :
                                        </label>
                                    </div>
                                    <input type="hidden" name="gender" value="">
                                    <ul class="dropdown-menu font-regular">
                                        <li id="menor-mayor">125cms</li>
                                        <li id="mayor-menor">200cms</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="select-banner-medio d-in-block">
                        <div class="select-filter">
                            <div class="wrapper-filter d-in-block">
                                <div class="dropdown dropdown-selects">
                                    <div class="select">
                                        <span class="title-select font-regular"></span>
                                        <i class="icon-select icon-triangle-down"></i>
                                        <label class="form__label font-regular">
                                            PERFIL :
                                        </label>
                                    </div>
                                    <input type="hidden" name="gender" value="Precio de menor a mayor">
                                    <ul class="dropdown-menu font-regular">
                                        <li id="menor-mayor">123</li>
                                        <li id="mayor-menor">3456</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="select-banner-medio d-in-block">
                        <div class="select-filter">
                            <div class="wrapper-filter d-in-block">
                                <div class="dropdown dropdown-selects">
                                    <div class="select">
                                        <span class="title-select font-regular"></span>
                                        <i class="icon-select icon-triangle-down"></i>
                                        <label class="form__label font-regular">
                                            ARO :
                                        </label>
                                    </div>
                                    <input type="hidden" name="gender" value="Precio de menor a mayor">
                                    <ul class="dropdown-menu font-regular">
                                        <li id="menor-mayor">789</li>
                                        <li id="mayor-menor">9115</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <button class="btn-bsc-select"><i class="icon-Lupa"></i></button>
                </div>
            </div>
        </div> -->
        <section class="sct-detail-product">
            <div class="container">
                <div class="row">
                    <!-- img-detalle-producto -->
                    <div class="col-xs-12 col-md-6">
                        <div class="img-big-detail-product">
                            <img src="assets/images/productos/llantas.png" alt="" class="img-dproduct">
                            <div class="circle-dsct">
                                <span class="font-regular">-20%</span>
                            </div>
                        </div>
                    </div>
                    <!-- detalle producto -->
                    <div class="col-xs-12 col-md-6">
                        <div class="row">
                            <div class="col-xs-10">
                                <img src="assets/images/logos/bridgestone.jpg" alt="" class="img-detail-product-brand">
                                <h2 class="font-bold text-uppercase my-0">195/65r15 91h</h2>
                                <h3 class="marca-descrp font-bold text-uppercase my-0">dueler g/t 684</h3>
                                <span class="p-internas">Código: 0001666</span>
                                <p class="p-internas text-dscr-cond">Diseñado para conductores entusiastas que desean una combinación
                                    look y rendimiento, destacando la maniobrabilidad y tracción en suelos secos y
                                    mojados.</p>
                            </div>
                            <div class="col-xs-12">
                                <div class="wrapper-select-cart">
                                    <div class="button dropdown col-xs-12 col-sm-6 col-md-8 col-lg-6">
                                        <select id="select-stores" class="font-regular">
                                            <option>SELECCIONE UNA TIENDA</option>
                                            <option value="benavides" class="stores">benavides</option>
                                            <option value="magdalena" class="stores">magdalena</option>
                                        </select>
                                    </div>

                                    <div class="output col-xs-12 px-0">
                                        <div id="benavides" class="stores-info">
                                            <div class="ubication-stores">
                                                <h2 class="name-stores font-bold color-primary">EKONO BENAVIDES</h2>
                                                <span class="distrito-stores font-bold">SURCO</span>
                                                <p class="p-internas mb-0">Av. Prolongación Benavides N° 3999(A media
                                                    cdra
                                                    cruce con Av. Ayacucho).</p>
                                                <p class="p-internas">Horario de atención de 8:00am a 6:00pm de lunes a
                                                    sábado.</p>
                                            </div>
                                            <div class="modal-map">
                                                <a href="#" class="font-bold ver-map"><i
                                                        class="icon-ubc color-primary"></i> VER MAPA</a>
                                            </div>
                                            <div class="div-alert">
                                                <p class="p-internas">Stock no disponible para este local, le
                                                    agradeceremos
                                                    seleccione otra tienda, de lo contrario se le recargará el monto de
                                                    traslado y los productos serán llevadas al local en un máximo de 48
                                                    horas.</p>
                                            </div>
                                        </div>
                                        <div id="magdalena" class="stores-info">
                                            <div class="ubication-stores">
                                                <h2 class="name-stores font-bold color-primary">EKONO primavera</h2>
                                                <span class="distrito-stores font-bold">surco</span>
                                                <p class="p-internas mb-0">Av. Primavera Nº 1195 (Alt. Pte. Primavera)
                                                </p>
                                                <p class="p-internas">Horario de atención de 8:00am a 6:00pm de lunes a
                                                    sábado.</p>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <div class="btn-comprar-detail col-xs-12">
                                    <div class="wrapper-price flex-al-cent sp-arrow float-left">
                                        <span class="font-regular price-dsct-prod">S/720.00</span>
                                        <h2 class="font-bold price-prod">S/ 720.00</h2>
                                    </div>
                                    <div class="add-purchase d-flex float-right">
                                        <div class="qty mt-5">
                                            <span class="minus bg-dark">-</span>
                                            <input type="number" class="count font-regular" name="qty" value="1">
                                            <span class="plus bg-dark">+</span>
                                        </div>
                                        <div class="btn-purchase">
                                            <a href="#"
                                                class="link-purchase font-bold text-uppercase text-center">comprar</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="tabs-info-product">
                                    <ul class="nav nav-tabs nav-pills plantac col-xs-12" id="myTabControls">
                                        <li class="nav-link active">
                                            <a data-toggle="tab" href="#description" class="nav-link-a font-regular">DESCRIPCIÓN</a>
                                        </li>
                                        <li class="nav-link">
                                            <a data-toggle="tab" href="#accesorios" class="nav-link-a font-regular">ACCESORIOS</a>
                                        </li>
                                        <li class="nav-link">
                                            <a data-toggle="tab" href="#videos" class="nav-link-a font-regular">
                                                <div class="icons-lista"></div>VIDEOS
                                            </a>
                                        </li>
                                    </ul>

                                    <div class="tab-content col-xs-12" id="content-solutions">
                                        <div id="description" class="tab-pane fade in active row">
                                            <p class="p-internas">1- Lorem, ipsum dolor sit amet consectetur adipisicing
                                                elit. Omnis, ratione
                                                nulla ipsa obcaecati earum officia optio! Illo, blanditiis facere.
                                                Corrupti beatae voluptatum commodi ex odio quisquam sint dolorem unde
                                                cupiditate.</p>
                                        </div>
                                        <div id="accesorios" class="tab-pane fade in row">
                                            <p class="p-internas">2 - Lorem ipsum dolor sit amet consectetur adipisicing
                                                elit. Aliquam libero, vero nulla accusamus dolorem repudiandae rerum
                                                necessitatibus fuga consequuntur repellendus vitae porro aspernatur
                                                alias provident incidunt, distinctio, temporibus quia in?</p>
                                        </div>
                                        <div id="videos" class="tab-pane fade in row">
                                            <p class="p-internas">3 - Lorem ipsum dolor sit amet consectetur adipisicing
                                                elit. Aliquam libero, vero nulla accusamus dolorem repudiandae rerum
                                                necessitatibus fuga consequuntur repellendus vitae porro aspernatur
                                                alias provident incidunt, distinctio, temporibus quia in?</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="product-related">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <h2 class="title-product-related font-bold text-center">PRODUCTOS RELACIONADOS</h2>
                    </div>
                    <div class="col-xs-12">
                        <div class="row">
                            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                                <div class="xhov">
                                    <div class="wrapper-card-product">
                                        <a href="detalle-de-producto.php" class="link-wrapper-prod h100 w100 d-block">
                                            <div class="wrapper-img-prod h-auto">
                                                <div class="content-img-prod">
                                                    <img class="w-100" src="assets/images/productos/llantas2.jpg" alt="">
                                                </div>
                                            </div>
                                            <div class="info-product text-uppercase">
                                                <h2 class="font-bold name-cod-prod">235/45R18</h2>
                                                <h3 class="font-regular name-brand">DARWIN</h3>
                                                <div class="wrapper-price flex-al-cent sp-arrow">
                                                    <h2 class="font-bold price-prod"><span class="font-regular price-dsct-prod">S/720.00</span> S/ 720.00</h2>
                                                </div>
                                            </div>
                                        </a>
                                        <div class="dif">
                                            <div class="add-purchase ad2b d-flex">
                                                <div class="qty mt-5">
                                                    <span class="plus bg-dark">+</span>
                                                    <input type="number" class="count font-regular" name="qty" value="1">
                                                    <span class="minus bg-dark">-</span>
                                                </div>
                                                <div class="btn-purchase">
                                                    <a href="#" class="link-purchase font-bold text-uppercase text-center">comprar <i class="icon-carrito"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="circle-dscto d-flex">
                                            <h4 class="font-regular">-20%</h4>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                                <div class="xhov">
                                    <div class="wrapper-card-product">
                                        <a href="detalle-de-producto.php" class="link-wrapper-prod h100 w100 d-block">
                                            <div class="wrapper-img-prod h-auto">
                                                <div class="content-img-prod">
                                                    <img class="w-100" src="assets/images/productos/llantas2.jpg" alt="">
                                                </div>
                                            </div>
                                            <div class="info-product text-uppercase">
                                                <h2 class="font-bold name-cod-prod">235/45R18</h2>
                                                <h3 class="font-regular name-brand">DARWIN</h3>
                                                <div class="wrapper-price flex-al-cent sp-arrow">
                                                    <h2 class="font-bold price-prod"><span class="font-regular price-dsct-prod">S/720.00</span> S/ 720.00</h2>
                                                </div>
                                            </div>
                                        </a>
                                        <div class="dif">
                                            <div class="add-purchase ad2b d-flex">
                                                <div class="qty mt-5">
                                                    <span class="plus bg-dark">+</span>
                                                    <input type="number" class="count font-regular" name="qty" value="1">
                                                    <span class="minus bg-dark">-</span>
                                                </div>
                                                <div class="btn-purchase">
                                                    <a href="#" class="link-purchase font-bold text-uppercase text-center">comprar <i class="icon-carrito"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="circle-dscto d-flex">
                                            <h4 class="font-regular">-20%</h4>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                                <div class="xhov">
                                    <div class="wrapper-card-product">
                                        <a href="detalle-de-producto.php" class="link-wrapper-prod h100 w100 d-block">
                                            <div class="wrapper-img-prod h-auto">
                                                <div class="content-img-prod">
                                                    <img class="w-100" src="assets/images/productos/llantas2.jpg" alt="">
                                                </div>
                                            </div>
                                            <div class="info-product text-uppercase">
                                                <h2 class="font-bold name-cod-prod">235/45R18</h2>
                                                <h3 class="font-regular name-brand">DARWIN</h3>
                                                <div class="wrapper-price flex-al-cent sp-arrow">
                                                    <h2 class="font-bold price-prod"><span class="font-regular price-dsct-prod">S/720.00</span> S/ 720.00</h2>
                                                </div>
                                            </div>
                                        </a>
                                        <div class="dif">
                                            <div class="add-purchase ad2b d-flex">
                                                <div class="qty mt-5">
                                                    <span class="plus bg-dark">+</span>
                                                    <input type="number" class="count font-regular" name="qty" value="1">
                                                    <span class="minus bg-dark">-</span>
                                                </div>
                                                <div class="btn-purchase">
                                                    <a href="#" class="link-purchase font-bold text-uppercase text-center">comprar <i class="icon-carrito"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="circle-dscto d-flex">
                                            <h4 class="font-regular">-20%</h4>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                                <div class="xhov">
                                    <div class="wrapper-card-product">
                                        <a href="detalle-de-producto.php" class="link-wrapper-prod h100 w100 d-block">
                                            <div class="wrapper-img-prod h-auto">
                                                <div class="content-img-prod">
                                                    <img class="w-100" src="assets/images/productos/llantas2.jpg" alt="">
                                                </div>
                                            </div>
                                            <div class="info-product text-uppercase">
                                                <h2 class="font-bold name-cod-prod">235/45R18</h2>
                                                <h3 class="font-regular name-brand">DARWIN</h3>
                                                <div class="wrapper-price flex-al-cent sp-arrow">
                                                    <h2 class="font-bold price-prod"><span class="font-regular price-dsct-prod">S/720.00</span> S/ 720.00</h2>
                                                </div>
                                            </div>
                                        </a>
                                        <div class="dif">
                                            <div class="add-purchase ad2b d-flex">
                                                <div class="qty mt-5">
                                                    <span class="plus bg-dark">+</span>
                                                    <input type="number" class="count font-regular" name="qty" value="1">
                                                    <span class="minus bg-dark">-</span>
                                                </div>
                                                <div class="btn-purchase">
                                                    <a href="#" class="link-purchase font-bold text-uppercase text-center">comprar <i class="icon-carrito"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="circle-dscto d-flex">
                                            <h4 class="font-regular">-20%</h4>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
    <?php
        include 'src/includes/footer.php'
    ?>
    <script src="assets/js/counting.js"></script>
    <script>
        $('.plantac li a').click(function (e) {
            var href = $(this).attr('href');

            $('.nav-tabs li').removeClass('active');
            $('.nav-tabs li a[href="' + href + '"]').closest('li').addClass('active');
            $('.tab-pane').removeClass('active');
            $('.tab-pane' + href).addClass('active');
        })
    </script>
</body>

</html>