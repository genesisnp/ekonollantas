<?php
    include 'src/includes/header.php'
?>
    <main class="main-work-whith-us">
        <section class="sct-banner">
            <div class="degrade-int"></div>
            <img class="img-banner" src="assets/images/banner/nuestros-servicios.jpg" alt="">
            <div class="content-text-banner">
                <i class="icon-banner icon-trab-con-nos"></i>
                <h2 class="title-banner text-uppercase font-bold">trabaja con nosotros</h2>
                <div class="p-internas">Porque estamos en constante crecimiento, necesitamos gente como tú,
                    comprometidos y dispuestos para crecer profesionalmente junto con nosotros</div>
            </div>
            <a href="#form-work-whith-us" class="icon-arrow" data-ancla="form-work-white-us"></a>
        </section>
        <section class="form-work-whith-us" id="form-work-whith-us">
            <div class="container">
                <form action="#" class="form row" method="post" id="form-work-whith-us">
                    <div class="col-xs-12 d-flex">
                        <div class="row">
                            <div class="col-xs-12 col-sm-8">
                                <div class="row">
                                    <div class="form__wrapper col-xs-12 col-sm-6">
                                        <input type="text" class="form__input bg-input" id="name-work" name="name-work">
                                        <label class="form__label">
                                            <span class="form__label-content">Nombres:</span>
                                        </label>
                                    </div>
                                    <div class="form__wrapper col-xs-12 col-sm-6">
                                        <input type="text" class="form__input bg-input" id="last-name-work"
                                            name="last-name-work">
                                        <label class="form__label">
                                            <span class="form__label-content">Apellidos:</span>
                                        </label>
                                    </div>
                                    <div class="form__wrapper col-xs-12 col-sm-6">
                                        <input type="text" class="form__input bg-input" id="phone-work"
                                            name="phone-work">
                                        <label class="form__label">
                                            <span class="form__label-content">Teléfono:</span>
                                        </label>
                                    </div>
                                    <div class="form__wrapper col-xs-12 col-sm-6">
                                        <input type="email" class="form__input bg-input" id="email-work"
                                            name="email-work">
                                        <label class="form__label">
                                            <span class="form__label-content">E-mail</span>
                                        </label>
                                    </div>
                                    <div class="form__wrapper col-xs-12 col-sm-6">
                                        <input type="text" class="form__input bg-input" id="area-work"
                                            name="area-work">
                                        <label class="form__label">
                                            <span class="form__label-content">Área a postular</span>
                                        </label>
                                    </div>
                                    <div class="form__wrapper col-xs-12 col-sm-6 col-lg-6 d-flex align-items-center">
                                        <div class="input_file d-flex">
                                            <label class="file_label d-flex font-regular">
                                                <span class="font-regular">Adjuntar archivo</span>
                                            </label>
                                            <input id="file" type="file" name="file" multiple />
                                            <div class="img-file d-flex justify-content-center align-items-center"><img
                                                    src="assets/images/icons/archivo.svg" alt=""></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-4 h100">
                                <div class="row h100 txt-ar">
                                    <div class="form__wrapper col-xs-12 h100">
                                        <textarea class="form__input form_textarea bg-input" id="textarea"
                                            name="textarea"></textarea>
                                        <label class="form__label">
                                            <span class="form__label-content">Mensaje</span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12">
                        <div class="checkbox">
                            <label class="font-regular label-pol">
                                <input type="checkbox" /><i class="helper"></i><span>He leído y acepto la <span
                                        class="span-pol color-primary btn-modals">Política
                                        de Privacidad</span></span>
                            </label>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-4">
                        <div class="btn-container">
                            <button type="submit" class="btn-send font-bold" id="btn-send-form">ENVIAR</button>
                        </div>
                    </div>
                </form>
            </div>
        </section>
    </main>
    <?php
        include 'src/includes/footer.php'
    ?>
    <script src="assets/js/form-work.js"></script>
</body>

</html>