<?php
    include 'src/includes/header.php'
?>
    <main class="main-">
        <section class="sct-banner">
            <div class="degrade-int"></div>
            <img class="img-banner" src="assets/images/banner/nuestros-servicios.jpg" alt="">
            <div class="content-text-banner">
                <i class="icon-banner icon-tiendas"></i>
                <h2 class="title-banner text-uppercase font-bold">Nuestras tiendas</h2>
                <p class="p-internas">Lorem ipsum dolor sit, amet consectetur adipisicing elit. Nihil quos laudantium,
                    vero incidunt optio deserunt nulla, odit nesciunt maxime veniam quibusdam magnam quo iure corrupti
                    consectetur obcaecati omnis distinctio numquam?</p>
            </div>
            <a href="#our-shops" class="icon-arrow" data-ancla="our-shops"></a>
        </section>
        <section class="our-shops" id="our-shops">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 d-flex">
                        <ul class="accordion wow slideInLeft">
                            <li><a href="#la-marina" class="accordion-heading font-regular active">EKONO LA
                                    MARINA - <span>Pueblo
                                        Libre</span><i class="icon-flecha-accord"></i></a>
                                <div class="accordion-content">
                                    <div class="info-shops">
                                        <div class="content-icon float-left">
                                            <i class="icon-acc-int icon-ubc"></i>
                                        </div>
                                        <div class="content-text-shops">
                                            <p class="p-internas">Av. La Marina Nº 1200 (esquina con Cueva)</p>
                                        </div>
                                    </div>
                                    <div class="info-shops">
                                        <div class="content-icon float-left">
                                            <i class="icon-acc-int icon-calendar"></i>
                                        </div>
                                        <div class="content-text-shops">
                                            <p class="p-internas">Horario de atención de lunes a sábado de 8:00am a
                                                6:00pm</p>
                                        </div>
                                    </div>
                                    <div class="info-shops">
                                        <div class="content-icon float-left">
                                            <i class="icon-acc-int icon-phone2"></i>
                                        </div>
                                        <div class="content-text-shops">
                                            <p href="#" class="p-internas"><strong><a href="#" class="color-black font-bold">01 219-4650</a> / <a href="#" class="color-black font-bold">01 219 4607</a><a href="#" class="font-bold color-black a-wts"><i class="icon-whatsapp"></i> +51 993 541 157</a></strong></p>
                                        </div>
                                    </div>
                                    <div class="content-map visible-xs visible-sm">
                                        <iframe
                                            src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1950.7345570045607!2d-77.07625060039213!3d-12.080003937576262!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x9105c90c4f889a53%3A0x69f19d1ac89e8131!2sAv.%20la%20Marina%201200%2C%20Cercado%20de%20Lima%2015084!5e0!3m2!1ses-419!2spe!4v1574878837995!5m2!1ses-419!2spe"
                                            frameborder="0" style="border:0;" allowfullscreen=""></iframe>
                                    </div>
                                </div>
                            </li>
                            <li><a href="#primavera" class="accordion-heading font-regular">EKONO PRIMAVERA -
                                    <span>Surco</span><i class="icon-flecha-accord"></i></a>
                                <div class="accordion-content">
                                    <div class="info-shops">
                                        <div class="content-icon float-left">
                                            <i class="icon-acc-int icon-ubc"></i>
                                        </div>
                                        <div class="content-text-shops">
                                            <p class="p-internas">Av. Primavera Nº 1195 (Alt. Pte. Primavera)</p>
                                        </div>
                                    </div>
                                    <div class="info-shops">
                                        <div class="content-icon float-left">
                                            <i class="icon-acc-int icon-calendar"></i>
                                        </div>
                                        <div class="content-text-shops">
                                            <p class="p-internas">Horario de atención de lunes a sábado de 8:00am a
                                                6:00pm</p>
                                        </div>
                                    </div>
                                    <div class="info-shops">
                                        <div class="content-icon float-left">
                                            <i class="icon-acc-int icon-phone2"></i>
                                        </div>
                                        <div class="content-text-shops">
                                            
                                        <div class="content-text-shops">
                                            <p href="#" class="p-internas"><strong><a href="#" class="color-black font-bold">01 219-4650</a><a href="#" class="font-bold color-black a-wts"><i class="icon-whatsapp"></i> +51 993 541 157</a></strong></p>
                                        </div>
                                    </div>
                                    <div class="content-map visible-xs visible-sm">
                                        <iframe
                                            src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3901.0315943163155!2d-76.97918608464833!3d-12.109989646378281!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x9105c7974694d52b%3A0x273c3d480da3866c!2sAv.%20Primavera%201195%2C%20Santiago%20de%20Surco%2015023!5e0!3m2!1ses-419!2spe!4v1574886766391!5m2!1ses-419!2spe"
                                            frameborder="0" style="border:0;" allowfullscreen=""></iframe>
                                    </div>
                                </div>
                            </li>
                            <li><a href="#benavides" class="accordion-heading font-regular">EKONO BENAVIDES -
                                    <span>Surco</span><i class="icon-flecha-accord"></i></a>
                                <div class="accordion-content">
                                    <div class="info-shops">
                                        <div class="content-icon float-left">
                                            <i class="icon-acc-int icon-ubc"></i>
                                        </div>
                                        <div class="content-text-shops">
                                            <p class="p-internas">Av. Prolongación Benavides Nº 3999 (a media cdra cruce
                                                con Av Ayacucho)</p>
                                        </div>
                                    </div>
                                    <div class="info-shops">
                                        <div class="content-icon float-left">
                                            <i class="icon-acc-int icon-calendar"></i>
                                        </div>
                                        <div class="content-text-shops">
                                            <p class="p-internas">Horario de atención de lunes a sábado de 9:00am a
                                                6:00pm</p>
                                        </div>
                                    </div>
                                    <div class="info-shops">
                                        <div class="content-icon float-left">
                                            <i class="icon-acc-int icon-phone2"></i>
                                        </div>
                                        <div class="content-text-shops">
                                            <p href="#" class="p-internas"><strong><a href="#" class="color-black font-bold">01 219-4650</a><a href="#" class="font-bold color-black a-wts"><i class="icon-whatsapp"></i> +51 993 541 157</a></strong></p>
                                        </div>
                                    </div>
                                    <div class="content-map visible-xs visible-sm">
                                        <iframe
                                            src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3900.7659263065407!2d-76.99596388464805!3d-12.128161746723425!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x9105c7f604a64219%3A0x195b83fd19af1c28!2sAv.%20Alfredo%20Benavides%203999%2C%20Cercado%20de%20Lima%2015038!5e0!3m2!1ses-419!2spe!4v1574886848558!5m2!1ses-419!2spe"
                                            frameborder="0" style="border:0;" allowfullscreen=""></iframe>
                                    </div>
                                </div>
                            </li>
                            <!-- <li><a href="#rambla" class="accordion-heading font-regular">EKONO RAMBLA - <span>San
                                        Borja</span><i class="icon-flecha-accord"></i></a>
                                <div class="accordion-content">
                                    <div class="info-shops">
                                        <div class="content-icon float-left">
                                            <i class="icon-acc-int icon-ubc"></i>
                                        </div>
                                        <div class="content-text-shops">
                                            <p class="p-internas">CC La Rambla de San Borja Sotano Nro 02 (Alt Ava
                                                Aviacion con Av Javier Prado Este)</p>
                                        </div>
                                    </div>
                                    <div class="info-shops">
                                        <div class="content-icon float-left">
                                            <i class="icon-acc-int icon-calendar"></i>
                                        </div>
                                        <div class="content-text-shops">
                                            <p class="p-internas">Horario de atención de lunes a sábado de 9:00am a
                                                6:00pm</p>
                                        </div>
                                    </div>
                                    <div class="info-shops">
                                        <div class="content-icon float-left">
                                            <i class="icon-acc-int icon-phone2"></i>
                                        </div>
                                        <div class="content-text-shops">
                                            <p href="#" class="p-internas"><strong>
                                                <a href="#" class="color-black font-bold">01 219-4650</a>
                                                <a href="#" class="font-bold color-black a-wts"><i class="icon-whatsapp"></i> +51 993 541 157</a>
                                            </strong></p>
                                        </div>
                                    </div>
                                    <div class="content-map visible-xs visible-sm">
                                        <iframe
                                            src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3280.615167665316!2d-77.00767658994178!3d-12.089484515488243!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x9105c7d40410676d%3A0x997e37c0e0fbbd48!2sLa%20Rambla%20San%20Borja!5e0!3m2!1ses-419!2spe!4v1574886966390!5m2!1ses-419!2spe"
                                            frameborder="0" style="border:0;" allowfullscreen=""></iframe>
                                    </div>
                                </div>
                            </li> -->
                            <li><a href="#miraflores" class="accordion-heading font-regular">EKONO MIRAFLORES -
                                    <span>Miraflores</span><i class="icon-flecha-accord"></i></a>
                                <div class="accordion-content">
                                    <div class="info-shops">
                                        <div class="content-icon float-left">
                                            <i class="icon-acc-int icon-ubc"></i>
                                        </div>
                                        <div class="content-text-shops">
                                            <p class="p-internas">Av. José Pardo 748 - Miraflores (frente a Vivanda)</p>
                                        </div>
                                    </div>
                                    <div class="info-shops">
                                        <div class="content-icon float-left">
                                            <i class="icon-acc-int icon-calendar"></i>
                                        </div>
                                        <div class="content-text-shops">
                                            <p class="p-internas">Horario de atención de lunes a sábado de 9:00am a
                                                6:00pm</p>
                                        </div>
                                    </div>
                                    <div class="info-shops">
                                        <div class="content-icon float-left">
                                            <i class="icon-acc-int icon-phone2"></i>
                                        </div>
                                        <div class="content-text-shops">
                                            <p href="#" class="p-internas"><strong><a href="#" class="color-black font-bold">01 219-4650</a><a href="#" class="font-bold color-black a-wts"><i class="icon-whatsapp"></i> +51 993 541 157</a></strong></p>
                                        </div>
                                    </div>
                                    <div class="content-map visible-xs visible-sm">
                                        <iframe
                                            src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2319.489654805363!2d-77.03805598637105!3d-12.118920935299927!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x9105c83d29fd3675%3A0x72c1da4e2199ff7c!2sAv.%20Jos%C3%A9%20Pardo%20748%2C%20Lima%2015074!5e0!3m2!1ses-419!2spe!4v1574887107819!5m2!1ses-419!2spe"
                                            frameborder="0" style="border:0;" allowfullscreen=""></iframe>
                                    </div>
                                </div>
                            </li>
                            <li><a href="#tomas-marsano" class="accordion-heading font-regular">EKONO TOMAS MARSANO -
                                    <span>Surquillo</span><i class="icon-flecha-accord"></i></a>
                                <div class="accordion-content">
                                    <div class="info-shops">
                                        <div class="content-icon float-left">
                                            <i class="icon-acc-int icon-ubc"></i>
                                        </div>
                                        <div class="content-text-shops">
                                            <p class="p-internas">Av.Tomás Marsano Nº300 (frente Univ San Martin, cruce
                                                1era cdra Av.Guardia Civil)</p>
                                        </div>
                                    </div>
                                    <div class="info-shops">
                                        <div class="content-icon float-left">
                                            <i class="icon-acc-int icon-calendar"></i>
                                        </div>
                                        <div class="content-text-shops">
                                            <p class="p-internas">Horario de atención de lunes a sábado de 9:00am a
                                                6:00pm</p>
                                        </div>
                                    </div>
                                    <div class="info-shops">
                                        <div class="content-icon float-left">
                                            <i class="icon-acc-int icon-phone2"></i>
                                        </div>
                                        <div class="content-text-shops">
                                            <p href="#" class="p-internas"><strong>
                                                <a href="#" class="color-black font-bold">01 219-4650</a>
                                                <a href="#" class="font-bold color-black a-wts"><i class="icon-whatsapp"></i> +51 993 541 157</a>
                                            </strong></p>
                                        </div>
                                    </div>
                                    <div class="content-map visible-xs visible-sm">
                                        <iframe
                                            src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3901.0807299931594!2d-77.01911368464835!3d-12.10662574631443!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x9105c8734fc26cc7%3A0xaf1b82fa9c8f1e74!2sEkono%20Llantas%20Tom%C3%A1s%20Marsano!5e0!3m2!1ses-419!2spe!4v1574887320308!5m2!1ses-419!2spe"
                                            frameborder="0" style="border:0;" allowfullscreen=""></iframe>
                                    </div>
                                </div>
                            </li>
                            <li><a href="#panama" class="accordion-heading font-regular">EKONO PANAMA -
                                    <span>Surquillo</span><i class="icon-flecha-accord"></i></a>
                                <div class="accordion-content">
                                    <div class="info-shops">
                                        <div class="content-icon float-left">
                                            <i class="icon-acc-int icon-ubc"></i>
                                        </div>
                                        <div class="content-text-shops">
                                            <p class="p-internas">Av. República de Panamá Nº5550 (cruce con la Av.
                                                Sergio Bernales)</p>
                                        </div>
                                    </div>
                                    <div class="info-shops">
                                        <div class="content-icon float-left">
                                            <i class="icon-acc-int icon-calendar"></i>
                                        </div>
                                        <div class="content-text-shops">
                                            <p class="p-internas">Horario de atención de lunes a sábado de 9:00am a
                                                6:00pm</p>
                                        </div>
                                    </div>
                                    <div class="info-shops">
                                        <div class="content-icon float-left">
                                            <i class="icon-acc-int icon-phone2"></i>
                                        </div>
                                        <div class="content-text-shops">
                                            <p href="#" class="p-internas"><strong>
                                                <a href="#" class="color-black font-bold">01 219-4650</a>
                                                <a href="#" class="font-bold color-black a-wts"><i class="icon-whatsapp"></i> +51 993 541 157</a>
                                            </strong></p>
                                        </div>
                                    </div>
                                    <div class="content-map visible-xs visible-sm">
                                        <iframe
                                            src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3900.9037982153754!2d-77.02053038464824!3d-12.11873444654427!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x9105c9479e1a2e6b%3A0x535082d70a36e448!2sEkono%20Llantas%20Panam%C3%A1!5e0!3m2!1ses-419!2spe!4v1574887381824!5m2!1ses-419!2spe"
                                            frameborder="0" style="border:0;" allowfullscreen=""></iframe>
                                    </div>
                                </div>
                            </li>
                            <li><a href="#molicentro" class="accordion-heading font-regular">EKONO MOLICENTRO - <span>La
                                        Molina</span><i class="icon-flecha-accord"></i></a>
                                <div class="accordion-content">
                                    <div class="info-shops">
                                        <div class="content-icon float-left">
                                            <i class="icon-acc-int icon-ubc"></i>
                                        </div>
                                        <div class="content-text-shops">
                                            <p class="p-internas">Av. La Molina Nº 2880 C.C. Molicentro</p>
                                        </div>
                                    </div>
                                    <div class="info-shops">
                                        <div class="content-icon float-left">
                                            <i class="icon-acc-int icon-calendar"></i>
                                        </div>
                                        <div class="content-text-shops">
                                            <p class="p-internas">Horario de atención de lunes a sábado de 9:00am a
                                                6:00pm</p>
                                        </div>
                                    </div>
                                    <div class="info-shops">
                                        <div class="content-icon float-left">
                                            <i class="icon-acc-int icon-phone2"></i>
                                        </div>
                                        <div class="content-text-shops">
                                            <p href="#" class="p-internas"><strong>
                                                <a href="#" class="color-black font-bold">01 219-4650</a>
                                                <a href="#" class="font-bold color-black a-wts"><i class="icon-whatsapp"></i> +51 993 541 157</a>
                                            </strong></p>
                                        </div>
                                    </div>
                                    <div class="content-map visible-xs visible-sm">
                                        <iframe
                                            src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2758.7262379823947!2d-76.92985970161072!3d-12.082820675335551!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x9105c0d4a904f2ab%3A0xd5aaa0ee8e74ac72!2sEkono%20Llantas!5e0!3m2!1ses-419!2spe!4v1574887451057!5m2!1ses-419!2spe"
                                            frameborder="0" style="border:0;" allowfullscreen=""></iframe>
                                    </div>
                                </div>
                            </li>
                            <li><a href="#camacho" class="accordion-heading font-regular">EKONO CAMACHO - <span>La
                                        Molina</span><i class="icon-flecha-accord"></i></a>
                                <div class="accordion-content">
                                    <div class="info-shops">
                                        <div class="content-icon float-left">
                                            <i class="icon-acc-int icon-ubc"></i>
                                        </div>
                                        <div class="content-text-shops">
                                            <p class="p-internas">Av. Javier Prado Este N° 5355 (Cruce con Av Frutales)
                                            </p>
                                        </div>
                                    </div>
                                    <div class="info-shops">
                                        <div class="content-icon float-left">
                                            <i class="icon-acc-int icon-calendar"></i>
                                        </div>
                                        <div class="content-text-shops">
                                            <p class="p-internas">Horario de atención de lunes a sábado de 9:00am a
                                                6:00pm</p>
                                        </div>
                                    </div>
                                    <div class="info-shops">
                                        <div class="content-icon float-left">
                                            <i class="icon-acc-int icon-phone2"></i>
                                        </div>
                                        <div class="content-text-shops">
                                            <p href="#" class="p-internas"><strong>
                                                <a href="#" class="color-black font-bold">01 219-4650</a>
                                                <a href="#" class="font-bold color-black a-wts"><i class="icon-whatsapp"></i> +51 993 541 157</a>
                                            </strong></p>
                                        </div>
                                    </div>
                                    <div class="content-map visible-xs visible-sm">
                                        <iframe
                                            src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3901.5177919357566!2d-76.96308129138129!3d-12.076663228858985!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x9105c6f8a3c8ef2d%3A0x6f76ff404f259f1a!2sEKONO%20LLANTAS%20CAMACHO!5e0!3m2!1ses-419!2spe!4v1574887493135!5m2!1ses-419!2spe"
                                            frameborder="0" style="border:0;" allowfullscreen=""></iframe>
                                    </div>
                                </div>
                            </li>
                        </ul>
                        <div class="wrapper-map-desktop hidden-xs hidden-sm">
                            <div class="content-map show" id="la-marina">
                                <iframe
                                    src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1950.7345570045607!2d-77.07625060039213!3d-12.080003937576262!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x9105c90c4f889a53%3A0x69f19d1ac89e8131!2sAv.%20la%20Marina%201200%2C%20Cercado%20de%20Lima%2015084!5e0!3m2!1ses-419!2spe!4v1574878837995!5m2!1ses-419!2spe"
                                    frameborder="0" style="border:0;" allowfullscreen=""></iframe>
                            </div>
                            <div class="content-map" id="primavera">
                                <iframe
                                    src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3901.0315943163155!2d-76.97918608464833!3d-12.109989646378281!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x9105c7974694d52b%3A0x273c3d480da3866c!2sAv.%20Primavera%201195%2C%20Santiago%20de%20Surco%2015023!5e0!3m2!1ses-419!2spe!4v1574886766391!5m2!1ses-419!2spe"
                                    frameborder="0" style="border:0;" allowfullscreen=""></iframe>
                            </div>
                            <div class="content-map" id="benavides">
                                <iframe
                                    src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3900.7659263065407!2d-76.99596388464805!3d-12.128161746723425!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x9105c7f604a64219%3A0x195b83fd19af1c28!2sAv.%20Alfredo%20Benavides%203999%2C%20Cercado%20de%20Lima%2015038!5e0!3m2!1ses-419!2spe!4v1574886848558!5m2!1ses-419!2spe"
                                    frameborder="0" style="border:0;" allowfullscreen=""></iframe>
                            </div>
                            <div class="content-map" id="rambla">
                                <iframe
                                    src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3280.615167665316!2d-77.00767658994178!3d-12.089484515488243!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x9105c7d40410676d%3A0x997e37c0e0fbbd48!2sLa%20Rambla%20San%20Borja!5e0!3m2!1ses-419!2spe!4v1574886966390!5m2!1ses-419!2spe"
                                    frameborder="0" style="border:0;" allowfullscreen=""></iframe>
                            </div>
                            <div class="content-map" id="miraflores">
                                <iframe
                                    src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2319.489654805363!2d-77.03805598637105!3d-12.118920935299927!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x9105c83d29fd3675%3A0x72c1da4e2199ff7c!2sAv.%20Jos%C3%A9%20Pardo%20748%2C%20Lima%2015074!5e0!3m2!1ses-419!2spe!4v1574887107819!5m2!1ses-419!2spe"
                                    frameborder="0" style="border:0;" allowfullscreen=""></iframe>
                            </div>
                            <div class="content-map" id="tomas-marsano">
                                <iframe
                                    src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3901.0807299931594!2d-77.01911368464835!3d-12.10662574631443!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x9105c8734fc26cc7%3A0xaf1b82fa9c8f1e74!2sEkono%20Llantas%20Tom%C3%A1s%20Marsano!5e0!3m2!1ses-419!2spe!4v1574887320308!5m2!1ses-419!2spe"
                                    frameborder="0" style="border:0;" allowfullscreen=""></iframe>
                            </div>
                            <div class="content-map" id="panama">
                                <iframe
                                    src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3900.9037982153754!2d-77.02053038464824!3d-12.11873444654427!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x9105c9479e1a2e6b%3A0x535082d70a36e448!2sEkono%20Llantas%20Panam%C3%A1!5e0!3m2!1ses-419!2spe!4v1574887381824!5m2!1ses-419!2spe"
                                    frameborder="0" style="border:0;" allowfullscreen=""></iframe>
                            </div>
                            <div class="content-map" id="molicentro">
                                <iframe
                                    src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2758.7262379823947!2d-76.92985970161072!3d-12.082820675335551!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x9105c0d4a904f2ab%3A0xd5aaa0ee8e74ac72!2sEkono%20Llantas!5e0!3m2!1ses-419!2spe!4v1574887451057!5m2!1ses-419!2spe"
                                    frameborder="0" style="border:0;" allowfullscreen=""></iframe>
                            </div>
                            <div class="content-map" id="camacho">
                                <iframe
                                    src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3901.5177919357566!2d-76.96308129138129!3d-12.076663228858985!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x9105c6f8a3c8ef2d%3A0x6f76ff404f259f1a!2sEKONO%20LLANTAS%20CAMACHO!5e0!3m2!1ses-419!2spe!4v1574887493135!5m2!1ses-419!2spe"
                                    frameborder="0" style="border:0;" allowfullscreen=""></iframe>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
    <?php
        include 'src/includes/footer.php'
    ?>
    <script>
        $('.accordion li a').click(function (e) {
            var href = $(this).attr('href');
            $('.accordion li a[href="' + href + '"]').closest('li').addClass('active');

            $('.wrapper-map-desktop .content-map').removeClass('show');
            $('.wrapper-map-desktop .content-map' + href).addClass('show');
        });
    </script>
</body>

</html>