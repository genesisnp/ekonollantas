<?php
    $show_menu_color = true;
    include 'src/includes/header.php'
?>
    <main class="main-shopping-cart">
        <section class="container">
            <div class="row">
                <div class="col-xs-12 col-md-8">
                    <form action="#" class="row">
                        <h2 class="col-xs-12 font-bold title-cart">TU CARRO</h2>
                        <div class="table-cart col-xs-12">
                            <div class="table-header-cart row">
                                <div class="celda-header text-center col-xs-1"></div>
                                <div class="celda-header text-center col-xs-7 col-sm-2"><span
                                        class="font-regular text-uppercase">producto</span></div>
                                <div class="celda-header text-center col-xs-3 col-sm-2"><span
                                        class="font-regular text-uppercase">cantidad</span></div>
                                <div class="hidden-xs celda-header text-center col-xs-3"><span
                                        class="font-regular text-uppercase">precio unitario</span></div>
                                <div class="hidden-xs celda-header text-center col-xs-3"><span
                                        class="font-regular text-uppercase">precio total</span></div>
                                <div class="hidden-xs celda-header col-xs-1"><span
                                        class="font-regular text-uppercase"></span>
                                </div>
                            </div>
                            <div class="table-body-cart">
                                <div class="table-detail-cart row">
                                    <div class="celda-detail text-center col-xs-3 col-sm-1">
                                        <figure class="content-img-cart d-in-block">
                                            <img src="assets/images/productos/llantas.png" alt=""
                                                class="img-detail-cart">
                                        </figure>
                                    </div>
                                    <div class="celda-detail text-center col-xs-5 col-sm-2">
                                        <div class="product-cart text-uppercase d-in-block">
                                            <span class="span-prod-cart font-bold">235/45r18</span>
                                            <span class="span-prod-cart font-regular">firestone</span>
                                            <span class="span-prod-cart cod font-regular">sku: 0001981</span>
                                        </div>
                                    </div>
                                    <div class="celda-detail text-center col-xs-4 col-sm-2">
                                        <div class="add-purchase d-flex">
                                            <div class="qty mt-5">
                                                <span class="minus bg-dark">-</span>
                                                <input type="number" class="count font-bold" name="qty" value="1">
                                                <span class="plus bg-dark">+</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="celda-detail text-center col-xs-5 col-sm-3">
                                        <h1 class="visible-xs titles-mob-carr color-primary font-bold">Prec.Un</h1>
                                        <span class="font-bold text-uppercase">S/ 720</span>
                                    </div>
                                    <div class="celda-detail text-center col-xs-4 col-sm-3">
                                        <h1 class="visible-xs titles-mob-carr color-primary font-bold">Total</h1>
                                        <span class="font-bold text-uppercase">S/ 2.880</span>
                                    </div>
                                    <div class="celda-detail text-center col-xs-12 col-sm-1 btn-mob-delete"><span class="icon-delete"></span>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </form>
                    <form action="#" class="form row datos-comprador">
                        <h2 class="title-int-cart text-uppercase font-bold col-xs-12">Datos del comprador</h2>
                        <div class="form__wrapper col-xs-12 col-md-4">
                            <input type="text" class="form__input bg-input" id="email-cart" name="email-cart">
                            <label class="form__label">
                                <span class="form__label-content">e-mail:</span>
                            </label>
                        </div>
                        <div class="form__wrapper col-xs-12 col-sm-6 col-md-4">
                            <input type="text" class="form__input bg-input" id="name-cart" name="name-cart">
                            <label class="form__label">
                                <span class="form__label-content">nombres:</span>
                            </label>
                        </div>
                        <div class="form__wrapper col-xs-12 col-sm-6 col-md-4">
                            <input type="text" class="form__input bg-input" id="lastname-cart" name="lastname-cart">
                            <label class="form__label">
                                <span class="form__label-content">apellidos:</span>
                            </label>
                        </div>
                        <div class="form__wrapper col-xs-12 col-sm-6 col-md-4">
                            <input type="text" class="form__input bg-input" id="dni-cart" name="dni-cart">
                            <label class="form__label">
                                <span class="form__label-content">dni:</span>
                            </label>
                        </div>
                        <div class="form__wrapper col-xs-12 col-sm-6 col-md-4">
                            <input type="text" class="form__input bg-input" id="phone-cart" name="phone-cart">
                            <label class="form__label">
                                <span class="form__label-content">teléfono / celular:</span>
                            </label>
                        </div>

                        <h2 class="title-int-cart text-uppercase font-bold col-xs-12">local de recojo / instalación</h2>
                        <div class="form__wrapper col-xs-12 mb-0">
                            <div class="wrapper-select-cart">
                                <div class="button dropdown col-xs-12 col-sm-5 col-md-4">
                                    <select id="select-stores" class="font-regular">
                                        <option>SELECCIONE UNA TIENDA</option>
                                        <option value="benavides" class="stores">benavides</option>
                                        <option value="magdalena" class="stores">magdalena</option>
                                    </select>
                                </div>

                                <div class="output col-xs-12 col-sm-9 px-0">
                                    <div id="benavides" class="stores-info">
                                        <div class="ubication-stores">
                                            <h2 class="name-stores font-bold color-primary">EKONO BENAVIDES <i class="icon-warning"></i></h2>
                                            <span class="distrito-stores font-bold">SURCO</span>
                                            <p class="p-internas mb-0">Av. Prolongación Benavides N° 3999(A media cdra
                                                cruce con Av. Ayacucho).</p>
                                            <p class="p-internas">Horario de atención de 8:00am a 6:00pm de lunes a
                                                sábado.</p>
                                        </div>
                                        <div class="div-alert d-flex">
                                            <div class="wrapper-warning"><i class="icon-warning"></i></div>
                                            <p class="p-internas">Stock no disponible para este local, le agradeceremos
                                                seleccione otra tienda, de lo contrario se le recargará el monto de
                                                traslado y los productos serán llevadas al local en un máximo de 48
                                                horas.</p>
                                        </div>
                                        <div class="map-stores">
                                            <iframe
                                                src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3900.7659263065407!2d-76.99596388464805!3d-12.128161746723425!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x9105c7f604a64219%3A0x195b83fd19af1c28!2sAv.%20Alfredo%20Benavides%203999%2C%20Cercado%20de%20Lima%2015038!5e0!3m2!1ses-419!2spe!4v1574886848558!5m2!1ses-419!2spe"
                                                frameborder="0" style="border:0;" allowfullscreen=""></iframe>
                                        </div>
                                    </div>
                                    <div id="magdalena" class="stores-info">
                                        <div class="ubication-stores">
                                            <h2 class="name-stores font-bold color-primary">EKONO primavera</h2>
                                            <span class="distrito-stores font-bold">surco</span>
                                            <p class="p-internas mb-0">Av. Primavera Nº 1195 (Alt. Pte. Primavera)</p>
                                            <p class="p-internas">Horario de atención de 8:00am a 6:00pm de lunes a
                                                sábado.</p>
                                        </div>
                                        <div class="map-stores">
                                            <iframe
                                                src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3900.7659263065407!2d-76.99596388464805!3d-12.128161746723425!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x9105c7f604a64219%3A0x195b83fd19af1c28!2sAv.%20Alfredo%20Benavides%203999%2C%20Cercado%20de%20Lima%2015038!5e0!3m2!1ses-419!2spe!4v1574886848558!5m2!1ses-419!2spe"
                                                frameborder="0" style="border:0;" allowfullscreen=""></iframe>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <h2 class="title-int-cart text-uppercase font-bold col-xs-12">comprobante de pago</h2>
                        <div class="wrapper-tabs-pago col-xs-12">
                            <div class="row">
                                <ul class="nav nav-tabs nav-pills plantac col-xs-12 col-sm-5" id="myTabControls">
                                    <li class="nav-link active">
                                        <a data-toggle="tab" href="#boleta" class="nav-link-a font-regular">
                                            <div class="icons-lista"></div>BOLETA
                                        </a>
                                    </li>
                                    <li class="nav-link">
                                        <a data-toggle="tab" href="#factura" class="nav-link-a font-regular">
                                            <div class="icons-lista"></div>FACTURA
                                        </a>
                                    </li>
                                </ul>

                                <div class="tab-content col-xs-12" id="content-solutions">
                                    <div id="boleta" class="tab-pane fade in active row">
                                        <div class="form__wrapper col-xs-12 col-sm-6 col-md-4">
                                            <input type="text" class="form__input bg-input" id="name" name="name">
                                            <label class="form__label">
                                                <span class="form__label-content">nombres:</span>
                                            </label>
                                        </div>
                                        <div class="form__wrapper col-xs-12 col-sm-6 col-md-4">
                                            <input type="text" class="form__input bg-input" id="lastname"
                                                name="lastname">
                                            <label class="form__label">
                                                <span class="form__label-content">apellidos:</span>
                                            </label>
                                        </div>
                                        <div class="form__wrapper col-xs-12 col-sm-6 col-md-4">
                                            <input type="text" class="form__input bg-input" id="dni" name="dni">
                                            <label class="form__label">
                                                <span class="form__label-content">dni:</span>
                                            </label>
                                        </div>
                                    </div>
                                    <div id="factura" class="tab-pane fade in row">
                                        <div class="form__wrapper col-xs-12 col-sm-6">
                                            <input type="text" class="form__input bg-input" id="name" name="name">
                                            <label class="form__label">
                                                <span class="form__label-content">razón social:</span>
                                            </label>
                                        </div>
                                        <div class="form__wrapper col-xs-12 col-sm-6">
                                            <input type="text" class="form__input bg-input" id="lastname"
                                                name="lastname">
                                            <label class="form__label">
                                                <span class="form__label-content">ruc:</span>
                                            </label>
                                        </div>
                                        <div class="form__wrapper col-xs-12 col-sm-6">
                                            <input type="text" class="form__input bg-input" id="dni" name="dni">
                                            <label class="form__label">
                                                <span class="form__label-content">dirección:</span>
                                            </label>
                                        </div>
                                        <div class="form__wrapper col-xs-12 col-sm-6">
                                            <input type="text" class="form__input bg-input" id="n-veh" name="n-veh">
                                            <label class="form__label">
                                                <span class="form__label-content">placa de vehículo:</span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <h2 class="title-int-cart text-uppercase font-bold col-xs-12">FORMA DE PAGO</h2>
                        <div class="col-xs-12 col-sm-6 select-check select-circle">
                            <div class="checkbox d-in-block">
                                <label class="font-regular label-pol text-uppercase">
                                    <input type="checkbox" /><i class="helper"></i><span>débito /
                                        crédito</span>
                                </label>
                                <div class="img-tarjetas">
                                    <img src="assets/images/logos/visa.jpg" alt="">
                                    <img src="assets/images/logos/mastercard.jpg" alt="">
                                    <img src="assets/images/logos/amerExpress.jpg" alt="">
                                    <img src="assets/images/logos/dinersClub.jpg" alt="">
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="col-xs-12 col-md-4">
                    <div class="pay-fixed">
                        <div class="wrapper-pay">
                            <div class="total text-center pd-pay">
                                <h2 class="title-total font-bold text-uppercase">TOTAL A PAGAR</h2>
                                <span class="text-uppercase font-regular span-peq">(3 productos)</span>
                                <div class="quantity-pay">
                                    <h2 class="final-payment font-bold color-primary">2.880</h2>
                                    <span class="title-igv font-regular">Incluye IGV</span>
                                </div>
                            </div>
                            <!--CUPÓN DE DSCTO-->
                            <div class="discount-coupon text-center">
                                <div class="pd-pay">
                                    <h2 class="title-dsct font-bold color-primary">CUPÓN DE DESCUENTO</h2>
                                    <p class="p-internas">Introduzca el código y haga click en el botón "VALIDAR" para
                                        actualizar el precio total.</p>
                                    <div class="content-btn-valid-dsct">
                                        <div class="div-input">
                                            <input type="text" class="float-left font-regular"
                                                placeholder="INGRESAR CÓDIGO">
                                            <button class="btn-val float-left font-bold">VALIDAR</button>
                                            <button class="btn-delete"><i
                                                    class="icon-delete color-primary"></i></button>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <!--BTN-PAGAR-->
                            <div class="btn-pay-scomp pd-pay">
                                <div class="checkbox">
                                    <label class="font-regular label-pol">
                                        <input type="checkbox" /><i class="helper"></i><span>He leído y acepto los<span
                                                class="span-pol color-primary btn-modals"> Términos y
                                                Condiciones</span> y la <span
                                                class="span-pol color-primary btn-modals">Política
                                                de Privacidad</span></span>
                                    </label>
                                </div>
                                <div class="text-center">
                                    <a href="#" type="btn" class="btn-pay text-center font-bold">
                                        <div class="azul">
                                        PAGAR <img src="assets/images/icons/check.svg" alt="">
                                        </div>
                                    </a>
                                </div>
                                <a href="#" class="font-bold text-center sg-comp d-in-block">SEGUIR COMPRANDO</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>

    <?php
        include 'src/includes/footer.php'
    ?>
    <script src="assets/js/counting.js"></script>

    <script>
        $('.plantac li a').click(function (e) {
            var href = $(this).attr('href');
            $('.nav-tabs li').removeClass('active');
            $('.nav-tabs li a[href="' + href + '"]').closest('li').addClass('active');
            $('.tab-pane').removeClass('active');
            $('.tab-pane' + href).addClass('active');
        })
    </script>
    <script>
        $('#select-stores').change(function () {
            $('.stores-info').hide();
            $('#' + $(this).val()).show();
        });
    </script>
    
</body>

</html>