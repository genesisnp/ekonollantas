<?php
    include 'src/includes/header.php'
?>
    <main class="main-services">
        <section class="sct-banner">
            <div class="degrade-int"></div>
            <img class="img-banner" src="assets/images/banner/nuestros-servicios.jpg" alt="">
            <div class="content-text-banner">
                <i class="icon-banner icon-formas-de-pago"></i>
                <h2 class="title-banner text-uppercase font-bold">formas de pago</h2>
            </div>
            <a href="#payment-methods" class="icon-arrow" data-ancla="payment-methods"></a>
        </section>
        <section class="payment-methods" id="payment-methods">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 text-center">
                        <i class="icon-visa color-primary"></i>
                        <h2 class="font-bold text-uppercase">Visa débito / crédito</h2>
                        <p class="p-internas">Realiza el pago de tu compra con tarjeta de crédito / tarjeta de débito al
                            instante.</p>
                    </div>
                    <div class="col-xs-10 col-center">
                        <div class="div-imgs m-auto text-center">
                            <div class="col-xs-6 col-md-3 content-imgs-trjt wow zoomIn">
                                <img src="assets/images/internas/mastercard.jpg" alt="" class="img-trjt">
                            </div>
                            <div class="col-xs-6 col-md-3 content-imgs-trjt wow zoomIn" data-wow-delay="1s">
                                <img src="assets/images/internas/visa.jpg" alt="" class="img-trjt">
                            </div>
                            <div class="col-xs-6 col-md-3 content-imgs-trjt wow zoomIn" data-wow-delay="2s">
                                <img src="assets/images/internas/american-exp.jpg" alt="" class="img-trjt">
                            </div>
                            <div class="col-xs-6 col-md-3 content-imgs-trjt wow zoomIn" data-wow-delay="3s">
                                <img src="assets/images/internas/diners.jpg" alt="" class="img-trjt">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
    <?php
        include 'src/includes/footer.php'
    ?>
</body>

</html>