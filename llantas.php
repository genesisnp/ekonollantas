<?php
    include 'src/includes/header.php'
?>
<main>
    <section class="sct-banner">
        <div class="degrade-int"></div>
        <img class="img-banner" src="assets/images/banner/encuentra-tu-aro.jpg" alt="">
        <div class="content-text-banner prdcts">
            <h2 class="title-banner text-uppercase font-bold">Encuentra tu llanta</h2>
            <h3 class="p-internas bsc-llanta">¿no encuentras tu llanta? solícitalo <a class="font-bold" href="pedido-especial.php">AQUÍ</a></h3>
            <div class="wrapper-select-banner">
                <div class="tabs">
                    <span class="font-bold tab-link current" data-tab="tab-1"><i class="icon-tab-banner icon-llanta"></i> 
                        POR MEDIDA DE LLANTAS <button class="btn-icircle">
                            <img class="img-" src="assets/images/icons/i-circle.svg" alt="">
                        </button>
                    </span>
                    <span class="font-bold tab-link" data-tab="tab-2"><i class="icon-tab-banner icon-auto"></i>
                        POR MODELO DE VEHÍCULO</span>
                </div>

                <div class="content">
                    <div id="tab-1" class="tab-content current">
                        <div class="wrapper-select-cart select-banner-one">
                            <div class="button dropdown">
                                <select id="select-stores" class="font-bold">
                                    <option>ANCHO :</option>
                                    <option value="125" class="stores">125</option>
                                    <option value="130" class="stores">130</option>
                                </select>
                            </div>
                        </div>
                        <div class="wrapper-select-cart select-banner-medio">
                            <div class="button dropdown">
                                <select id="select-stores" class="font-bold">
                                    <option>PERFIL :</option>
                                    <option value="125" class="stores">125</option>
                                    <option value="130" class="stores">130</option>
                                </select>
                            </div>
                        </div>
                        <div class="wrapper-select-cart select-banner-medio">
                            <div class="button dropdown">
                                <select id="select-stores" class="font-bold">
                                    <option>ARO :</option>
                                    <option value="125" class="stores">125</option>
                                    <option value="130" class="stores">130</option>
                                </select>
                            </div>
                        </div>
                        <button class="btn-bsc-select"><i class="icon-Lupa"></i></button>
                        <img class="img-product-banner llnt visible-lg wow slideInUp" src="assets/images/banner/Llantas.png" alt="">
                    </div>
                    <div id="tab-2" class="tab-content">
                        <div class="wrapper-select-cart select-banner-one">
                            <div class="button dropdown">
                                <select id="select-stores" class="font-bold">
                                    <option>MARCA :</option>
                                    <option value="125" class="stores">125</option>
                                    <option value="130" class="stores">130</option>
                                </select>
                            </div>
                        </div>
                        <div class="wrapper-select-cart select-banner-medio">
                            <div class="button dropdown">
                                <select id="select-stores" class="font-bold">
                                    <option>AÑO :</option>
                                    <option value="125" class="stores">125</option>
                                    <option value="130" class="stores">130</option>
                                </select>
                            </div>
                        </div>
                        <div class="wrapper-select-cart select-banner-medio">
                            <div class="button dropdown">
                                <select id="select-stores" class="font-bold">
                                    <option>MODELO :</option>
                                    <option value="125" class="stores">125</option>
                                    <option value="130" class="stores">130</option>
                                </select>
                            </div>
                        </div>
                        <button class="btn-bsc-select"><i class="icon-Lupa"></i></button>
                    </div>
                    </div>
                </div>
            </div>
        </div>
        <ol class="breadcrumb font-regular text-uppercase">
            <li class="bread-item"><a class="bread-link" href="#">INICIO</a></li>
            <li class="bread-item active"><a class="bread-link" href="#">Llantas</a></li>
        </ol>
    </section>
    <section class="sct-product">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 padd-tselect">
                    <div class="">
                        <h2 class="font-bold title-busq col-xs-12 col-sm-6">Resultado de búsqueda: <span class="color-primary">VF542 Eliminator</span></h2>
                        <div class="select-int col-xs-12 col-sm-6">
                            <h3 class="title-left-select font-regular d-in-block">ORDENAR POR: </h3>
                            <div class="wrapper-select-cart">
                                <div class="button dropdown">
                                    <select id="select-stores" class="font-regular">
                                        <option value="mayor-menor" class="stores">Precio de mayor a menor</option>
                                        <option value="menor-mayor" class="stores">Precio de menor a mayor</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12">
                    <div class="row">
                        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                            <div class="xhov wow zoomIn">
                                <div class="wrapper-card-product">
                                    <a href="detalle-producto.php" class="link-wrapper-prod h100 w100 d-block">
                                        <div class="wrapper-img-prod h-auto">
                                            <div class="content-img-prod">
                                                <img class="w-100" src="assets/images/productos/llantas2.jpg" alt="">
                                            </div>
                                        </div>
                                        <div class="info-product text-uppercase">
                                            <h2 class="font-bold name-cod-prod">aro 15</h2>
                                            <h3 class="font-regular name-brand">DARWIN</h3>
                                            <div class="wrapper-price flex-al-cent sp-arrow">
                                                <h2 class="font-bold price-prod"><span class="font-regular price-dsct-prod">S/720.00</span> S/ 720.00</h2>
                                            </div>
                                        </div>
                                    </a>
                                    <div class="dif">
                                        <div class="add-purchase ad2b d-flex">
                                            <div class="qty mt-5">
                                                <span class="plus bg-dark">+</span>
                                                <input type="number" class="count font-regular" name="qty" value="1">
                                                <span class="minus bg-dark">-</span>
                                            </div>
                                            <div class="btn-purchase">
                                                <a href="carrito-de-compras.php" class="link-purchase font-bold text-uppercase text-center">comprar <i class="icon-carrito"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="circle-dscto d-flex">
                                        <h4 class="font-regular">-20%</h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                            <div class="xhov wow zoomIn">
                                <div class="wrapper-card-product">
                                    <a href="detalle-producto.php" class="link-wrapper-prod h100 w100 d-block">
                                        <div class="wrapper-img-prod h-auto">
                                            <div class="content-img-prod">
                                                <img class="w-100" src="assets/images/productos/llantas2.jpg" alt="">
                                            </div>
                                        </div>
                                        <div class="info-product text-uppercase">
                                            <h2 class="font-bold name-cod-prod">aro 15</h2>
                                            <h3 class="font-regular name-brand">DARWIN</h3>
                                            <div class="wrapper-price flex-al-cent sp-arrow">
                                                <h2 class="font-bold price-prod"><span class="font-regular price-dsct-prod">S/720.00</span> S/ 720.00</h2>
                                            </div>
                                        </div>
                                    </a>
                                    <div class="dif">
                                        <div class="add-purchase ad2b d-flex">
                                            <div class="qty mt-5">
                                                <span class="plus bg-dark">+</span>
                                                <input type="number" class="count font-regular" name="qty" value="1">
                                                <span class="minus bg-dark">-</span>
                                            </div>
                                            <div class="btn-purchase">
                                                <a href="carrito-de-compras.php" class="link-purchase font-bold text-uppercase text-center">comprar <i class="icon-carrito"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="circle-dscto d-flex">
                                        <h4 class="font-regular">-20%</h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                            <div class="xhov wow zoomIn">
                                <div class="wrapper-card-product">
                                    <a href="detalle-producto.php" class="link-wrapper-prod h100 w100 d-block">
                                        <div class="wrapper-img-prod h-auto">
                                            <div class="content-img-prod">
                                                <img class="w-100" src="assets/images/productos/llantas2.jpg" alt="">
                                            </div>
                                        </div>
                                        <div class="info-product text-uppercase">
                                            <h2 class="font-bold name-cod-prod">aro 15</h2>
                                            <h3 class="font-regular name-brand">DARWIN</h3>
                                            <div class="wrapper-price flex-al-cent sp-arrow">
                                                <h2 class="font-bold price-prod"><span class="font-regular price-dsct-prod">S/720.00</span> S/ 720.00</h2>
                                            </div>
                                        </div>
                                    </a>
                                    <div class="dif">
                                        <div class="add-purchase ad2b d-flex">
                                            <div class="qty mt-5">
                                                <span class="plus bg-dark">+</span>
                                                <input type="number" class="count font-regular" name="qty" value="1">
                                                <span class="minus bg-dark">-</span>
                                            </div>
                                            <div class="btn-purchase">
                                                <a href="carrito-de-compras.php" class="link-purchase font-bold text-uppercase text-center">comprar <i class="icon-carrito"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="circle-dscto d-flex">
                                        <h4 class="font-regular">-20%</h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                            <div class="xhov wow zoomIn">
                                <div class="wrapper-card-product">
                                    <a href="detalle-producto.php" class="link-wrapper-prod h100 w100 d-block">
                                        <div class="wrapper-img-prod h-auto">
                                            <div class="content-img-prod">
                                                <img class="w-100" src="assets/images/productos/llantas2.jpg" alt="">
                                            </div>
                                        </div>
                                        <div class="info-product text-uppercase">
                                            <h2 class="font-bold name-cod-prod">aro 15</h2>
                                            <h3 class="font-regular name-brand">DARWIN</h3>
                                            <div class="wrapper-price flex-al-cent sp-arrow">
                                                <h2 class="font-bold price-prod"><span class="font-regular price-dsct-prod">S/720.00</span> S/ 720.00</h2>
                                            </div>
                                        </div>
                                    </a>
                                    <div class="dif">
                                        <div class="add-purchase ad2b d-flex">
                                            <div class="qty mt-5">
                                                <span class="plus bg-dark">+</span>
                                                <input type="number" class="count font-regular" name="qty" value="1">
                                                <span class="minus bg-dark">-</span>
                                            </div>
                                            <div class="btn-purchase">
                                                <a href="carrito-de-compras.php" class="link-purchase font-bold text-uppercase text-center">comprar <i class="icon-carrito"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="circle-dscto d-flex">
                                        <h4 class="font-regular">-20%</h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                            <div class="xhov wow zoomIn">
                                <div class="wrapper-card-product">
                                    <a href="detalle-producto.php" class="link-wrapper-prod h100 w100 d-block">
                                        <div class="wrapper-img-prod h-auto">
                                            <div class="content-img-prod">
                                                <img class="w-100" src="assets/images/productos/llantas2.jpg" alt="">
                                            </div>
                                        </div>
                                        <div class="info-product text-uppercase">
                                            <h2 class="font-bold name-cod-prod">aro 15</h2>
                                            <h3 class="font-regular name-brand">DARWIN</h3>
                                            <div class="wrapper-price flex-al-cent sp-arrow">
                                                <h2 class="font-bold price-prod"><span class="font-regular price-dsct-prod">S/720.00</span> S/ 720.00</h2>
                                            </div>
                                        </div>
                                    </a>
                                    <div class="dif">
                                        <div class="add-purchase ad2b d-flex">
                                            <div class="qty mt-5">
                                                <span class="plus bg-dark">+</span>
                                                <input type="number" class="count font-regular" name="qty" value="1">
                                                <span class="minus bg-dark">-</span>
                                            </div>
                                            <div class="btn-purchase">
                                                <a href="carrito-de-compras.php" class="link-purchase font-bold text-uppercase text-center">comprar <i class="icon-carrito"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="circle-dscto d-flex">
                                        <h4 class="font-regular">-20%</h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>
    <?php
        include 'src/includes/footer.php'
    ?>
    <script>
        $('.plantac li a').click(function (e) {
            var href = $(this).attr('href');

            $('.nav-tabs li').removeClass('active');
            $('.nav-tabs li a[href="' + href + '"]').closest('li').addClass('active');
            $('.tab-pane').removeClass('active');
            $('.tab-pane' + href).addClass('active');
        })
    </script>
</body>
</html>