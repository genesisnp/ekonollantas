$('#btn-send-form').on('click', function(){
    $("#form-work-whith-us").validate({
        rules: {
            "name-pd-es": "required",
            "last-name-pd-es": "required",
            "area-work": "required",
            "ancho-pd-es": "required",
            "perfil-pd-es": "required",
            "aro-pd-es": "required",
            "marca-pd-es": "required",
            "modelo-pd-es": "required",
            "year-pd-es": "required",
            "email-pd-es": {
                required: true,
                email: true
            },
            "phone-pd-es": {
                required: true,
                minlength: 9
            },
            "textarea": {
                required: true
            }
        },
    });
})