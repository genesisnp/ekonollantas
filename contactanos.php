<?php
    include 'src/includes/header.php'
?>
    <main class="main-work-whith-us">
        <section class="sct-banner">
            <div class="degrade-int"></div>
            <img class="img-banner" src="assets/images/banner/nuestros-servicios.jpg" alt="">
            <div class="content-text-banner">
                <i class="icon-banner icon-contacto"></i>
                <h2 class="title-banner text-uppercase font-bold">contáctanos</h2>
                <div class="p-internas">Para contactarte con nosotros por favor llena el siguiente formulario y te
                    atenderemos a la brevedad:</div>
            </div>
            <a href="#form-contact-us" class="icon-arrow" data-ancla="form-contact-us"></a>
        </section>
        <section class="form-contact-us" id="form-contact-us">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-md-8">
                        <form action="#" class="form row" method="post" id="form-contact-us">
                            <div class="col-xs-12">
                                <div class="row">
                                    <div class="form__wrapper col-xs-12 col-sm-6">
                                        <input type="text" class="form__input bg-input" id="name" name="name">
                                        <label class="form__label">
                                            <span class="form__label-content">Nombres:</span>
                                        </label>
                                    </div>
                                    <div class="form__wrapper col-xs-12 col-sm-6">
                                        <input type="text" class="form__input bg-input" id="last-name"
                                            name="last-name">
                                        <label class="form__label">
                                            <span class="form__label-content">Apellidos:</span>
                                        </label>
                                    </div>
                                    <div class="form__wrapper col-xs-12 col-sm-6">
                                        <input type="text" class="form__input bg-input" id="phone"
                                            name="phone">
                                        <label class="form__label">
                                            <span class="form__label-content">Teléfono:</span>
                                        </label>
                                    </div>
                                    <div class="form__wrapper col-xs-12 col-sm-6">
                                        <input type="email" class="form__input bg-input" id="email"
                                            name="email">
                                        <label class="form__label">
                                            <span class="form__label-content">E-mail</span>
                                        </label>
                                    </div>
                                    <div class="form__wrapper col-xs-12 h100">
                                        <textarea class="form__input form_textarea bg-input" id="textarea"
                                            name="textarea"></textarea>
                                        <label class="form__label">
                                            <span class="form__label-content">Mensaje</span>
                                        </label>
                                    </div>
                                    <div class="col-xs-12">
                                        <div class="checkbox">
                                            <label class="font-regular label-pol">
                                                <input type="checkbox" /><i class="helper"></i><span>He leído y acepto
                                                    la <span class="span-pol color-primary btn-modals">Política
                                                        de Privacidad</span></span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-6">
                                        <div class="btn-container">
                                            <button type="submit" class="btn-send font-bold"
                                                id="btn-send-form">ENVIAR</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="col-xs-12 col-md-4 mt-inf">
                        <div class="row">
                            <div class="col-xs-12 info-form-contact d-flex">
                                <div class="icon-info-form d-in-block">
                                    <i class="icons-cntc icon-calendar"></i>
                                </div>
                                <div class="info-descrp d-in-block">
                                    <p class="p-internas">Horario de atención de Lunes a Sábado (9:00 am - 6:00 pm.)</p>
                                </div>
                            </div>
                            <div class="col-xs-12 info-form-contact d-flex">
                                <div class="icon-info-form d-in-block">
                                    <i class="icons-cntc icon-phone2"></i>
                                </div>
                                <div class="info-descrp d-in-block">
                                    <a href="#" class="text-phns font-regular">01 219 4650</a>
                                </div>
                            </div>
                            <div class="col-xs-12 info-form-contact d-flex">
                                <div class="icon-info-form d-in-block">
                                    <i class="icons-cntc icon-whatsapp"></i>
                                </div>
                                <div class="info-descrp d-in-block">
                                    <a href="#" class="text-phns font-regular">+51 994 189 768</a>
                                </div>
                            </div>
                            <!-- <div class="col-xs-12 info-form-contact d-flex">
                                <div class="icon-info-form d-in-block">
                                    <i class="icons-cntc icon-ubc"></i>
                                </div>
                                <div class="info-descrp d-in-block">
                                    <p class="p-internas">Dirección: Av.La Marina Nro. 1200 - Pueblo Libre</p>
                                </div>
                            </div> -->
                            <div class="col-xs-12 info-form-contact d-flex">
                                <div class="icon-info-form d-in-block">
                                    <i class="icons-cntc icon-email"></i>
                                </div>
                                <div class="info-descrp d-in-block">
                                    <a href="#" class="p-internas">compraenlinea@ekonollantas.com</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
    <?php
        include 'src/includes/footer.php'
    ?>
    <script src="assets/js/form-work.js"></script>
</body>

</html>