<?php
    include 'src/includes/header.php'
?>
    <main class="main-claim-book">
        <section class="sct-banner">
            <div class="degrade-int"></div>
            <img class="img-banner" src="assets/images/banner/nuestros-servicios.jpg" alt="">
            <div class="content-text-banner">
                <i class="icon-banner icon-libro-reclamos"></i>
                <h2 class="title-banner text-uppercase font-bold">libro de reclamaciones</h2>
                <div class="p-internas">Conforme a lo establecido en el Código de Protección y Defensa del Consumidor
                    Ekonolantas tienen a su disposición el Libro de Reclamaciones correspondiente en el que usted podrá
                    efectuar las anotaciones que crea conveniente.</div>
            </div>
            <a href="#" class="icon-arrow"></a>
        </section>
        <section class="form-claim-book">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-md-8 col-center">
                        <form action="#" class="form row" method="post" id="form-claim-book">
                            <div class="xs-12 info-company-cb">
                                <div class="row">
                                    <div class="col-xs-6">
                                        <p class="p-internas font-regular text-uppercase">IZOO S.A</p>
                                        <p class="p-internas font-regular text-uppercase">r.u.c: 20100310369</p>
                                        <p class="p-internas font-regular">Dirección: Av.La Marina Nro. 1200 - Pueblo
                                            Libre</p>
                                    </div>
                                    <div class="col-xs-6 text-right">
                                        <p class="p-internas font-regular"><strong>Hoja de reclamación N°:
                                                2019-00000001</strong>
                                        </p>
                                        <p class="p-internas font-regular">Fecha: 28-11-2019</p>
                                        <p class="p-internas font-regular">Hora: 11:36:09 ams</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 px-0">
                                <div class="">
                                    <div class="col-xs-12">
                                        <h2 class="title-claim-book font-bold">1. Identificación del consumidor
                                            reclamante</h2>
                                    </div>
                                    <div class="form__wrapper col-xs-12 col-sm-6">
                                        <input type="text" class="form__input bg-input" id="name-claim-book"
                                            name="name-claim-book">
                                        <label class="form__label">
                                            <span class="form__label-content">Nombres:</span>
                                        </label>
                                    </div>
                                    <div class="form__wrapper col-xs-12 col-sm-6">
                                        <input type="text" class="form__input bg-input" id="home-claim-book"
                                            name="home-claim-book">
                                        <label class="form__label">
                                            <span class="form__label-content">Domicilio:</span>
                                        </label>
                                    </div>
                                    <div class="form__wrapper col-xs-12 col-sm-6">
                                        <input type="text" class="form__input bg-input" id="phone-claim-book"
                                            name="phone-claim-book">
                                        <label class="form__label">
                                            <span class="form__label-content">Teléfono:</span>
                                        </label>
                                    </div>
                                    <div class="form__wrapper col-xs-12 col-sm-6">
                                        <input type="email" class="form__input bg-input" id="email-claim-book"
                                            name="email-claim-book">
                                        <label class="form__label">
                                            <span class="form__label-content">E-mail</span>
                                        </label>
                                    </div>
                                    <div class="form__wrapper col-xs-12 col-sm-6">
                                        <input type="text" class="form__input bg-input" id="dni-claim-book"
                                            name="dni-claim-book">
                                        <label class="form__label">
                                            <span class="form__label-content">DNI:</span>
                                        </label>
                                    </div>
                                    <div class="form__wrapper col-xs-12 col-sm-6">
                                        <input type="text" class="form__input bg-input" id="parents-claim-book"
                                            name="parents-claim-book">
                                        <label class="form__label">
                                            <span class="form__label-content">padre o madre (Sólo menores de
                                                edad):</span>
                                        </label>
                                    </div>
                                    <div class="col-xs-12">
                                        <h2 class="title-claim-book font-bold">2. Identificación del bien contratado
                                        </h2>
                                    </div>
                                    <div class="col-xs-12 col-sm-6 select-check select-circle">
                                        <div class="checkbox d-in-block">
                                            <label class="font-regular label-pol">
                                                <input type="radio" name="gender" value="" /><i class="helper"></i><span>PRODUCTO</span>
                                            </label>
                                        </div>
                                        <div class="checkbox d-in-block">
                                            <label class="font-regular label-pol">
                                                <input type="radio" name="gender" value=""/><i class="helper"></i><span>SERVICIO</span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="form__wrapper col-xs-12 col-sm-6">
                                        <input type="text" class="form__input bg-input" id="quantity-claim-book"
                                            name="quantity-claim-book">
                                        <label class="form__label">
                                            <span class="form__label-content">monto reclamado:</span>
                                        </label>
                                    </div>
                                    <div class="form__wrapper col-xs-12">
                                        <textarea class="form__input form_textarea bg-input" id="textarea"
                                            name="textarea"></textarea>
                                        <label class="form__label">
                                            <span class="form__label-content">Mensaje</span>
                                        </label>
                                    </div>
                                    <div class="col-xs-12">
                                        <h2 class="title-claim-book font-bold">3. Detalle de la Reclamación</h2>
                                    </div>
                                    <div class="col-xs-12 select-check select-circle">
                                        <div class="checkbox">
                                            <label class="font-regular label-pol">
                                                <input type="radio" name="genders" value=""/><i class="helper"></i><span>RECLAMO
                                                    (Disconformidad relacionada a los productos o servicios)</span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 select-check select-circle">
                                        <div class="checkbox">
                                            <label class="font-regular label-pol">
                                                <input type="radio" name="genders" value=""/><i class="helper"></i><span>QUEJA
                                                    (Disconformidad no relacionada a los productos o servicios; o,
                                                    malestar o descontento respecto a la atención al público)</span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="form__wrapper col-xs-12">
                                        <textarea class="form__input form_textarea bg-input" id="textarea"
                                            name="textarea"></textarea>
                                        <label class="form__label">
                                            <span class="form__label-content">Descripción:</span>
                                        </label>
                                    </div>
                                    <div class="form__wrapper col-xs-12">
                                        <textarea class="form__input form_textarea bg-input" id="textarea"
                                            name="textarea"></textarea>
                                        <label class="form__label">
                                            <span class="form__label-content">pedido:</span>
                                        </label>
                                    </div>
                                    <div class="col-xs-12">
                                        <h2 class="title-claim-book font-bold">4. Acciones adoptadas por el proveedor
                                        </h2>
                                    </div>
                                    <div class="form__wrapper col-xs-12">
                                        <textarea class="form__input form_textarea bg-input" id="textarea"
                                            name="textarea"></textarea>
                                        <label class="form__label">
                                            <span class="form__label-content">Descripción:</span>
                                        </label>
                                    </div>
                                    <div class="col-xs-12">
                                        <div class="checkbox">
                                            <label class="font-regular label-pol">
                                                <input type="checkbox" /><i class="helper"></i><span>He leído y acepto
                                                    la <span class="span-pol color-primary btn-modals">Política
                                                        de Privacidad</span></span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-6">
                                        <div class="btn-container">
                                            <button type="submit" class="btn-send font-bold"
                                                id="btn-send-form">ENVIAR</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
    </main>
    <?php
        include 'src/includes/footer.php'
    ?>
    <script src="assets/js/form-work.js"></script>
</body>

</html>