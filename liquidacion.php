<?php
    include 'src/includes/header.php'
?>
    <main class="main-liquidation">
        <section class="sct-banner">
            <div class="degrade-int"></div>
            <img class="img-banner" src="assets/images/banner/nuestros-servicios.jpg" alt="">
            <div class="content-text-banner">
                <i class="icon-banner icon-liquidacion"></i>
                <h2 class="title-banner text-uppercase font-bold">liquidacion</h2>
                <p class="p-internas">¡Productos en liquidación a precio de costo y menos!</p>
            </div>
            <a href="#sct-liquidation" class="icon-arrow" data-ancla="sct-liquidation"></a>
        </section>
        <section class="sct-liquidation" id="sct-liquidation">
            <div class="container">
                <div class="row">
                    <!-- select-interno -->
                    <div class="col-xs-12 padd-tselect">
                        <div class="">
                            <div class="select-int col-xs-12 col-sm-6">
                                <h3 class="title-left-select font-regular d-in-block">ORDENAR POR: </h3>
                                <div class="wrapper-select-cart">
                                    <div class="button dropdown">
                                        <select id="select-stores" class="font-regular">
                                            <option value="mayor-menor" class="stores">Precio de mayor a menor</option>
                                            <option value="menor-mayor" class="stores">Precio de menor a mayor</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- card-products -->
                    <div class="col-xs-12 col-sm-4 col-md-3 col-lg-3">
                        <div class="xhov wow zoomIn">
                            <div class="wrapper-card-product">
                                <a href="detalle-producto.php" class="link-wrapper-prod h100 w100 d-block">
                                    <div class="wrapper-img-prod h-auto">
                                        <div class="content-img-prod">
                                            <img class="w-100" src="assets/images/productos/aro.jpg" alt="">
                                        </div>
                                    </div>
                                    <div class="info-product text-uppercase">
                                        <h2 class="font-bold name-cod-prod">235/45R18</h2>
                                        <h3 class="font-regular name-brand">DARWIN</h3>
                                        <div class="wrapper-price flex-al-cent sp-arrow">
                                            <h2 class="font-bold price-prod"><span class="font-regular price-dsct-prod">S/720.00</span> S/ 720.00</h2>
                                        </div>
                                    </div>
                                </a>
                                <div class="dif">
                                    <div class="add-purchase ad2b d-flex">
                                        <div class="qty mt-5">
                                            <span class="plus bg-dark">+</span>
                                            <input type="number" class="count font-regular" name="qty" value="1">
                                            <span class="minus bg-dark">-</span>
                                        </div>
                                        <div class="btn-purchase">
                                            <a href="carrito-de-compras.php" class="link-purchase font-bold text-uppercase text-center">comprar <i class="icon-carrito"></i></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="circle-dscto d-flex">
                                    <h4 class="font-regular">-20%</h4>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-4 col-md-3 col-lg-3">
                        <div class="xhov wow zoomIn">
                            <div class="wrapper-card-product">
                                <a href="detalle-producto.php" class="link-wrapper-prod h100 w100 d-block">
                                    <div class="wrapper-img-prod h-auto">
                                        <div class="content-img-prod">
                                            <img class="w-100" src="assets/images/productos/aro.jpg" alt="">
                                        </div>
                                    </div>
                                    <div class="info-product text-uppercase">
                                        <h2 class="font-bold name-cod-prod">235/45R18</h2>
                                        <h3 class="font-regular name-brand">DARWIN</h3>
                                        <div class="wrapper-price flex-al-cent sp-arrow">
                                            <h2 class="font-bold price-prod"><span class="font-regular price-dsct-prod">S/720.00</span> S/ 720.00</h2>
                                        </div>
                                    </div>
                                </a>
                                <div class="dif">
                                    <div class="add-purchase ad2b d-flex">
                                        <div class="qty mt-5">
                                            <span class="plus bg-dark">+</span>
                                            <input type="number" class="count font-regular" name="qty" value="1">
                                            <span class="minus bg-dark">-</span>
                                        </div>
                                        <div class="btn-purchase">
                                            <a href="carrito-de-compras.php" class="link-purchase font-bold text-uppercase text-center">comprar <i class="icon-carrito"></i></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="circle-dscto d-flex">
                                    <h4 class="font-regular">-20%</h4>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-4 col-md-3 col-lg-3">
                        <div class="xhov wow zoomIn">
                            <div class="wrapper-card-product">
                                <a href="detalle-producto.php" class="link-wrapper-prod h100 w100 d-block">
                                    <div class="wrapper-img-prod h-auto">
                                        <div class="content-img-prod">
                                            <img class="w-100" src="assets/images/productos/aro.jpg" alt="">
                                        </div>
                                    </div>
                                    <div class="info-product text-uppercase">
                                        <h2 class="font-bold name-cod-prod">235/45R18</h2>
                                        <h3 class="font-regular name-brand">DARWIN</h3>
                                        <div class="wrapper-price flex-al-cent sp-arrow">
                                            <h2 class="font-bold price-prod"><span class="font-regular price-dsct-prod">S/720.00</span> S/ 720.00</h2>
                                        </div>
                                    </div>
                                </a>
                                <div class="dif">
                                    <div class="add-purchase ad2b d-flex">
                                        <div class="qty mt-5">
                                            <span class="plus bg-dark">+</span>
                                            <input type="number" class="count font-regular" name="qty" value="1">
                                            <span class="minus bg-dark">-</span>
                                        </div>
                                        <div class="btn-purchase">
                                            <a href="carrito-de-compras.php" class="link-purchase font-bold text-uppercase text-center">comprar <i class="icon-carrito"></i></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="circle-dscto d-flex">
                                    <h4 class="font-regular">-20%</h4>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-4 col-md-3 col-lg-3">
                        <div class="xhov wow zoomIn">
                            <div class="wrapper-card-product">
                                <a href="detalle-producto.php" class="link-wrapper-prod h100 w100 d-block">
                                    <div class="wrapper-img-prod h-auto">
                                        <div class="content-img-prod">
                                            <img class="w-100" src="assets/images/productos/aro.jpg" alt="">
                                        </div>
                                    </div>
                                    <div class="info-product text-uppercase">
                                        <h2 class="font-bold name-cod-prod">235/45R18</h2>
                                        <h3 class="font-regular name-brand">DARWIN</h3>
                                        <div class="wrapper-price flex-al-cent sp-arrow">
                                            <h2 class="font-bold price-prod"><span class="font-regular price-dsct-prod">S/720.00</span> S/ 720.00</h2>
                                        </div>
                                    </div>
                                </a>
                                <div class="dif">
                                    <div class="add-purchase ad2b d-flex">
                                        <div class="qty mt-5">
                                            <span class="plus bg-dark">+</span>
                                            <input type="number" class="count font-regular" name="qty" value="1">
                                            <span class="minus bg-dark">-</span>
                                        </div>
                                        <div class="btn-purchase">
                                            <a href="carrito-de-compras.php" class="link-purchase font-bold text-uppercase text-center">comprar <i class="icon-carrito"></i></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="circle-dscto d-flex">
                                    <h4 class="font-regular">-20%</h4>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-4 col-md-3 col-lg-3">
                        <div class="xhov wow zoomIn">
                            <div class="wrapper-card-product">
                                <a href="detalle-producto.php" class="link-wrapper-prod h100 w100 d-block">
                                    <div class="wrapper-img-prod h-auto">
                                        <div class="content-img-prod">
                                            <img class="w-100" src="assets/images/productos/aro.jpg" alt="">
                                        </div>
                                    </div>
                                    <div class="info-product text-uppercase">
                                        <h2 class="font-bold name-cod-prod">235/45R18</h2>
                                        <h3 class="font-regular name-brand">DARWIN</h3>
                                        <div class="wrapper-price flex-al-cent sp-arrow">
                                            <h2 class="font-bold price-prod"><span class="font-regular price-dsct-prod">S/720.00</span> S/ 720.00</h2>
                                        </div>
                                    </div>
                                </a>
                                <div class="dif">
                                    <div class="add-purchase ad2b d-flex">
                                        <div class="qty mt-5">
                                            <span class="plus bg-dark">+</span>
                                            <input type="number" class="count font-regular" name="qty" value="1">
                                            <span class="minus bg-dark">-</span>
                                        </div>
                                        <div class="btn-purchase">
                                            <a href="carrito-de-compras.php" class="link-purchase font-bold text-uppercase text-center">comprar <i class="icon-carrito"></i></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="circle-dscto d-flex">
                                    <h4 class="font-regular">-20%</h4>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </section>
    </main>
    <?php
        include 'src/includes/footer.php'
    ?>

</body>

</html>