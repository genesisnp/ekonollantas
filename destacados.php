<?php
    include 'src/includes/header.php'
?>
    <main class="main-home" id="fullpage">
        <section class="section sct-video" id="section0">
            <div class="degrade"></div>
            <div class="bg-blue"></div>
            <div class="h100">
                <video data-autoplay autoplay loop muted class="video">
                    <source type="video/mp4" src="assets/video/video_3.mp4">
                </video>
                <!-- <img class="img-home-mobile" src="desarrollo/images/index12.jpg" alt=""> -->
                <div class="content-text-banner">
                    <h2 class="title-banner text-uppercase font-bold fsb">Encuentra tu llanta</h2>
                    <h3 class="font-regular bsc-llanta">¿no encuentras tu llanta? solícitalo <a class="font-bold" href="pedido-especial.php">AQUÍ</a></h3>
                    <div class="wrapper-select-banner">
                        <div class="tabs">
                            <span class="font-bold tab-link current" data-tab="tab-1"><i class="icon-tab-banner icon-llanta"></i> 
                                POR MEDIDA DE LLANTAS <button class="btn-icircle">
                                    <img class="img-" src="assets/images/icons/i-circle.svg" alt="">
                                </button>
                            </span>
                            <span class="font-bold tab-link" data-tab="tab-2"><i class="icon-tab-banner icon-auto"></i>
                                POR MODELO DE VEHÍCULO</span>
                        </div>

                        <div class="content">
                            <div id="tab-1" class="tab-content current">
                                <div class="wrapper-select-cart select-banner-one">
                                    <div class="button dropdown">
                                        <select id="select-stores" class="font-bold">
                                            <option>ANCHO :</option>
                                            <option value="125" class="stores">125</option>
                                            <option value="130" class="stores">130</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="wrapper-select-cart select-banner-medio">
                                    <div class="button dropdown">
                                        <select id="select-stores" class="font-bold">
                                            <option>PERFIL :</option>
                                            <option value="125" class="stores">125</option>
                                            <option value="130" class="stores">130</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="wrapper-select-cart select-banner-medio">
                                    <div class="button dropdown">
                                        <select id="select-stores" class="font-bold">
                                            <option>ARO :</option>
                                            <option value="125" class="stores">125</option>
                                            <option value="130" class="stores">130</option>
                                        </select>
                                    </div>
                                </div>
                                <button class="btn-bsc-select"><i class="icon-Lupa"></i></button>
                                <img class="img-product-banner llnt visible-lg" src="assets/images/banner/Llantas.png" alt="">
                            </div>
                            <div id="tab-2" class="tab-content">
                                <div class="wrapper-select-cart select-banner-one">
                                    <div class="button dropdown">
                                        <select id="select-stores" class="font-bold">
                                            <option>MARCA :</option>
                                            <option value="125" class="stores">125</option>
                                            <option value="130" class="stores">130</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="wrapper-select-cart select-banner-medio">
                                    <div class="button dropdown">
                                        <select id="select-stores" class="font-bold">
                                            <option>AÑO :</option>
                                            <option value="125" class="stores">125</option>
                                            <option value="130" class="stores">130</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="wrapper-select-cart select-banner-medio">
                                    <div class="button dropdown">
                                        <select id="select-stores" class="font-bold">
                                            <option>MODELO :</option>
                                            <option value="125" class="stores">125</option>
                                            <option value="130" class="stores">130</option>
                                        </select>
                                    </div>
                                </div>
                                <button class="btn-bsc-select"><i class="icon-Lupa"></i></button>
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
        </section>
        <!-- carosuel llantas home -->
        <section class="section sct-oferts">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <ul class="list-hom-dest-of">
                            <li  class="<?= in_array('index.php', $uriSegments ) ? 'active' : ''; ?>"><a href="index.php" class="font-bold text-uppercase">ofertas del mes</a></li>
                            <li  class="<?= in_array('destacados.php', $uriSegments ) ? 'active' : ''; ?>"><a href="#" class="font-bold text-uppercase">destacados</a></li>
                        </ul>
                        
                        
                    </div>
                    <div class="col-xs-12">
                        <div class="row">
                            <div class="carrousel-home">
                                <div class="item-carousel">
                                    <div class="xhov wow zoomIn">
                                        <div class="wrapper-card-product">
                                            <a href="#" class="link-wrapper-prod h100 w100 d-block">
                                                <div class="wrapper-img-prod h-auto">
                                                    <div class="content-img-prod">
                                                        <img class="w-100" src="assets/images/productos/aro.jpg" alt="">
                                                    </div>
                                                </div>
                                                <div class="info-product text-uppercase">
                                                    <h2 class="font-bold name-cod-prod">aro 15</h2>
                                                    <h3 class="font-regular name-brand">DARWIN</h3>
                                                    <div class="wrapper-price flex-al-cent sp-arrow">
                                                        <h2 class="font-bold price-prod"><span class="font-regular price-dsct-prod">S/720.00</span> S/ 720.00</h2>
                                                    </div>
                                                </div>
                                            </a>
                                            <div class="dif">
                                                <div class="add-purchase ad2b d-flex">
                                                    <div class="qty mt-5">
                                                        <span class="plus bg-dark">+</span>
                                                        <input type="number" class="count font-regular" name="qty" value="1">
                                                        <span class="minus bg-dark">-</span>
                                                    </div>
                                                    <div class="btn-purchase">
                                                        <a href="#" class="link-purchase font-bold text-uppercase text-center">comprar <i class="icon-carrito"></i></a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="circle-dscto d-flex">
                                                <h4 class="font-regular">-20%</h4>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="item-carousel">
                                    <div class="xhov wow zoomIn">
                                        <div class="wrapper-card-product">
                                            <a href="#" class="link-wrapper-prod h100 w100 d-block">
                                                <div class="wrapper-img-prod h-auto">
                                                    <div class="content-img-prod">
                                                        <img class="w-100" src="assets/images/productos/aro.jpg" alt="">
                                                    </div>
                                                </div>
                                                <div class="info-product text-uppercase">
                                                    <h2 class="font-bold name-cod-prod">aro 16</h2>
                                                    <h3 class="font-regular name-brand">DARWIN</h3>
                                                    <div class="wrapper-price flex-al-cent sp-arrow">
                                                        <h2 class="font-bold price-prod"><span class="font-regular price-dsct-prod">S/720.00</span> S/ 720.00</h2>
                                                    </div>
                                                </div>
                                            </a>
                                            <div class="dif">
                                                <div class="add-purchase ad2b d-flex">
                                                    <div class="qty mt-5">
                                                        <span class="plus bg-dark">+</span>
                                                        <input type="number" class="count font-regular" name="qty" value="1">
                                                        <span class="minus bg-dark">-</span>
                                                    </div>
                                                    <div class="btn-purchase">
                                                        <a href="#" class="link-purchase font-bold text-uppercase text-center">comprar <i class="icon-carrito"></i></a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="circle-dscto d-flex">
                                                <h4 class="font-regular">-20%</h4>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="item-carousel">
                                    <div class="xhov wow zoomIn">
                                        <div class="wrapper-card-product">
                                            <a href="#" class="link-wrapper-prod h100 w100 d-block">
                                                <div class="wrapper-img-prod h-auto">
                                                    <div class="content-img-prod">
                                                        <img class="w-100" src="assets/images/productos/aro.jpg" alt="">
                                                    </div>
                                                </div>
                                                <div class="info-product text-uppercase">
                                                    <h2 class="font-bold name-cod-prod">aro 15</h2>
                                                    <h3 class="font-regular name-brand">DARWIN</h3>
                                                    <div class="wrapper-price flex-al-cent sp-arrow">
                                                        <h2 class="font-bold price-prod"><span class="font-regular price-dsct-prod">S/720.00</span> S/ 720.00</h2>
                                                    </div>
                                                </div>
                                            </a>
                                            <div class="dif">
                                                <div class="add-purchase ad2b d-flex">
                                                    <div class="qty mt-5">
                                                        <span class="plus bg-dark">+</span>
                                                        <input type="number" class="count font-regular" name="qty" value="1">
                                                        <span class="minus bg-dark">-</span>
                                                    </div>
                                                    <div class="btn-purchase">
                                                        <a href="#" class="link-purchase font-bold text-uppercase text-center">comprar <i class="icon-carrito"></i></a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="circle-dscto d-flex">
                                                <h4 class="font-regular">-20%</h4>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="item-carousel">
                                    <div class="xhov wow zoomIn">
                                        <div class="wrapper-card-product">
                                            <a href="#" class="link-wrapper-prod h100 w100 d-block">
                                                <div class="wrapper-img-prod h-auto">
                                                    <div class="content-img-prod">
                                                        <img class="w-100" src="assets/images/productos/aro.jpg" alt="">
                                                    </div>
                                                </div>
                                                <div class="info-product text-uppercase">
                                                    <h2 class="font-bold name-cod-prod">aro 15</h2>
                                                    <h3 class="font-regular name-brand">DARWIN</h3>
                                                    <div class="wrapper-price flex-al-cent sp-arrow">
                                                        <h2 class="font-bold price-prod"><span class="font-regular price-dsct-prod">S/720.00</span> S/ 720.00</h2>
                                                    </div>
                                                </div>
                                            </a>
                                            <div class="dif">
                                                <div class="add-purchase ad2b d-flex">
                                                    <div class="qty mt-5">
                                                        <span class="plus bg-dark">+</span>
                                                        <input type="number" class="count font-regular" name="qty" value="1">
                                                        <span class="minus bg-dark">-</span>
                                                    </div>
                                                    <div class="btn-purchase">
                                                        <a href="#" class="link-purchase font-bold text-uppercase text-center">comprar <i class="icon-carrito"></i></a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="circle-dscto d-flex">
                                                <h4 class="font-regular">-20%</h4>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="item-carousel">
                                    <div class="xhov wow zoomIn">
                                        <div class="wrapper-card-product">
                                            <a href="#" class="link-wrapper-prod h100 w100 d-block">
                                                <div class="wrapper-img-prod h-auto">
                                                    <div class="content-img-prod">
                                                        <img class="w-100" src="assets/images/productos/aro.jpg" alt="">
                                                    </div>
                                                </div>
                                                <div class="info-product text-uppercase">
                                                    <h2 class="font-bold name-cod-prod">aro 16</h2>
                                                    <h3 class="font-regular name-brand">DARWIN</h3>
                                                    <div class="wrapper-price flex-al-cent sp-arrow">
                                                        <h2 class="font-bold price-prod"><span class="font-regular price-dsct-prod">S/720.00</span> S/ 720.00</h2>
                                                    </div>
                                                </div>
                                            </a>
                                            <div class="dif">
                                                <div class="add-purchase ad2b d-flex">
                                                    <div class="qty mt-5">
                                                        <span class="plus bg-dark">+</span>
                                                        <input type="number" class="count font-regular" name="qty" value="1">
                                                        <span class="minus bg-dark">-</span>
                                                    </div>
                                                    <div class="btn-purchase">
                                                        <a href="#" class="link-purchase font-bold text-uppercase text-center">comprar <i class="icon-carrito"></i></a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="circle-dscto d-flex">
                                                <h4 class="font-regular">-20%</h4>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!-- carosuel aros home -->
        <section class="section sct-oferts">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="row">
                            <div class="col-xs-12">
                                <h2 class="titles-int-home font-bold color-primary text-center">OFERTAS DEL MES</h2>
                            </div>
                            <div class="col-xs-12">
                                <div class="row">
                                    <div class="carrousel-home">
                                        <div class="item-carousel">
                                            <div class="xhov wow zoomIn">
                                                <div class="wrapper-card-product">
                                                    <a href="#" class="link-wrapper-prod h100 w100 d-block">
                                                        <div class="wrapper-img-prod h-auto">
                                                            <div class="content-img-prod">
                                                                <img class="w-100" src="assets/images/productos/aro.jpg" alt="">
                                                            </div>
                                                        </div>
                                                        <div class="info-product text-uppercase">
                                                            <h2 class="font-bold name-cod-prod">aro 15</h2>
                                                            <h3 class="font-regular name-brand">DARWIN</h3>
                                                            <div class="wrapper-price flex-al-cent sp-arrow">
                                                                <h2 class="font-bold price-prod"><span class="font-regular price-dsct-prod">S/720.00</span> S/ 720.00</h2>
                                                            </div>
                                                        </div>
                                                    </a>
                                                    <div class="dif">
                                                        <div class="add-purchase ad2b d-flex">
                                                            <div class="qty mt-5">
                                                                <span class="plus bg-dark">+</span>
                                                                <input type="number" class="count font-regular" name="qty" value="1">
                                                                <span class="minus bg-dark">-</span>
                                                            </div>
                                                            <div class="btn-purchase">
                                                                <a href="#" class="link-purchase font-bold text-uppercase text-center">comprar <i class="icon-carrito"></i></a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="circle-dscto d-flex">
                                                        <h4 class="font-regular">-20%</h4>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="item-carousel">
                                            <div class="xhov wow zoomIn">
                                                <div class="wrapper-card-product">
                                                    <a href="#" class="link-wrapper-prod h100 w100 d-block">
                                                        <div class="wrapper-img-prod h-auto">
                                                            <div class="content-img-prod">
                                                                <img class="w-100" src="assets/images/productos/aro.jpg" alt="">
                                                            </div>
                                                        </div>
                                                        <div class="info-product text-uppercase">
                                                            <h2 class="font-bold name-cod-prod">aro 16</h2>
                                                            <h3 class="font-regular name-brand">DARWIN</h3>
                                                            <div class="wrapper-price flex-al-cent sp-arrow">
                                                                <h2 class="font-bold price-prod"><span class="font-regular price-dsct-prod">S/720.00</span> S/ 720.00</h2>
                                                            </div>
                                                        </div>
                                                    </a>
                                                    <div class="dif">
                                                        <div class="add-purchase ad2b d-flex">
                                                            <div class="qty mt-5">
                                                                <span class="plus bg-dark">+</span>
                                                                <input type="number" class="count font-regular" name="qty" value="1">
                                                                <span class="minus bg-dark">-</span>
                                                            </div>
                                                            <div class="btn-purchase">
                                                                <a href="#" class="link-purchase font-bold text-uppercase text-center">comprar <i class="icon-carrito"></i></a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="circle-dscto d-flex">
                                                        <h4 class="font-regular">-20%</h4>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="item-carousel">
                                            <div class="xhov wow zoomIn">
                                                <div class="wrapper-card-product">
                                                    <a href="#" class="link-wrapper-prod h100 w100 d-block">
                                                        <div class="wrapper-img-prod h-auto">
                                                            <div class="content-img-prod">
                                                                <img class="w-100" src="assets/images/productos/aro.jpg" alt="">
                                                            </div>
                                                        </div>
                                                        <div class="info-product text-uppercase">
                                                            <h2 class="font-bold name-cod-prod">aro 15</h2>
                                                            <h3 class="font-regular name-brand">DARWIN</h3>
                                                            <div class="wrapper-price flex-al-cent sp-arrow">
                                                                <h2 class="font-bold price-prod"><span class="font-regular price-dsct-prod">S/720.00</span> S/ 720.00</h2>
                                                            </div>
                                                        </div>
                                                    </a>
                                                    <div class="dif">
                                                        <div class="add-purchase ad2b d-flex">
                                                            <div class="qty mt-5">
                                                                <span class="plus bg-dark">+</span>
                                                                <input type="number" class="count font-regular" name="qty" value="1">
                                                                <span class="minus bg-dark">-</span>
                                                            </div>
                                                            <div class="btn-purchase">
                                                                <a href="#" class="link-purchase font-bold text-uppercase text-center">comprar <i class="icon-carrito"></i></a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="circle-dscto d-flex">
                                                        <h4 class="font-regular">-20%</h4>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="item-carousel">
                                            <div class="xhov wow zoomIn">
                                                <div class="wrapper-card-product">
                                                    <a href="#" class="link-wrapper-prod h100 w100 d-block">
                                                        <div class="wrapper-img-prod h-auto">
                                                            <div class="content-img-prod">
                                                                <img class="w-100" src="assets/images/productos/aro.jpg" alt="">
                                                            </div>
                                                        </div>
                                                        <div class="info-product text-uppercase">
                                                            <h2 class="font-bold name-cod-prod">aro 15</h2>
                                                            <h3 class="font-regular name-brand">DARWIN</h3>
                                                            <div class="wrapper-price flex-al-cent sp-arrow">
                                                                <h2 class="font-bold price-prod"><span class="font-regular price-dsct-prod">S/720.00</span> S/ 720.00</h2>
                                                            </div>
                                                        </div>
                                                    </a>
                                                    <div class="dif">
                                                        <div class="add-purchase ad2b d-flex">
                                                            <div class="qty mt-5">
                                                                <span class="plus bg-dark">+</span>
                                                                <input type="number" class="count font-regular" name="qty" value="1">
                                                                <span class="minus bg-dark">-</span>
                                                            </div>
                                                            <div class="btn-purchase">
                                                                <a href="#" class="link-purchase font-bold text-uppercase text-center">comprar <i class="icon-carrito"></i></a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="circle-dscto d-flex">
                                                        <h4 class="font-regular">-20%</h4>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="item-carousel">
                                            <div class="xhov wow zoomIn">
                                                <div class="wrapper-card-product">
                                                    <a href="#" class="link-wrapper-prod h100 w100 d-block">
                                                        <div class="wrapper-img-prod h-auto">
                                                            <div class="content-img-prod">
                                                                <img class="w-100" src="assets/images/productos/aro.jpg" alt="">
                                                            </div>
                                                        </div>
                                                        <div class="info-product text-uppercase">
                                                            <h2 class="font-bold name-cod-prod">aro 16</h2>
                                                            <h3 class="font-regular name-brand">DARWIN</h3>
                                                            <div class="wrapper-price flex-al-cent sp-arrow">
                                                                <h2 class="font-bold price-prod"><span class="font-regular price-dsct-prod">S/720.00</span> S/ 720.00</h2>
                                                            </div>
                                                        </div>
                                                    </a>
                                                    <div class="dif">
                                                        <div class="add-purchase ad2b d-flex">
                                                            <div class="qty mt-5">
                                                                <span class="plus bg-dark">+</span>
                                                                <input type="number" class="count font-regular" name="qty" value="1">
                                                                <span class="minus bg-dark">-</span>
                                                            </div>
                                                            <div class="btn-purchase">
                                                                <a href="#" class="link-purchase font-bold text-uppercase text-center">comprar <i class="icon-carrito"></i></a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="circle-dscto d-flex">
                                                        <h4 class="font-regular">-20%</h4>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- carosuel baterias home -->
        <section class="section sct-oferts">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="row">
                            <div class="col-xs-12">
                                <h2 class="titles-int-home font-bold color-primary text-center">OFERTAS DEL MES</h2>
                            </div>
                            <div class="col-xs-12">
                                <div class="row">
                                    <div class="carrousel-home">
                                        <div class="item-carousel">
                                            <div class="xhov wow zoomIn">
                                                <div class="wrapper-card-product">
                                                    <a href="dettale-de-producto.php" class="link-wrapper-prod h100 w100 d-block">
                                                        <div class="wrapper-img-prod h-auto">
                                                            <div class="content-img-prod">
                                                                <img class="w-100" src="assets/images/productos/bateria.jpg" alt="">
                                                            </div>
                                                        </div>
                                                        <div class="info-product text-uppercase">
                                                            <h2 class="font-bold name-cod-prod">aro 15</h2>
                                                            <h3 class="font-regular name-brand">DARWIN</h3>
                                                            <div class="wrapper-price flex-al-cent sp-arrow">
                                                                <h2 class="font-bold price-prod"><span class="font-regular price-dsct-prod">S/720.00</span> S/ 720.00</h2>
                                                            </div>
                                                        </div>
                                                    </a>
                                                    <div class="dif">
                                                        <div class="add-purchase ad2b d-flex">
                                                            <div class="qty mt-5">
                                                                <span class="plus bg-dark">+</span>
                                                                <input type="number" class="count font-regular" name="qty" value="1">
                                                                <span class="minus bg-dark">-</span>
                                                            </div>
                                                            <div class="btn-purchase">
                                                                <a href="#" class="link-purchase font-bold text-uppercase text-center">comprar <i class="icon-carrito"></i></a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="circle-dscto d-flex">
                                                        <h4 class="font-regular">-20%</h4>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="item-carousel">
                                            <div class="xhov wow zoomIn">
                                                <div class="wrapper-card-product">
                                                    <a href="#" class="link-wrapper-prod h100 w100 d-block">
                                                        <div class="wrapper-img-prod h-auto">
                                                            <div class="content-img-prod">
                                                                <img class="w-100" src="assets/images/productos/bateria.jpg" alt="">
                                                            </div>
                                                        </div>
                                                        <div class="info-product text-uppercase">
                                                            <h2 class="font-bold name-cod-prod">aro 16</h2>
                                                            <h3 class="font-regular name-brand">DARWIN</h3>
                                                            <div class="wrapper-price flex-al-cent sp-arrow">
                                                                <h2 class="font-bold price-prod"><span class="font-regular price-dsct-prod">S/720.00</span> S/ 720.00</h2>
                                                            </div>
                                                        </div>
                                                    </a>
                                                    <div class="dif">
                                                        <div class="add-purchase ad2b d-flex">
                                                            <div class="qty mt-5">
                                                                <span class="plus bg-dark">+</span>
                                                                <input type="number" class="count font-regular" name="qty" value="1">
                                                                <span class="minus bg-dark">-</span>
                                                            </div>
                                                            <div class="btn-purchase">
                                                                <a href="#" class="link-purchase font-bold text-uppercase text-center">comprar <i class="icon-carrito"></i></a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="circle-dscto d-flex">
                                                        <h4 class="font-regular">-20%</h4>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="item-carousel">
                                            <div class="xhov wow zoomIn">
                                                <div class="wrapper-card-product">
                                                    <a href="#" class="link-wrapper-prod h100 w100 d-block">
                                                        <div class="wrapper-img-prod h-auto">
                                                            <div class="content-img-prod">
                                                                <img class="w-100" src="assets/images/productos/bateria.jpg" alt="">
                                                            </div>
                                                        </div>
                                                        <div class="info-product text-uppercase">
                                                            <h2 class="font-bold name-cod-prod">aro 15</h2>
                                                            <h3 class="font-regular name-brand">DARWIN</h3>
                                                            <div class="wrapper-price flex-al-cent sp-arrow">
                                                                <h2 class="font-bold price-prod"><span class="font-regular price-dsct-prod">S/720.00</span> S/ 720.00</h2>
                                                            </div>
                                                        </div>
                                                    </a>
                                                    <div class="dif">
                                                        <div class="add-purchase ad2b d-flex">
                                                            <div class="qty mt-5">
                                                                <span class="plus bg-dark">+</span>
                                                                <input type="number" class="count font-regular" name="qty" value="1">
                                                                <span class="minus bg-dark">-</span>
                                                            </div>
                                                            <div class="btn-purchase">
                                                                <a href="#" class="link-purchase font-bold text-uppercase text-center">comprar <i class="icon-carrito"></i></a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="circle-dscto d-flex">
                                                        <h4 class="font-regular">-20%</h4>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="item-carousel">
                                            <div class="xhov wow zoomIn">
                                                <div class="wrapper-card-product">
                                                    <a href="#" class="link-wrapper-prod h100 w100 d-block">
                                                        <div class="wrapper-img-prod h-auto">
                                                            <div class="content-img-prod">
                                                                <img class="w-100" src="assets/images/productos/bateria.jpg" alt="">
                                                            </div>
                                                        </div>
                                                        <div class="info-product text-uppercase">
                                                            <h2 class="font-bold name-cod-prod">aro 15</h2>
                                                            <h3 class="font-regular name-brand">DARWIN</h3>
                                                            <div class="wrapper-price flex-al-cent sp-arrow">
                                                                <h2 class="font-bold price-prod"><span class="font-regular price-dsct-prod">S/720.00</span> S/ 720.00</h2>
                                                            </div>
                                                        </div>
                                                    </a>
                                                    <div class="dif">
                                                        <div class="add-purchase ad2b d-flex">
                                                            <div class="qty mt-5">
                                                                <span class="plus bg-dark">+</span>
                                                                <input type="number" class="count font-regular" name="qty" value="1">
                                                                <span class="minus bg-dark">-</span>
                                                            </div>
                                                            <div class="btn-purchase">
                                                                <a href="#" class="link-purchase font-bold text-uppercase text-center">comprar <i class="icon-carrito"></i></a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="circle-dscto d-flex">
                                                        <h4 class="font-regular">-20%</h4>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="item-carousel">
                                            <div class="xhov wow zoomIn">
                                                <div class="wrapper-card-product">
                                                    <a href="#" class="link-wrapper-prod h100 w100 d-block">
                                                        <div class="wrapper-img-prod h-auto">
                                                            <div class="content-img-prod">
                                                                <img class="w-100" src="assets/images/productos/bateria.jpg" alt="">
                                                            </div>
                                                        </div>
                                                        <div class="info-product text-uppercase">
                                                            <h2 class="font-bold name-cod-prod">aro 16</h2>
                                                            <h3 class="font-regular name-brand">DARWIN</h3>
                                                            <div class="wrapper-price flex-al-cent sp-arrow">
                                                                <h2 class="font-bold price-prod"><span class="font-regular price-dsct-prod">S/720.00</span> S/ 720.00</h2>
                                                            </div>
                                                        </div>
                                                    </a>
                                                    <div class="dif">
                                                        <div class="add-purchase ad2b d-flex">
                                                            <div class="qty mt-5">
                                                                <span class="plus bg-dark">+</span>
                                                                <input type="number" class="count font-regular" name="qty" value="1">
                                                                <span class="minus bg-dark">-</span>
                                                            </div>
                                                            <div class="btn-purchase">
                                                                <a href="#" class="link-purchase font-bold text-uppercase text-center">comprar <i class="icon-carrito"></i></a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="circle-dscto d-flex">
                                                        <h4 class="font-regular">-20%</h4>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <div class="section fp-auto-height">
            <?php
                include 'src/includes/footer.php'
            ?>
        </div>
    </main>
    
    <script src="assets/js/libraries/fullpage.js"></script>
    <script src="assets/js/carrousel-home.js"></script>
    <script src="assets/js/counting.js"></script>
    <script>
        $(document).ready(function(){
            $('.tabs span').click(function(){
                var tab_id = $(this).attr('data-tab');

                $('.tabs span').removeClass('current');
                $('.tab-content').removeClass('current');

                $(this).addClass('current');
                $("#"+tab_id).addClass('current');
            })
        })
    </script>
    <script>
        $(".tabs_goto").click(function() {
            $(this)
                .addClass("-active")
                .siblings()
                .removeClass("-active")
                .closest(".tabs")
                .find(".tabs_section")
                .eq($(this).index())
                .addClass("-open")
                .siblings()
                .removeClass("-open")
            })
    </script>

    <script>
        if (screen && screen.width > 1300) {
            let fullpageDiv = $('#fullpage');
            if (fullpageDiv.length) {
                fullpageDiv.fullpage({
                    scrollBar: true,
                    scrollOverflow: true,
                    verticalCentered: true,
                    afterRender: function () {

                    }
                });
            }
        }
    </script>

</body>

</html>