$('#btn-send-form').on('click', function(){
    $("#form-contact-us").validate({
        rules: {
            "name": "required",
            "last-name": "required",
            "email": {
                required: true,
                email: true
            },
            "phone": {
                required: true,
                minlength: 9
            },
            "textarea": {
                required: true
            }
        },
    });
})
