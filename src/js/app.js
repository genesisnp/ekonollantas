window.Popper = require('popper.js').default

try {
  window.$ = window.jQuery = require('jquery')

} catch (e) {}

require ('slick-carousel')
require ('./header')
require ('./accordion')
require ('./form-config')
require ('./select')
require ('./ancla-banner')
require ('./buscador')
