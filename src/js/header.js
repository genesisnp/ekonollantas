$(document).ready(function () {
    $(window).scroll(function () {
        var scrollTop = $(window).scrollTop();
        if (scrollTop > 50) {
            $('.wrapper-header.scroll').addClass('header-scroll');
            $('.menu-btn.scroll').css({
                'color': '#000 !important'
            })
        } else {
            $('.wrapper-header.scroll').removeClass('header-scroll');
            $('.menu-btn.scroll').css({
                'color': 'white'
            })
        }
    });

    $('#menu-btn').click(function () {
        if ($('#menu-btn').attr('class') == 'icon-hamb-open') {

            $('#menu-btn').removeClass('icon-hamb-open').addClass('icon-hamb-close').css({
                'color': 'black !important'
            });
            $('#navbar-right').css({
                'right': '0',
            });
            $('#menu-btn').css({
                'color': '#000'
            })

        } else {

            $('#menu-btn').removeClass('icon-hamb-close').addClass('icon-hamb-open');
            $('#navbar-right').css({
                'right': '-100%',
            });
            $('#menu-btn').css({
                'color': 'white'
            });
        }
    });
});