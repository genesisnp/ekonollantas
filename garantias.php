<?php
    include 'src/includes/header.php'
?>
    <main class="main-">
        <section class="sct-banner">
            <div class="degrade-int"></div>
            <img class="img-banner" src="assets/images/banner/nuestros-servicios.jpg" alt="">
            <div class="content-text-banner">
                <i class="icon-banner icon-garantias"></i>
                <h2 class="title-banner text-uppercase font-bold">garantías</h2>
                <p class="p-internas p2">¡Compra con Confianza!</p>
            </div>
            <a href="#guarantee" class="icon-arrow" data-ancla="guarantee"></a>
        </section>
        <section class="guarantee" id="guarantee">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="description-page">
                            <p class="text-center text-page font-regular">Todos los productos comercializados por EKONO
                                llantas cuentan con una
                                garantía sobre defectos de fábrica.</p>
                            <p class="text-center text-page font-regular">Si tuvieras algún inconveniente con alguno de
                                nuestros productos, o
                                tienes alguna duda relacionada a estos, envíanos un correo a <strong
                                    class="color-primary">compraenlinea@ekonollantas.com</strong>, o recibe asistencia
                                telefónica llamando al <strong>2194666.</strong></p>
                        </div>
                    </div>
                    <div class="col-xs-12">
                        <ul class="accordion">
                            <li><a class="accordion-heading font-bold">Condiciones sobre la garantía <i
                                        class="icon-flecha-accord"></i></a>
                                <div class="accordion-content">
                                    <p class="p-internas">Para poder realizar una revisión completa del producto
                                        adquirido es importante que te acerques a nuestra tienda de Pueblo Libre, ver
                                        dirección y mapa (link a pestaña de tiendas), llenar un formulario simple de
                                        inspección y un técnico calificado realizará un informe técnico.</p>
                                    <p class="p-internas">En el caso de llantas o aros el plazo de emisión del informe
                                        técnico es de 3 días hábiles, en el caso de otros productos puede variar de 3 a
                                        15 días.</p>
                                    <h3 class="subtitle-heading2 color-primary font-bold">Sobre llantas:</h3>
                                    <p class="p-internas">Esta garantía es vigente dentro de los cinco años posteriores
                                        a la fecha de compra y aplica solo a aquellas llantas que como mínimo tengan 1.6
                                        mm de profundidad de cocada.</p>
                                    <p class="p-internas">La garantía no es aplicable a fallas ocasionadas por
                                        resultados de abusos en su uso, como accidentes, pinchaduras, cortes, desgarro,
                                        magulladuras, desgaste irregular derivados de defectos mecánicos, presión de
                                        aire inadecuada, fuego, impacto, colisiones, defectos de carrocería o partes
                                        mecánicas que obstruyen el normal funcionamiento de la llanta, u otras
                                        condiciones atribuibles al conductor, tampoco aplica la garantía a aquellos
                                        neumáticos que hayan sido usados en competencias, piques, drifting.</p>
                                    <p class="p-internas">Si después de la revisión el neumático muestra que puede ser
                                        sujeto a cobertura por garantía (ajustable), la concesión será sobre la compra
                                        de un nuevo neumático. El cliente deberá pagar la cantidad correspondiente al
                                        desgaste de la banda de rodamiento, expresado como porcentaje del dibujo
                                        original de éste, multiplicado por el precio de compra del usuario, de un
                                        neumático nuevo en el momento del reemplazo.</p>
                                    <p class="p-internas">Esta garantía no incluye costos de servicios, materiales,
                                        gastos de transporte y otros gastos relacionados con la gestión del reclamo.</p>

                                    <h3 class="subtitle-heading2 color-primary font-bold">Sobre aros:</h3>
                                    <p class="p-internas">La garantía de aros tiene 6 meses de vigencia desde la fecha
                                        de compra, esta garantía cubre desperfectos originados en el proceso de
                                        fabricación, como por ejemplo porosidad, problemas con el acabado y problemas de
                                        dinamismo.</p>
                                </div>
                            </li>
                            <li><a class="accordion-heading font-bold">Retracto, cambios o devoluciones <i
                                        class="icon-flecha-accord"></i></a>
                                <div class="accordion-content">
                                    <p class="p-internas">¡Tu Satisfacción es lo primero!</p>
                                    <p class="p-internas">Si no estás conforme con el producto, lo cambiamos o te
                                        reintegramos el pago, deberás comunicarte en un máximo de 7 días hábiles a
                                        partir de haber recibido los productos (no aplica para compras por importación
                                        directa ni para compras de llantas de vehículos de carga o transporte como bus,
                                        camión u OTR).</p>
                                    <p class="p-internas">Los productos deben de estar en perfecto estado, sin uso, como
                                        cuando los entregamos y no deben haberse instalados.</p>
                                    <p class="p-internas">Es muy importante recibir tu comunicación para gestionar este
                                        cambio o devolución.</p>
                                    <p class="p-internas">Envíanos un correo a <strong
                                            class="color-primary">compraenlinea@ekonollantas.com</strong>, o recibe
                                        asistencia telefónica llamando al 2194650.</p>
                                </div>
                            </li>
                            <li><a class="accordion-heading font-bold">Reintegro del pago <i
                                        class="icon-flecha-accord"></i></a>
                                <div class="accordion-content">
                                    <p class="p-internas">Si el pago se realizó con tarjeta de crédito, el plazo
                                        establecido es de 15 días hábiles.</p>
                                    <p class="p-internas">Si el pago se realizó con depósito o trasferencia a nuestras
                                        cuentas, se emitirá un cheque no negociable, en un plazo de 07 días hábiles.</p>
                                    <h3 class="subtitle-heading2 font-bold">Tener en cuenta :</h3>
                                    <ul>
                                        <li class="p-internas">Si compraste con boleta de venta el cheque se emitirá con
                                            el nombre de quién realizó la compra (los datos de la boleta de venta), no
                                            se podrá emitir a nombre de otra persona o a nombre de una empresa.</li>
                                        <li class="p-internas">Si compraste con factura el cheque se emitirá con el
                                            nombre de la empresa, no se podrá emitir a nombre de otra empresa o a nombre
                                            personal.</li>
                                    </ul>
                                    <h2 class="font-bold color-primary text-ext">¡Estaremos gustosos de atenderte!</h2>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>
    </main>
    <?php
        include 'src/includes/footer.php'
    ?>
</body>

</html>